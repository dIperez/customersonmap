﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using FieldService.ServiceDispatch;
using PX.Common;
using PX.Data;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.EP;
using PX.Objects.IN;
using PX.Objects.AR;


public partial class Page_AR900000 : PX.Web.UI.PXPage
{
    public String baseUrl;
    public String pageUrl;

    protected void Page_Load(object sender, EventArgs e)
    {
        baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/')+ "/(W(10000))/";
        string urlKey = "ar";
        string webMethodPage = "/CustomersOnMap/AR990000.aspx";
        string path = Request.Path.Substring(0, Request.Path.IndexOf(urlKey) + urlKey.Length) + "/";
		DateTime? startDateBridge;
        var date = PXContext.GetBusinessDate();

        pageUrl = path + webMethodPage;
    }
}