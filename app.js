/*
 * This file is generated and updated by Sencha Cmd. You can edit this file as
 * needed for your application, but these edits will have to be merged by
 * Sencha Cmd when upgrading.
 */
/*Ext.application({
    name: 'CustomersOnMap',

    extend: 'CustomersOnMap.Application',

    requires: [
        'CustomersOnMap.view.main.Main'
    ],

    // The name of the initial view to create. With the classic toolkit this class
    // will gain a "viewport" plugin if it does not extend Ext.Viewport. With the
    // modern toolkit, the main view will be added to the Viewport.
    //
    mainView: 'CustomersOnMap.view.main.Main'
	
    //-------------------------------------------------------------------------
    // Most customizations should be made to CustomersOnMap.Application. If you need to
    // customize this file, doing so below this section reduces the likelihood
    // of merge conflicts when upgrading to new versions of Sencha Cmd.
    //-------------------------------------------------------------------------
});*/


/*
    This file is generated and updated by Sencha Cmd. You can edit this file as
    needed for your application, but these edits will have to be merged by
    Sencha Cmd when upgrading.
*/

// MVC relies on loading on demand, so we need to enable and configure the loader
Ext.Loader.setConfig({
    disableCaching  : true,
    enabled         :true
});

Ext.application({
    name: 'CustomersOnMap',

    controllers:[
        'Main'
    ],

    autoCreateViewport: false,

    launch: function (){
        var me = this;

        var container = new CustomersOnMap.view.MainContainer(
            {
                renderTo: 'main-container'
            }
        );
    }
});