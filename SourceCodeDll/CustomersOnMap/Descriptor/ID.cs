﻿using PX.Objects.CS;

namespace FieldService.ServiceDispatch
{
    public static class ID
    {        
        //FSEquipment - LocationType
        public class LocationType
        {
            public const string WAREHOUSE   = "W";
            public const string EMPLOYEE    = "E";

            public readonly string[] ID_LIST = { ID.LocationType.WAREHOUSE, ID.LocationType.EMPLOYEE };
            public readonly string[] TX_LIST = { TX.LocationType.WAREHOUSE, TX.LocationType.EMPLOYEE };
        }

        //FSEquipment - Condition
        public class Condition
        {
            public const string NEW     = "N";
            public const string USED    = "U";

            public readonly string[] ID_LIST = { ID.Condition.NEW, ID.Condition.USED };
            public readonly string[] TX_LIST = { TX.Condition.NEW, TX.Condition.USED };
        }

        //FSLicense - OwnerType
        public class OwnerType
        {
            public const string BUSINESS    = "B";
            public const string EMPLOYEE    = "E";

            public readonly string[] ID_LIST = { ID.OwnerType.BUSINESS, ID.OwnerType.EMPLOYEE };
            public readonly string[] TX_LIST = { TX.OwnerType.BUSINESS, TX.OwnerType.EMPLOYEE };
        }

        //FSLicense - TermType
        public class TermType
        {
            public const string DAYS    = "D";
            public const string MONTH   = "M";

            public readonly string[] ID_LIST = { ID.TermType.DAYS, ID.TermType.MONTH };
            public readonly string[] TX_LIST = { TX.TermType.DAYS, TX.TermType.MONTH };
        }

        //FSServiceType - Invoice Subaccount Source
        public class InvoiceSubaccountSource
        {
            public const string CLIENT              = "CL";
            public const string ORDER_TYPE          = "OT";
            public const string BRANCH              = "BR";
            public const string SERVICE             = "SC";
            public const string BRANCH_LOCATION     = "BL"; 

            public readonly string[] ID_LIST = { ID.InvoiceSubaccountSource.CLIENT, ID.InvoiceSubaccountSource.ORDER_TYPE, ID.InvoiceSubaccountSource.SERVICE, ID.InvoiceSubaccountSource.BRANCH };
            public readonly string[] TX_LIST = { TX.InvoiceSubaccountSource.CLIENT, TX.InvoiceSubaccountSource.ORDER_TYPE, TX.InvoiceSubaccountSource.SERVICE, TX.InvoiceSubaccountSource.BRANCH };
        }

        //FSServiceCode - Charges To Apply
        public class BillingRule
        {
            public const string TIME                    = "TIME";
            public const string FLAT_RATE               = "FLRA";
            public const string NONE                    = "NONE";

            public readonly string[] ID_LIST = { ID.BillingRule.TIME, ID.BillingRule.FLAT_RATE, ID.BillingRule.NONE };
            public readonly string[] TX_LIST = { TX.BillingRule.TIME, TX.BillingRule.FLAT_RATE, TX.BillingRule.NONE };
        }

        //FSEmployeeSchedule - ScheduleType
        public class ScheduleType
        {
            public const string AVAILABILITY   = "A";
            public const string UNAVAILABILITY = "U";
            public const string BUSY           = "B";

            public readonly string[] ID_LIST = { ID.ScheduleType.AVAILABILITY, ID.ScheduleType.UNAVAILABILITY, ID.ScheduleType.BUSY };
            public readonly string[] TX_LIST = { TX.ScheduleType.AVAILABILITY, TX.ScheduleType.UNAVAILABILITY, TX.ScheduleType.BUSY };
        }

        //FSSchedule - Frequency Type
        public class Schedule_FrequencyType
        {
            public const string DAILY   = "D";
            public const string WEEKLY  = "W";
            public const string MONTHLY = "M";
            public const string ANNUAL  = "A";

            public readonly string[] ID_LIST = { ID.Schedule_FrequencyType.DAILY, ID.Schedule_FrequencyType.WEEKLY, ID.Schedule_FrequencyType.MONTHLY, ID.Schedule_FrequencyType.ANNUAL };
            public readonly string[] TX_LIST = { TX.Schedule_FrequencyType.DAILY, TX.Schedule_FrequencyType.WEEKLY, TX.Schedule_FrequencyType.MONTHLY, TX.Schedule_FrequencyType.ANNUAL };
        }

        //FSSchedule - Entity Type
        public class Schedule_EntityType
        {
            public const string CONTRACT = "C";
            public const string EMPLOYEE = "E";

            public readonly string[] ID_LIST = { ID.Schedule_EntityType.CONTRACT, ID.Schedule_EntityType.EMPLOYEE };
            public readonly string[] TX_LIST = { TX.Schedule_EntityType.CONTRACT, TX.Schedule_EntityType.EMPLOYEE };
        }

        ////FSEmployeeSchedule - SingleDayFlag
        //public class SingleDayFlag
        //{
        //    public const string SINGLE_DAY = "S";
        //    public const string MULTIPLE_DAYS = "M";
        //}

        //FSSetup - TimeConstants        
        public class TimeConstants
        {
            public const int HOURS_12   = 720;
            public const int MINUTES_10 = 10;
            public const int MINUTES_15 = 15;
            public const int MINUTES_30 = 30;  
            public const int MINUTES_60 = 60;
        }

        public class WeekDays
        {
            public const string ANYDAY       = "NT";
            public const string SUNDAY       = "SU";
            public const string MONDAY       = "MO";
            public const string TUESDAY      = "TU";
            public const string WEDNESDAY    = "WE";
            public const string THURSDAY     = "TH";
            public const string FRIDAY       = "FR";
            public const string SATURDAY     = "SA";
            
            public readonly string[] ID_LIST = { ID.WeekDays.ANYDAY, ID.WeekDays.SUNDAY, ID.WeekDays.MONDAY, ID.WeekDays.TUESDAY, ID.WeekDays.WEDNESDAY, ID.WeekDays.THURSDAY, ID.WeekDays.FRIDAY, ID.WeekDays.SATURDAY };
            public readonly string[] TX_LIST = { TX.WeekDays.ANYDAY, TX.WeekDays.SUNDAY, TX.WeekDays.MONDAY, TX.WeekDays.TUESDAY, TX.WeekDays.WEDNESDAY, TX.WeekDays.THURSDAY, TX.WeekDays.FRIDAY, TX.WeekDays.SATURDAY };
        }

        public class WeekDaysNumber
        {
            public const int SUNDAY    = 0;
            public const int MONDAY    = 1;
            public const int TUESDAY   = 2;
            public const int WEDNESDAY = 3;
            public const int THURSDAY  = 4;
            public const int FRIDAY    = 5;
            public const int SATURDAY  = 6;

            public readonly int[] ID_LIST = { ID.WeekDaysNumber.SUNDAY, ID.WeekDaysNumber.MONDAY, ID.WeekDaysNumber.TUESDAY, ID.WeekDaysNumber.WEDNESDAY, ID.WeekDaysNumber.THURSDAY, ID.WeekDaysNumber.FRIDAY, ID.WeekDaysNumber.SATURDAY };
            public readonly string[] TX_LIST = { TX.WeekDays.SUNDAY, TX.WeekDays.MONDAY, TX.WeekDays.TUESDAY, TX.WeekDays.WEDNESDAY, TX.WeekDays.THURSDAY, TX.WeekDays.FRIDAY, TX.WeekDays.SATURDAY };
        }

        public class Months
        {
            public const string JANUARY   = "JAN";
            public const string FEBRUARY  = "FEB";
            public const string MARCH     = "MAR";
            public const string APRIL     = "APR";
            public const string MAY       = "MAY";
            public const string JUNE      = "JUN";
            public const string JULY      = "JUL";
            public const string AUGUST    = "AUG";
            public const string SEPTEMBER = "SEP";
            public const string OCTOBER   = "OCT";
            public const string NOVEMBER  = "NOV";
            public const string DECEMBER  = "DEC";

            public readonly string[] ID_LIST =
            { 
                ID.Months.JANUARY, 
                ID.Months.FEBRUARY, 
                ID.Months.MARCH, 
                ID.Months.APRIL, 
                ID.Months.MAY, 
                ID.Months.JUNE, 
                ID.Months.JULY, 
                ID.Months.AUGUST,
                ID.Months.SEPTEMBER, 
                ID.Months.OCTOBER, 
                ID.Months.NOVEMBER, 
                ID.Months.DECEMBER
            };

            public readonly string[] TX_LIST =
            {
                TX.Months.JANUARY,
                TX.Months.FEBRUARY,
                TX.Months.MARCH,
                TX.Months.APRIL,
                TX.Months.MAY,
                TX.Months.JUNE,
                TX.Months.JULY,
                TX.Months.AUGUST,
                TX.Months.SEPTEMBER,
                TX.Months.OCTOBER,
                TX.Months.NOVEMBER,
                TX.Months.DECEMBER
            };
        }

        public class SourceType_ServiceOrder
        {
            public const string CUSTOMER_MANAGEMENT = "CR";
            public const string SALES_ORDER         = "SO";
            public const string SERVICE_DISPATCH    = "SD";
            public const string QUOTE               = "QU";

            public readonly string[] ID_LIST = { ID.SourceType_ServiceOrder.CUSTOMER_MANAGEMENT, ID.SourceType_ServiceOrder.SALES_ORDER, ID.SourceType_ServiceOrder.SERVICE_DISPATCH, ID.SourceType_ServiceOrder.QUOTE };
            public readonly string[] TX_LIST = { TX.SourceType_ServiceOrder.CUSTOMER_MANAGEMENT, TX.SourceType_ServiceOrder.SALES_ORDER, TX.SourceType_ServiceOrder.SERVICE_DISPATCH, TX.SourceType_ServiceOrder.QUOTE };            
        }

        public class AppResizePrecision_Setup
        {
            public readonly int[] ID_LIST = { ID.TimeConstants.MINUTES_10, ID.TimeConstants.MINUTES_15, ID.TimeConstants.MINUTES_30, ID.TimeConstants.MINUTES_60 };
            public readonly string[] TX_LIST = { TX.AppResizePrecision_Setup.MINUTES_10, TX.AppResizePrecision_Setup.MINUTES_15, TX.AppResizePrecision_Setup.MINUTES_30, TX.AppResizePrecision_Setup.MINUTES_60 };
        }

        #region Priority+Severity
        public abstract class Priority_ALL
        {
            public const string LOW     = "L";
            public const string MEDIUM  = "M";
            public const string HIGH    = "H";

            public readonly string[] ID_LIST = { ID.Priority_ALL.LOW, ID.Priority_ALL.MEDIUM, ID.Priority_ALL.HIGH };
            public readonly string[] TX_LIST = { TX.Priority_ALL.LOW, TX.Priority_ALL.MEDIUM, TX.Priority_ALL.HIGH };
        }

        public class Priority_ServiceOrder : Priority_ALL
        {
        }

        public class Severity_ServiceOrder : Priority_ALL
        {
        }
        #endregion

        #region LineType

        public class LineType_All
        {
            public const string SERVICE             = "SERVI";
            public const string INVENTORY_ITEM      = "SLPRO";
            public const string COMMENT_SERVICE     = "CM_SV";
            public const string INSTRUCTION_SERVICE = "IT_SV";
            public const string COMMENT_PART        = "CM_PT";
            public const string INSTRUCTION_PART    = "IT_PT";
            public const string SERVICE_TEMPLATE    = "TEMPL";
            public const string NONSTOCKITEM        = "NSTKI";

            public readonly string[] ID_LIST_ALL = 
            { 
                ID.LineType_All.SERVICE,
                ID.LineType_All.COMMENT_SERVICE,
                ID.LineType_All.INSTRUCTION_SERVICE,
                ID.LineType_All.SERVICE_TEMPLATE,
                ID.LineType_All.INVENTORY_ITEM, 
                ID.LineType_All.COMMENT_PART, 
                ID.LineType_All.INSTRUCTION_PART,
                ID.LineType_All.NONSTOCKITEM
            };

            public readonly string[] TX_LIST_ALL = 
            { 
                TX.LineType_ALL.SERVICE,
                TX.LineType_ALL.COMMENT_SERVICE, 
                TX.LineType_ALL.INSTRUCTION_SERVICE,
                TX.LineType_ALL.SERVICE_TEMPLATE,
                TX.LineType_ALL.INVENTORY_ITEM, 
                TX.LineType_ALL.COMMENT_PART, 
                TX.LineType_ALL.INSTRUCTION_PART,
                TX.LineType_ALL.NONSTOCKITEM
            };

            public readonly string[] ID_LIST_PART = 
            { 
                ID.LineType_All.INVENTORY_ITEM, 
                ID.LineType_All.COMMENT_PART, 
                ID.LineType_All.INSTRUCTION_PART 
            };

            public readonly string[] TX_LIST_PART = 
            { 
                TX.LineType_ALL.INVENTORY_ITEM, 
                TX.LineType_ALL.COMMENT_PART, 
                TX.LineType_ALL.INSTRUCTION_PART 
            };
        }

        public class LineType_ServiceTemplate : LineType_All
        {
            public readonly string[] ID_LIST_SERVICE = 
            { 
                ID.LineType_All.SERVICE,
                ID.LineType_All.COMMENT_SERVICE, 
                ID.LineType_All.INSTRUCTION_SERVICE,
                ID.LineType_All.NONSTOCKITEM
            };

            public readonly string[] TX_LIST_SERVICE = 
            { 
                TX.LineType_ALL.SERVICE,
                TX.LineType_ALL.COMMENT_SERVICE, 
                TX.LineType_ALL.INSTRUCTION_SERVICE,
                TX.LineType_ALL.NONSTOCKITEM
            };           
        }

        public class LineType_ServiceContract : LineType_All
        {
            public readonly string[] ID_LIST_SERVICE = 
            { 
                ID.LineType_All.SERVICE,
                ID.LineType_All.COMMENT_SERVICE, 
                ID.LineType_All.INSTRUCTION_SERVICE,
                ID.LineType_All.NONSTOCKITEM,
                ID.LineType_All.SERVICE_TEMPLATE 
            };

            public readonly string[] TX_LIST_SERVICE = 
            { 
                TX.LineType_ALL.SERVICE,
                TX.LineType_ALL.COMMENT_SERVICE, 
                TX.LineType_ALL.INSTRUCTION_SERVICE,
                TX.LineType_ALL.NONSTOCKITEM,
                TX.LineType_ALL.SERVICE_TEMPLATE 
            };            
        }

        #endregion

        #region PriceType
        public class PriceType
        {
            public const string CONTRACT    = "CONTR";
            public const string CUSTOMER    = "CUSTM";
            public const string PRICE_CLASS = "PRCLS";
            public const string BASE        = "BASE";
            public const string DEFAULT     = "DEFLT";

            public readonly string[] ID_LIST_PRICETYPE = { ID.PriceType.CONTRACT, ID.PriceType.CUSTOMER, ID.PriceType.PRICE_CLASS, ID.PriceType.BASE, ID.PriceType.DEFAULT };
            public readonly string[] TX_LIST_PRICETYPE = { TX.PriceType.CONTRACT, TX.PriceType.CUSTOMER, TX.PriceType.PRICE_CLASS, TX.PriceType.BASE, TX.PriceType.DEFAULT };
        }
        #endregion

        #region Status
        public class Status_Parts
        {
            public const string OPEN      = "O";
            public const string CANCELED  = "X";
            public const string COMPLETED = "C";

            public readonly string[] ID_LIST = { ID.Status_Parts.OPEN, ID.Status_Parts.CANCELED, ID.Status_Parts.COMPLETED };
            public readonly string[] TX_LIST = { TX.Status_Parts.OPEN, TX.Status_Parts.CANCELED, TX.Status_Parts.COMPLETED };
        }

        public class Status_AppointmentDet : Status_Parts
        {
            public const string IN_PROCESS  = "P"; //Android            

            public new readonly string[] ID_LIST = { ID.Status_AppointmentDet.OPEN, ID.Status_AppointmentDet.IN_PROCESS, ID.Status_AppointmentDet.CANCELED, ID.Status_AppointmentDet.COMPLETED };
            public new readonly string[] TX_LIST = { TX.Status_AppointmentDet.OPEN, TX.Status_AppointmentDet.IN_PROCESS, TX.Status_AppointmentDet.CANCELED, TX.Status_AppointmentDet.COMPLETED };
        }

        public class Status_Appointment
        {
            public const string AUTOMATIC_SCHEDULED  = "A";
            public const string MANUAL_SCHEDULED     = "S";
            public const string IN_PROCESS           = "P";
            public const string CANCELED             = "X";
            public const string COMPLETED            = "C";
            public const string CLOSED               = "Z";

            public readonly string[] ID_LIST = 
            {
                ID.Status_Appointment.AUTOMATIC_SCHEDULED, ID.Status_Appointment.MANUAL_SCHEDULED,
                    ID.Status_Appointment.IN_PROCESS, ID.Status_Appointment.CANCELED,
                        ID.Status_Appointment.COMPLETED, ID.Status_Appointment.CLOSED
            };

            public readonly string[] TX_LIST = 
            { 
                TX.Status_Appointment.AUTOMATIC_SCHEDULED, TX.Status_Appointment.MANUAL_SCHEDULED,
                    TX.Status_Appointment.IN_PROCESS, TX.Status_Appointment.CANCELED, 
                        TX.Status_Appointment.COMPLETED, TX.Status_Appointment.CLOSED
            };
        }
             
        public class Status_ServiceOrder
        {
            public const string OPEN               = "O";
            public const string QUOTE              = "Q";
            public const string ON_HOLD            = "H";
            public const string CANCELED           = "X";
            public const string CLOSED             = "Z";
            public const string COMPLETED          = "C";

            public readonly string[] ID_LIST = { ID.Status_ServiceOrder.OPEN, ID.Status_ServiceOrder.QUOTE, ID.Status_ServiceOrder.ON_HOLD, ID.Status_ServiceOrder.CANCELED, ID.Status_ServiceOrder.CLOSED, ID.Status_ServiceOrder.COMPLETED };
            public readonly string[] TX_LIST = { TX.Status_ServiceOrder.OPEN, TX.Status_ServiceOrder.QUOTE, TX.Status_ServiceOrder.ON_HOLD, TX.Status_ServiceOrder.CANCELED, TX.Status_ServiceOrder.CLOSED, TX.Status_ServiceOrder.COMPLETED };
        }

        public class Status_ServiceContract
        {
            public const string ACTIVE      = "A";
            public const string ON_HOLD     = "H";
            public const string INACTIVE    = "I";

            public readonly string[] ID_LIST = { ID.Status_ServiceContract.ACTIVE, ID.Status_ServiceContract.ON_HOLD, ID.Status_ServiceContract.INACTIVE };
            public readonly string[] TX_LIST = { TX.Status_ServiceContract.ACTIVE, TX.Status_ServiceContract.ON_HOLD, TX.Status_ServiceContract.INACTIVE };
        }

        public class Status_Route
        {
            public const string OPEN        = "O";
            public const string IN_PROCESS  = "P";
            public const string CANCELED    = "X";
            public const string COMPLETED   = "C";
            public const string CLOSED      = "Z";

            public readonly string[] ID_LIST = { ID.Status_Route.OPEN, ID.Status_Route.IN_PROCESS, ID.Status_Route.CANCELED, ID.Status_Route.COMPLETED, ID.Status_Route.CLOSED };
            public readonly string[] TX_LIST = { TX.Status_Route.OPEN, TX.Status_Route.IN_PROCESS, TX.Status_Route.CANCELED, TX.Status_Route.COMPLETED, TX.Status_Route.CLOSED };
        }

        public class Status_Equipment
        {
            public const string ACTIVE      = "A";
            public const string INACTIVE    = "I";

            public readonly string[] ID_LIST = { ID.Status_Equipment.ACTIVE, ID.Status_Equipment.INACTIVE };
            public readonly string[] TX_LIST = { TX.Status_Equipment.ACTIVE, TX.Status_Equipment.INACTIVE };
        }

        public class Status_InventoryItem
        {
            public const string ACTIVE = "AC";
            public const string INACTIVE = "IN";

            public readonly string[] ID_LIST = { ID.Status_InventoryItem.ACTIVE, ID.Status_InventoryItem.INACTIVE };
            public readonly string[] TX_LIST = { TX.Status_InventoryItem.ACTIVE, TX.Status_InventoryItem.INACTIVE };
        }
        #endregion

        public class FuelType_Equipment
        {
            public const string REGULAR_UNLEADED    = "R";
            public const string PREMIUM_UNLEADED    = "P";
            public const string DIESEL              = "D";
            public const string OTHER               = "O";

            public readonly string[] ID_LIST = { ID.FuelType_Equipment.REGULAR_UNLEADED, ID.FuelType_Equipment.PREMIUM_UNLEADED, ID.FuelType_Equipment.DIESEL, ID.FuelType_Equipment.OTHER };
            public readonly string[] TX_LIST = { TX.FuelType_Equipment.REGULAR_UNLEADED, TX.FuelType_Equipment.PREMIUM_UNLEADED, TX.FuelType_Equipment.DIESEL, TX.FuelType_Equipment.OTHER };
        }

        #region Inquiry
        public class Confirmed_Appointment
        {
            public const string ALL           = "A";
            public const string CONFIRMED     = "C";
            public const string NOT_CONFIRMED = "N";

            public readonly string[] ID_LIST = { ID.Confirmed_Appointment.ALL, ID.Confirmed_Appointment.CONFIRMED, ID.Confirmed_Appointment.NOT_CONFIRMED };
            public readonly string[] TX_LIST = { TX.Confirmed_Appointment.ALL, TX.Confirmed_Appointment.CONFIRMED, TX.Confirmed_Appointment.NOT_CONFIRMED };
        }

        public class PeriodType
        {
            public const string DAY   = "D";
            public const string WEEK  = "W";
            public const string MONTH = "M";

            public readonly string[] ID_LIST = { ID.PeriodType.DAY, ID.PeriodType.WEEK, ID.PeriodType.MONTH };
            public readonly string[] TX_LIST = { TX.PeriodType.DAY, TX.PeriodType.WEEK, TX.PeriodType.MONTH };
        }
        #endregion
        
        public class LicenseType_ValidIn
        {
            public const string ALL_LOCATIONS       = "ALL";
            public const string COUNTRY_STATE_CITY  = "CSC";
            public const string GEOGRAPHICAL_ZONE   = "GZN";
        }

        public class ReasonType
        {
            public const string CANCEL_SERVICE_ORDER = "CSOR";
            public const string CANCEL_APPOINTMENT   = "CAPP";
            public const string WORKFLOW_STAGE       = "WSTG";
            public const string APPOINTMENT_DETAIL   = "APPD";
            public const string GENERAL              = "GNRL";

            public readonly string[] ID_LIST = 
            {
                ID.ReasonType.CANCEL_SERVICE_ORDER, ID.ReasonType.CANCEL_APPOINTMENT, 
                    ID.ReasonType.WORKFLOW_STAGE, ID.ReasonType.APPOINTMENT_DETAIL, ID.ReasonType.GENERAL
            };

            public readonly string[] TX_LIST = 
            {
                TX.ReasonType.CANCEL_SERVICE_ORDER, TX.ReasonType.CANCEL_APPOINTMENT, 
                    TX.ReasonType.WORKFLOW_STAGE, TX.ReasonType.APPOINTMENT_DETAIL, TX.ReasonType.GENERAL
            };
        }

        public class Setup_CopyTimeSpentFrom
        {
            public const string NONE                              = "N";
            public const string ACTUAL_APPOINTMENT_LINE_DURATION  = "A";
            public const string SERVICE_DURATION                  = "T";

            public readonly string[] ID_LIST = 
            {                
                ID.Setup_CopyTimeSpentFrom.ACTUAL_APPOINTMENT_LINE_DURATION, 
                ID.Setup_CopyTimeSpentFrom.SERVICE_DURATION,
                ID.Setup_CopyTimeSpentFrom.NONE
            };

            public readonly string[] TX_LIST =
            {                
                TX.Setup_CopyTimeSpentFrom.ACTUAL_APPOINTMENT_LINE_DURATION, 
                TX.Setup_CopyTimeSpentFrom.SERVICE_DURATION,
                TX.Setup_CopyTimeSpentFrom.NONE
            };
        }

        public class ContractType_BillingFrequency
        {
            public const string EVERY_4TH_MONTH     = "F";
            public const string SEMI_ANNUAL         = "S";
            public const string ANNUAL              = "A";
            public const string BEG_OF_CONTRACT     = "B";
            public const string END_OF_CONTRACT     = "E";
            public const string DAYS_30_60_90       = "D";
            public const string TIME_OF_SERVICE     = "T";
            public const string NONE                = "N";
            public const string MONTHLY             = "M";

            public readonly string[] ID_LIST =
            {
                ID.ContractType_BillingFrequency.EVERY_4TH_MONTH, ID.ContractType_BillingFrequency.SEMI_ANNUAL, ID.ContractType_BillingFrequency.ANNUAL,
                    ID.ContractType_BillingFrequency.BEG_OF_CONTRACT, ID.ContractType_BillingFrequency.END_OF_CONTRACT, ID.ContractType_BillingFrequency.DAYS_30_60_90,
                    ID.ContractType_BillingFrequency.TIME_OF_SERVICE, ID.ContractType_BillingFrequency.NONE, ID.ContractType_BillingFrequency.MONTHLY
            };

            public readonly string[] TX_LIST =
            {
                TX.ContractType_BillingFrequency.EVERY_4TH_MONTH, TX.ContractType_BillingFrequency.SEMI_ANNUAL, TX.ContractType_BillingFrequency.ANNUAL,
                    TX.ContractType_BillingFrequency.BEG_OF_CONTRACT, TX.ContractType_BillingFrequency.END_OF_CONTRACT, TX.ContractType_BillingFrequency.DAYS_30_60_90,
                    TX.ContractType_BillingFrequency.TIME_OF_SERVICE, TX.ContractType_BillingFrequency.NONE, TX.ContractType_BillingFrequency.MONTHLY
            };
        }

        public class ContractType_BillingType
        {
            public const string SEPARATE_ORIGINAL   = "S";
            public const string COMPLETE            = "C";

            public readonly string[] ID_LIST = { ID.ContractType_BillingType.SEPARATE_ORIGINAL, ID.ContractType_BillingType.COMPLETE };
            public readonly string[] TX_LIST = { TX.ContractType_BillingType.SEPARATE_ORIGINAL, TX.ContractType_BillingType.COMPLETE };
        }

        #region SrvOrdType
        public class SrvOrdType_RecordType
        {
            //public const string TRAVEL             = "TV";
            //public const string TRAINING           = "TR";
            //public const string DOWNTIME           = "DT";
            public const string SERVICE_ORDER      = "SO";

            public readonly string[] ID_LIST =
            {
                ID.SrvOrdType_RecordType.SERVICE_ORDER/*, ID.SrvOrdType_RecordType.TRAVEL*//*, ID.SrvOrdType_RecordType.TRAINING*/,
                                                    /*ID.SrvOrdType_RecordType.DOWNTIME*/
            };

            public readonly string[] TX_LIST =
            {
                TX.SrvOrdType_RecordType.SERVICE_ORDER/*, TX.SrvOrdType_RecordType.TRAVEL*//*, TX.SrvOrdType_RecordType.TRAINING*/,
                                                    /*TX.SrvOrdType_RecordType.DOWNTIME*/
            };
        }

        public class SrvOrdType_PostTo
        {
            public const string NONE                        = "NA";
            public const string ACCOUNTS_RECEIVABLE_MODULE  = "AR";
            public const string SALES_ORDER_MODULE          = "SO";

            public readonly string[] ID_LIST = { ID.SrvOrdType_PostTo.NONE, ID.SrvOrdType_PostTo.ACCOUNTS_RECEIVABLE_MODULE, ID.SrvOrdType_PostTo.SALES_ORDER_MODULE };
            public readonly string[] TX_LIST = { TX.SrvOrdType_PostTo.NONE, TX.SrvOrdType_PostTo.ACCOUNTS_RECEIVABLE_MODULE, TX.SrvOrdType_PostTo.SALES_ORDER_MODULE };
        }

        public class SrvOrdType_SalesAcctSource 
        {
            public const string CUSTOMER_LOCATION   = "CL";
            public const string INVENTORY_ITEM      = "II";

            public readonly string[] ID_LIST = { ID.SrvOrdType_SalesAcctSource.CUSTOMER_LOCATION, ID.SrvOrdType_SalesAcctSource.INVENTORY_ITEM };
            public readonly string[] TX_LIST = { TX.SrvOrdType_SalesAcctSource.CUSTOMER_LOCATION, TX.SrvOrdType_SalesAcctSource.INVENTORY_ITEM };
        }

        public class SrvOrdType_GenerateInvoiceBy
        {
            public const string CRM_AR       = "CRAR";
            public const string SALES_ORDER  = "SORD";
            public const string PROJECT      = "PROJ";
            public const string NOT_BILLABLE = "NBIL";

            public readonly string[] ID_LIST =
            {
                ID.SrvOrdType_GenerateInvoiceBy.CRM_AR, ID.SrvOrdType_GenerateInvoiceBy.SALES_ORDER,
                    ID.SrvOrdType_GenerateInvoiceBy.PROJECT, ID.SrvOrdType_GenerateInvoiceBy.NOT_BILLABLE
            };

            public readonly string[] TX_LIST =
            {
                TX.SrvOrdType_GenerateInvoiceBy.CRM_AR, TX.SrvOrdType_GenerateInvoiceBy.SALES_ORDER,
                    TX.SrvOrdType_GenerateInvoiceBy.PROJECT, TX.SrvOrdType_GenerateInvoiceBy.NOT_BILLABLE
            };
        }

        public class BusinessAcctType
        {
            public const string CUSTOMER = "C";
            public const string PROSPECT = "P";

            public readonly string[] ID_LIST = { ID.BusinessAcctType.CUSTOMER, ID.BusinessAcctType.PROSPECT };
            public readonly string[] TX_LIST = { TX.BusinessAcctType.CUSTOMER, TX.BusinessAcctType.PROSPECT };
        }

        public class Source_Info
        {
            public const string BUSINESS_ACCOUNT = "BA";
            public const string CUSTOMER_CONTACT = "CC";
            public const string BRANCH_LOCATION  = "BL";
        }

        public class SrvOrdType_AppAddressSource : Source_Info
        {
            public readonly string[] ID_LIST = { ID.Source_Info.BUSINESS_ACCOUNT, ID.Source_Info.CUSTOMER_CONTACT, ID.Source_Info.BRANCH_LOCATION };
            public readonly string[] TX_LIST = { TX.Source_Info.BUSINESS_ACCOUNT, TX.Source_Info.CUSTOMER_CONTACT, TX.Source_Info.BRANCH_LOCATION };
        }

        public class SrvOrdType_AppContactInfoSource : Source_Info
        {
            public readonly string[] ID_LIST = { ID.Source_Info.BUSINESS_ACCOUNT, ID.Source_Info.CUSTOMER_CONTACT };
            public readonly string[] TX_LIST = { TX.Source_Info.BUSINESS_ACCOUNT, TX.Source_Info.CUSTOMER_CONTACT };
        }

        #endregion

        public class ValidationType
        {
            public const string PREVENT      = "D";
            public const string WARN         = "W";
            public const string NOT_VALIDATE = "N";

            public readonly string[] ID_LIST =
            {
                ID.ValidationType.PREVENT, ID.ValidationType.WARN, 
                    ID.ValidationType.NOT_VALIDATE
            };

            public readonly string[] TX_LIST =
            {
                TX.ValidationType.PREVENT, TX.ValidationType.WARN, 
                    TX.ValidationType.NOT_VALIDATE
            };
        }

        public class SourcePrice
        {
            public const string CONTRACT   = "C";
            public const string PRICE_LIST = "P";

            public readonly string[] ID_LIST = { ID.SourcePrice.CONTRACT, ID.SourcePrice.PRICE_LIST };
            public readonly string[] TX_LIST = { TX.SourcePrice.CONTRACT, TX.SourcePrice.PRICE_LIST };
        }

        public class FuelType
        {
            public const string DIESEL = "D";
            public const string GAS = "G";

            public readonly string[] ID_LIST = { ID.FuelType.DIESEL, ID.FuelType.GAS };
            public readonly string[] TX_LIST = { TX.FuelType.DIESEL, TX.FuelType.GAS };
        }

        public class WarrantyDurationType
        {
            public const string DAY   = "D";
            public const string MONTH = "M";
            public const string YEAR  = "Y";

            public readonly string[] ID_LIST = { ID.WarrantyDurationType.DAY, ID.WarrantyDurationType.MONTH, ID.WarrantyDurationType.YEAR };
            public readonly string[] TX_LIST = { TX.WarrantyDurationType.DAY, TX.WarrantyDurationType.MONTH, TX.WarrantyDurationType.YEAR };
        }

        public class WarrantyApplicationOrder
        {
            public const string COMPANY = "C";
            public const string VENDOR  = "V";

            public readonly string[] ID_LIST = { ID.WarrantyApplicationOrder.COMPANY, ID.WarrantyApplicationOrder.VENDOR };
            public readonly string[] TX_LIST = { TX.WarrantyApplicationOrder.COMPANY, TX.WarrantyApplicationOrder.VENDOR };
        }

        public class ModelType
        {
            public const string EQUIPMENT   = "EQ";
            public const string REPLACEMENT = "RP";

            public readonly string[] ID_LIST = { ID.ModelType.EQUIPMENT, ID.ModelType.REPLACEMENT };
            public readonly string[] TX_LIST = { TX.ModelType.EQUIPMENT, TX.ModelType.REPLACEMENT };
        }

        public class SourceType_Equipment
        {
            public const string SM_EQUIPMENT        = "SME";
            public const string VEHICLE             = "VEH";
            public const string EP_EQUIPMENT        = "EPE";
            public const string AR_INVOICE          = "ARI";

            public readonly string[] ID_LIST = { ID.SourceType_Equipment.SM_EQUIPMENT, ID.SourceType_Equipment.VEHICLE, ID.SourceType_Equipment.EP_EQUIPMENT, ID.SourceType_Equipment.AR_INVOICE };
            public readonly string[] TX_LIST = { TX.SourceType_Equipment.SM_EQUIPMENT, TX.SourceType_Equipment.VEHICLE, TX.SourceType_Equipment.EP_EQUIPMENT, TX.SourceType_Equipment.AR_INVOICE };
        }

        //This class is used for filtering purposes only
        public class SourceType_Equipment_ALL : SourceType_Equipment
        {
            public const string ALL = "ALL";

            public readonly new string[] ID_LIST = { ID.SourceType_Equipment_ALL.SM_EQUIPMENT, ID.SourceType_Equipment_ALL.ALL };
            public readonly new string[] TX_LIST = { TX.SourceType_Equipment_ALL.SM_EQUIPMENT, TX.SourceType_Equipment_ALL.ALL };
        }

        public class ScreenID
        {
            public const string WRKPROCESS                            = "SD200000";
            public const string APPOINTMENT                           = "SD300200";
            public const string EMPLOYEE_SCHEDULE                     = "SD202200";
            public const string SALES_ORDER                           = "SO301000";
            public const string GENERATE_SERVICE_CONTRACT_APPOINTMENT = "SD500200";
            public const string BRANCH_LOCATION                       = "SD202500";
            public const string ROUTE_CLOSING                         = "SD304010";
        }

        public class OwnerType_Equipment
        {
            public const string THIRD_PARTY = "TP";
            public const string OWN         = "OW";

            public readonly string[] ID_LIST = { ID.OwnerType_Equipment.THIRD_PARTY, ID.OwnerType_Equipment.OWN };
            public readonly string[] TX_LIST = { TX.OwnerType_Equipment.THIRD_PARTY, TX.OwnerType_Equipment.OWN };
        }

        public class AcumaticaErrorNumber
        { 
            public const string SAVE_DISABLED_BUTTON = "#96" /*Error number in acumatica when you try to save a document and the save button is disabled*/;
        }

        public class GoogleMapsStatusCodes
        {
            public const string OK                     = "OK";
            public const string NOT_FOUND              = "NOT_FOUND";
            public const string ZERO_RESULTS           = "ZERO_RESULTS";
            public const string MAX_WAYPOINTS_EXCEEDED = "MAX_WAYPOINTS_EXCEEDED";
            public const string INVALID_REQUEST        = "INVALID_REQUEST";
            public const string OVER_QUERY_LIMIT       = "OVER_QUERY_LIMIT";
            public const string REQUEST_DENIED         = "REQUEST_DENIED";
            public const string UNKNOWN_ERROR          = "UNKNOWN_ERROR";
        }

        public class ScheduleMonthlySelection 
        { 
            public const string DAILY       = "D";
            public const string WEEKLY      = "W";
        }

        public class RecordType_ServiceContract
        {
            public const string SERVICE_CONTRACT = "NRSC";
            public const string ROUTE_SERVICE_CONTRACT = "IRSC";
            public const string EMPLOYEE_SCHEDULE_CONTRACT = "EPSC";

            public readonly string[] ID_LIST =
            {
                ID.RecordType_ServiceContract.SERVICE_CONTRACT, ID.RecordType_ServiceContract.ROUTE_SERVICE_CONTRACT,
                    ID.RecordType_ServiceContract.EMPLOYEE_SCHEDULE_CONTRACT
            };

            public readonly string[] TX_LIST =
            {
                TX.RecordType_ServiceContract.SERVICE_CONTRACT, TX.RecordType_ServiceContract.ROUTE_SERVICE_CONTRACT,
                    TX.RecordType_ServiceContract.EMPLOYEE_SCHEDULE_CONTRACT
            };
        }

        public class Behavior_SrvOrderType
        {
            public const string REGULAR_APPOINTMENT     = "RE";
            public const string ROUTE_APPOINTMENT       = "RO";
            public const string INTERNAL_APPOINTMENT    = "IN";
            public const string QUOTE                   = "QT";

            public readonly string[] ID_LIST =
            {
                ID.Behavior_SrvOrderType.REGULAR_APPOINTMENT, ID.Behavior_SrvOrderType.ROUTE_APPOINTMENT, 
                ID.Behavior_SrvOrderType.INTERNAL_APPOINTMENT, ID.Behavior_SrvOrderType.QUOTE
            };

            public readonly string[] TX_LIST =
            {
                TX.Behavior_SrvOrderType.REGULAR_APPOINTMENT, TX.Behavior_SrvOrderType.ROUTE_APPOINTMENT, 
                TX.Behavior_SrvOrderType.INTERNAL_APPOINTMENT, TX.Behavior_SrvOrderType.QUOTE
            };
        }

        public class ContactType_ApptMail
        {
            //public const string VENDOR_STAFF   = "X"; TODO:SD-6998
            public const string CUSTOMER       = "U";
            public const string EMPLOYEE_STAFF = "F";           
            public const string GEOZONE_STAFF  = "G";
            
            public readonly string[] ID_LIST =
            {
                NotificationContactType.Billing, NotificationContactType.Employee,
                ID.ContactType_ApptMail.CUSTOMER, ID.ContactType_ApptMail.EMPLOYEE_STAFF, /*ID.ContactType_ApptMail.VENDOR_STAFF, TODO:SD-6998*/
                ID.ContactType_ApptMail.GEOZONE_STAFF
            };

            public readonly string[] TX_LIST =
            {
                PX.Objects.AR.Messages.Billing, PX.Objects.EP.Messages.Employee,
                TX.ContactType_ApptMail.CUSTOMER, TX.ContactType_ApptMail.EMPLOYEE_STAFF, /*TX.ContactType_ApptMail.VENDOR_STAFF, TODO:SD-6998*/            
                TX.ContactType_ApptMail.GEOZONE_STAFF
            };
        }

        public class Calendar_ExceptionType
        {
            public const string AVAILABILITY   = "CA";
            public const string UNAVAILABILITY = "CE";
        }

        public class AppointmentAssignment_Status
        {
            public const string DELETE_APPOINTMENT_FROM_DB  = "D";
            public const string UNASSIGN_APPOINTMENT_ONLY   = "U";
        }

        public class EmployeeTimeSlotLevel
        {
            public const int BASE = 0;
            public const int COMPRESS = 1;

            public readonly int[] ID_LIST = { ID.EmployeeTimeSlotLevel.BASE, ID.EmployeeTimeSlotLevel.COMPRESS };
            public readonly string[] TX_LIST = { TX.EmployeeTimeSlotLevel.BASE, TX.EmployeeTimeSlotLevel.COMPRESS };
        }

        public class Service_Action_Type
        {
            public const string NO_ITEMS_RELATED    = "N";
            public const string PICKED_UP_ITEMS     = "P";
            public const string DELIVERED_ITEMS     = "D";

            public readonly string[] ID_LIST =
            {
                ID.Service_Action_Type.NO_ITEMS_RELATED, ID.Service_Action_Type.PICKED_UP_ITEMS, 
                ID.Service_Action_Type.DELIVERED_ITEMS
            };

            public readonly string[] TX_LIST =
            {
                TX.Service_Action_Type.NO_ITEMS_RELATED, TX.Service_Action_Type.PICKED_UP_ITEMS, 
                TX.Service_Action_Type.DELIVERED_ITEMS
            };
        }

        public class Billing_By
        {
            public const string APPOINTMENT     = "AP";
            public const string SERVICE_ORDER   = "SO";

            public readonly string[] ID_LIST =
            {
                ID.Billing_By.APPOINTMENT, ID.Billing_By.SERVICE_ORDER
            };

            public readonly string[] TX_LIST =
            {
                TX.Billing_By.APPOINTMENT, TX.Billing_By.SERVICE_ORDER
            };
        }

        public class Billing_Cycle_Type
        {
            public const string APPOINTMENT     = "AP";
            public const string SERVICE_ORDER   = "SO";
            public const string TIME_CYCLE      = "TC";
            public const string PURCHASE_ORDER  = "PO";
            public const string WORK_ORDER      = "WO";

            public readonly string[] ID_LIST =
            {
                ID.Billing_Cycle_Type.APPOINTMENT, ID.Billing_Cycle_Type.SERVICE_ORDER,
                    ID.Billing_Cycle_Type.TIME_CYCLE, ID.Billing_Cycle_Type.PURCHASE_ORDER,
                    ID.Billing_Cycle_Type.WORK_ORDER
            };

            public readonly string[] TX_LIST =
            {
                TX.Billing_Cycle_Type.APPOINTMENT, TX.Billing_Cycle_Type.SERVICE_ORDER,
                    TX.Billing_Cycle_Type.TIME_CYCLE, TX.Billing_Cycle_Type.PURCHASE_ORDER,
                    TX.Billing_Cycle_Type.WORK_ORDER
            };
        }

        public class Time_Cycle_Type
        {
            public const string WEEKDAY      = "WK";
            public const string DAY_OF_MONTH = "MT";

            public readonly string[] ID_LIST = { ID.Time_Cycle_Type.WEEKDAY, ID.Time_Cycle_Type.DAY_OF_MONTH };
            public readonly string[] TX_LIST = { TX.Time_Cycle_Type.WEEKDAY, TX.Time_Cycle_Type.DAY_OF_MONTH };
        }

        public class Frequency_Type
        {
            public const string WEEKLY  = "WK";
            public const string MONTHLY = "MT";
            public const string NONE    = "NO";

            public readonly string[] ID_LIST = { ID.Frequency_Type.WEEKLY, ID.Frequency_Type.MONTHLY, ID.Frequency_Type.NONE };
            public readonly string[] TX_LIST = { TX.Frequency_Type.WEEKLY, TX.Frequency_Type.MONTHLY, TX.Frequency_Type.NONE };
        }

        public class Send_Invoices_To
        {
            public const string DEFAULT_LOCATION    = "DF";
            public const string SPECIFIC_LOCATION   = "LC";

            public readonly string[] ID_LIST = { ID.Send_Invoices_To.DEFAULT_LOCATION, ID.Send_Invoices_To.SPECIFIC_LOCATION };
            public readonly string[] TX_LIST = { TX.Send_Invoices_To.DEFAULT_LOCATION, TX.Send_Invoices_To.SPECIFIC_LOCATION };
        }

        public class Batch_PostTo : Batch_PostTo_Filter
        {
            public new const string SO      = "SO";
            public new const string AR_AP   = "AA";
            public const string AR          = "AR";
            public const string AP          = "AP";
            public const string IN          = "IN";

            public new readonly string[] ID_LIST = { ID.Batch_PostTo.AR, ID.Batch_PostTo.SO, ID.Batch_PostTo.AP, ID.Batch_PostTo.IN, ID.Batch_PostTo.AR_AP };
            public new readonly string[] TX_LIST = { TX.Batch_PostTo.AR, TX.Batch_PostTo.SO, TX.Batch_PostTo.AP, TX.Batch_PostTo.IN, TX.Batch_PostTo.AR_AP };
        }

        public class Batch_PostTo_Filter
        {
            public const string AR_AP = "AA";
            public const string SO = "SO";

            public readonly string[] ID_LIST = { ID.Batch_PostTo.AR_AP, ID.Batch_PostTo.SO };
            public readonly string[] TX_LIST = { TX.Batch_PostTo.AR_AP, TX.Batch_PostTo.SO };
        }

        public class TablePostSource
        {
            public const string FSAPPOINTMENT_DET               = "FSAppointmentDet";
            public const string FSAPPOINTNMENT_INVENTORYITEM    = "FSInventoryItem";
            public const string FSSO_DET                        = "FSSODet";
        }

        public class PriceErrorCode
        {
            public const string OK                = "OK";
            public const string UOM_INCONSISTENCY = "UOM_INCONSISTENCY";
        }
     }
}
