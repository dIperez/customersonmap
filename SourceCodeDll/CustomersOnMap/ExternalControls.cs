using System;
using System.Collections.Generic;
using System.Linq;
using PX.Data;
using PX.Objects.AR;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.EP;
using PX.Objects.GL;
using PX.Objects.IN;

namespace FieldService.ServiceDispatch
{
    public class ExternalControls : PXGraph<ExternalControls>
    {
        //#region DispatchBoardSelect
        //    #region Single
        //        public PXSelect<BAccount> BAccountBase;
        //        public PXSelect<FSSetup> SetupRecords;
        //        public PXSelect<FSSkill> SkillRecords;
        //        public PXSelect<FSGeoZone> GeoZoneRecords;
        //        public PXSelect<FSProblem> ProblemRecords;
        //        public PXSelect<FSLicenseType> LicenseTypeRecords;
        //        public PXSelect<FSBranchLocation> BranchLocationRecords;
        //        public PXSelect<FSRoom> SDRooms;
        //    #endregion

        //    #region BranchLocation
        //        public PXSelect<FSBranchLocation,
        //                Where<
        //                    FSBranchLocation.branchLocationID, Equal<Required<FSBranchLocation.branchLocationID>>>> BranchLocationRecord;

        //        public PXSelect<FSBranchLocation,
        //                Where<
        //                    FSBranchLocation.branchID, Equal<Required<FSBranchLocation.branchID>>>> BranchLocationRecordsByBranch;
        //    #endregion   

        //    #region Extension

        //        public PXSelectJoin<EPEquipment,
        //               InnerJoin<FSEquipment,
        //                   On<FSEquipment.sourceID, Equal<EPEquipment.equipmentID>>>,
        //                   Where<
        //                       FSxEquipment.sDEnabled, Equal<True>,
        //                   And<
        //                       FSEquipment.isVehicle, Equal<False>>>> SDResources;

        //        public PXSelect<INItemClass,
        //                Where<
        //                    INItemClass.stkItem, Equal<False>>> SDServiceClasses;

        //        public PXSelect<InventoryItem,
        //                Where2<
        //                    Where<
        //                        InventoryItem.stkItem, Equal<False>,
        //                        And<Match<Current<AccessInfo.userName>>>>,
        //                And<
        //                    Where<
        //                        FSxService.sDEnabled, Equal<True>,
        //                        And<InventoryItem.itemStatus, Equal<InventoryItemStatus.active>>>>>> SDServices;

        //        public PXSelectJoin<FSAppointment,
        //                            InnerJoin<FSServiceOrder,
        //                                On<
        //                                    FSServiceOrder.sOID, Equal<FSAppointment.sOID>>,
        //                            LeftJoin<Location,
        //                                On<
        //                                    Location.locationID, Equal<FSServiceOrder.locationID>>,
        //                            LeftJoin<Contact,
        //                                On<
        //                                    Contact.contactID, Equal<FSServiceOrder.contactID>>,
        //                            LeftJoin<BAccount,
        //                                On<
        //                                    BAccount.bAccountID, Equal<FSServiceOrder.customerID>>>>>>,
        //                            Where<
        //                                FSServiceOrder.status, NotEqual<FSServiceOrder.status.Canceled>,
        //                            And<
        //                                FSAppointment.scheduledDateTimeBegin, GreaterEqual<Required<FSAppointment.scheduledDateTimeBegin>>,
        //                            And<
        //                                FSAppointment.scheduledDateTimeEnd, Less<Required<FSAppointment.scheduledDateTimeEnd>>,
        //                            And<
        //                                FSServiceOrder.branchID, Equal<Required<FSServiceOrder.branchID>>>>>>> SDAppointments;

        //        public PXSelectJoin<FSAppointment,
        //                        InnerJoin<FSServiceOrder,
        //                            On<
        //                                FSServiceOrder.sOID, Equal<FSAppointment.sOID>>,
        //                        LeftJoin<Location,
        //                            On<
        //                                Location.locationID, Equal<FSServiceOrder.locationID>>,
        //                        LeftJoin<Contact,
        //                            On<
        //                                Contact.contactID, Equal<FSServiceOrder.contactID>>,
        //                        LeftJoin<BAccount,
        //                            On<
        //                                BAccount.bAccountID, Equal<FSServiceOrder.customerID>>>>>>,
        //                        Where<
        //                            FSServiceOrder.status, NotEqual<FSServiceOrder.status.Canceled>,
        //                        And<
        //                            FSAppointment.scheduledDateTimeBegin, GreaterEqual<Required<FSAppointment.scheduledDateTimeBegin>>,
        //                        And<
        //                            FSAppointment.scheduledDateTimeEnd, Less<Required<FSAppointment.scheduledDateTimeEnd>>,
        //                        And<
        //                            FSServiceOrder.branchID, Equal<Required<FSServiceOrder.branchID>>,

        //                        And<
        //                            FSServiceOrder.branchLocationID, Equal<Required<FSServiceOrder.branchLocationID>>>>>>>> SDAppointmentsByBranchLocation;

        //        public PXSelectJoin<FSAppointment,
        //                            InnerJoin<FSServiceOrder,
        //                                On<
        //                                    FSServiceOrder.sOID, Equal<FSAppointment.sOID>>,
        //                            LeftJoin<FSAppointmentEmployee,
        //                                On<
        //                                    FSAppointmentEmployee.appointmentID, Equal<FSAppointment.appointmentID>>,
        //                            LeftJoin<EPEmployee,
        //                                On<
        //                                    EPEmployee.bAccountID, Equal<FSAppointmentEmployee.employeeID>>,
        //                            LeftJoin<Location,
        //                                On<
        //                                    Location.locationID, Equal<FSServiceOrder.locationID>>,
        //                            LeftJoin<Contact,
        //                                On<
        //                                    Contact.contactID, Equal<FSServiceOrder.contactID>>,
        //                            LeftJoin<BAccount,
        //                                On<
        //                                    BAccount.bAccountID, Equal<FSServiceOrder.customerID>>>>>>>>,
        //                            Where<
        //                                FSServiceOrder.status, NotEqual<FSServiceOrder.status.Canceled>,

        //                            And<
        //                                FSAppointment.scheduledDateTimeBegin, GreaterEqual<Required<FSAppointment.scheduledDateTimeBegin>>,
        //                            And<
        //                                FSAppointment.scheduledDateTimeEnd, Less<Required<FSAppointment.scheduledDateTimeEnd>>>>>,
        //                            OrderBy<
        //                                Asc<
        //                                    FSAppointmentEmployee.employeeID>>> SDEmployeesAppointments;

        //        public PXSelectJoin<FSAppointment,
        //                    InnerJoin<FSServiceOrder,

        //                        On<
        //                            FSServiceOrder.sOID, Equal<FSAppointment.sOID>>,
        //                    InnerJoin<FSAppointmentEmployee,

        //                        On<
        //                            FSAppointmentEmployee.appointmentID, Equal<FSAppointment.appointmentID>>,
        //                    LeftJoin<Location,

        //                        On<
        //                            Location.locationID, Equal<FSServiceOrder.locationID>>,
        //                    LeftJoin<Contact,

        //                        On<
        //                            Contact.contactID, Equal<FSServiceOrder.contactID>>,
        //                    LeftJoin<BAccount,

        //                        On<
        //                            BAccount.bAccountID, Equal<FSServiceOrder.customerID>>>>>>>,

        //                    Where<
        //                        FSServiceOrder.status, NotEqual<FSServiceOrder.status.Canceled>,

        //                    And<
        //                        FSAppointment.scheduledDateTimeBegin, GreaterEqual<Required<FSAppointment.scheduledDateTimeBegin>>,

        //                    And<
        //                        FSAppointment.scheduledDateTimeEnd, Less<Required<FSAppointment.scheduledDateTimeEnd>>,

        //                    And<
        //                        FSAppointmentEmployee.employeeID, Equal<Required<FSAppointmentEmployee.employeeID>>,

        //                    And<
        //                        FSServiceOrder.branchID, Equal<Required<FSServiceOrder.branchID>>>>>>>> SDAppointmentsByEmployee;

        //        public PXSelectJoin<FSAppointment,
        //                   InnerJoin<FSServiceOrder,
        //                        On<
        //                            FSServiceOrder.sOID, Equal<FSAppointment.sOID>>,
        //                    InnerJoin<FSAppointmentEmployee,
        //                        On<
        //                            FSAppointmentEmployee.appointmentID, Equal<FSAppointment.appointmentID>>,
        //                    LeftJoin<Location,
        //                        On<
        //                            Location.locationID, Equal<FSServiceOrder.locationID>>,
        //                    LeftJoin<Contact,
        //                        On<
        //                            Contact.contactID, Equal<FSServiceOrder.contactID>>,
        //                    LeftJoin<BAccount,
        //                        On<
        //                            BAccount.bAccountID, Equal<FSServiceOrder.customerID>>>>>>>,
        //                    Where<
        //                        FSServiceOrder.status, NotEqual<FSServiceOrder.status.Canceled>,
        //                    And<
        //                        FSAppointment.scheduledDateTimeBegin, GreaterEqual<Required<FSAppointment.scheduledDateTimeBegin>>,
        //                    And<
        //                        FSAppointment.scheduledDateTimeEnd, Less<Required<FSAppointment.scheduledDateTimeEnd>>,
        //                    And<
        //                        FSAppointmentEmployee.employeeID, Equal<Required<FSAppointmentEmployee.employeeID>>,
        //                    And<
        //                        FSServiceOrder.branchID, Equal<Required<FSServiceOrder.branchID>>,
        //                    And<
        //                        FSServiceOrder.branchLocationID, Equal<Required<FSServiceOrder.branchLocationID>>>>>>>>> SDAppointmentsByEmployeeBranchLocation;

        //        public PXSelectJoin<FSServiceOrder,
        //                InnerJoin<FSAppointment,
        //                    On<
        //                        FSAppointment.sOID, Equal<FSServiceOrder.sOID>>>,
        //                Where<
        //                    FSServiceOrder.status, NotEqual<FSServiceOrder.status.Canceled>,
        //                And<
        //                    FSAppointment.appointmentID, Equal<Required<FSAppointment.appointmentID>>>>> SDServiceOrder;

        //        public PXSelect<FSAppointmentEmployee,
        //                    Where<
        //                        FSAppointmentEmployee.appointmentID, Equal<Required<FSAppointmentEmployee.appointmentID>>>> SDAppointmentEmployees;
        //#endregion

        //    #region Calendar

        //        public PXSelect<CSCalendar,
        //                    Where<
        //                        CSCalendar.calendarID, Equal<Required<CSCalendar.calendarID>>>> CSCalendar;

        //        public PXSelect<CSCalendarExceptions,
        //                Where<
        //                    CSCalendarExceptions.calendarID, Equal<Required<CSCalendarExceptions.calendarID>>,
        //                And<
        //                    CSCalendarExceptions.date, Equal<Required<CSCalendarExceptions.date>>>>> CSCalendarExceptions;

        //        public PXSelect<CSCalendarExceptions,
        //                        Where<
        //                            CSCalendarExceptions.calendarID, Equal<Required<CSCalendarExceptions.calendarID>>,
        //                        And<
        //                            CSCalendarExceptions.date, GreaterEqual<Required<CSCalendarExceptions.date>>,
        //                        And<
        //                            CSCalendarExceptions.date, LessEqual<Required<CSCalendarExceptions.date>>>>>> FromToCSCalendarExceptions;
        //    #endregion

        //    #region Contact
        //        public PXSelect<Contact,
        //            Where<
        //                Contact.contactID, Equal<Required<Contact.contactID>>>> Contact;

        //        public PXSelect<Contact,
        //                Where<
        //                    Contact.bAccountID, Equal<Optional<EPEmployee.parentBAccountID>>,
        //                And<
        //                    Contact.contactID, Equal<Optional<EPEmployee.defContactID>>>>> EmployeeContact;

        //        public PXSelectJoin<Contact,
        //                InnerJoin<BAccount,
        //                    On<
        //                        BAccount.bAccountID, Equal<Contact.bAccountID>>>,
        //                Where<
        //                    Contact.bAccountID, Equal<Required<Contact.bAccountID>>,
        //                And<
        //                    Contact.contactID, Equal<BAccount.defContactID>>>> CustomerContact;

        //        public PXSelect<BAccount,
        //                Where<
        //                    BAccount.bAccountID, Equal<Required<BAccount.bAccountID>>>> Customer;
        //#endregion

        //    #region InventoryItem
        //        public PXSelect<InventoryItem,
        //                Where<
        //                    InventoryItem.inventoryID, Equal<Required<InventoryItem.inventoryID>>>> InventoryItem;

        //        public PXSelectJoin<InventoryItem,
        //                InnerJoin<FSSODet,
        //                    On<
        //                        FSSODet.serviceID, Equal<InventoryItem.inventoryID>>>,
        //                Where<
        //                    FSSODet.lineType, Equal<ListField_LineType_Service_ServiceTemplate.Service>,
        //                And<
        //                    FSSODet.sODetID, Equal<Required<FSSODet.sODetID>>>>> SODetInventoryItem;
        //    #endregion

        //    #region Employee

        //        public 
        //            PXSelectJoin<EPEmployee,
        //            LeftJoin<Contact,
        //                On<Contact.contactID, Equal<EPEmployee.defContactID>>>,
        //            Where<
        //                EPEmployee.userID, Equal<Current<AccessInfo.userID>>>> AndroidEmployee; 

        //        public 
        //            PXSelect<BAccount,
        //            Where<
        //                BAccount.bAccountID, Equal<Required<BAccount.bAccountID>>>> EmployeeBAccount;

        //        public 
        //            PXSelectJoin<FSWrkEmployeeSchedule,
        //            LeftJoin<FSBranchLocation,
        //                On<FSBranchLocation.branchLocationID, Equal<FSWrkEmployeeSchedule.branchLocationID>>>,
        //            Where<
        //                FSWrkEmployeeSchedule.timeStart, GreaterEqual<Required<FSWrkEmployeeSchedule.timeStart>>,
        //            And<
        //                FSWrkEmployeeSchedule.timeEnd, LessEqual<Required<FSWrkEmployeeSchedule.timeEnd>>,
        //            And<
        //                FSWrkEmployeeSchedule.branchID, Equal<Required<FSWrkEmployeeSchedule.branchID>>,
        //            And<
        //                FSWrkEmployeeSchedule.branchLocationID, Equal<Required<FSWrkEmployeeSchedule.branchLocationID>>>>>>> EmployeesWorkingScheduleByBranchLocation;

        //        public 
        //            PXSelectJoin<FSWrkEmployeeSchedule,
        //            LeftJoin<FSBranchLocation,
        //                On<FSBranchLocation.branchLocationID, Equal<FSWrkEmployeeSchedule.branchLocationID>>>,
        //            Where<
        //                FSWrkEmployeeSchedule.timeStart, GreaterEqual<Required<FSWrkEmployeeSchedule.timeStart>>,
        //            And<
        //                FSWrkEmployeeSchedule.timeEnd, LessEqual<Required<FSWrkEmployeeSchedule.timeEnd>>,
        //            And<
        //                FSWrkEmployeeSchedule.employeeID, Equal<Required<FSWrkEmployeeSchedule.employeeID>>,
        //            And<
        //                FSWrkEmployeeSchedule.branchID, Equal<Required<FSWrkEmployeeSchedule.branchID>>>>>>> WorkingScheduleByEmployee;

        //        public 
        //            PXSelectJoin<FSWrkEmployeeSchedule,
        //            LeftJoin<FSBranchLocation,
        //                On<FSBranchLocation.branchLocationID, Equal<FSWrkEmployeeSchedule.branchLocationID>>>,
        //            Where<
        //                FSWrkEmployeeSchedule.timeStart, GreaterEqual<Required<FSWrkEmployeeSchedule.timeStart>>,
        //            And<
        //                FSWrkEmployeeSchedule.timeEnd, LessEqual<Required<FSWrkEmployeeSchedule.timeEnd>>,
        //            And<
        //                FSWrkEmployeeSchedule.employeeID, Equal<Required<FSWrkEmployeeSchedule.employeeID>>,
        //            And<
        //                FSWrkEmployeeSchedule.branchID, Equal<Required<FSWrkEmployeeSchedule.branchID>>,
        //            And<
        //                FSWrkEmployeeSchedule.branchLocationID, Equal<Required<FSWrkEmployeeSchedule.branchLocationID>>>>>>>> WorkingScheduleByEmployeeAndBranchLocation;
            
        //        public 
        //            PXSelectJoinGroupBy<EPEmployee,
        //            InnerJoin<FSAppointmentEmployee,
        //                On<
        //                    FSAppointmentEmployee.employeeID, Equal<EPEmployee.bAccountID>>,
        //            InnerJoin<FSAppointment,
        //                On<
        //                    FSAppointment.appointmentID, Equal<FSAppointmentEmployee.appointmentID>>,
        //            InnerJoin<FSServiceOrder,
        //                On<
        //                    FSServiceOrder.sOID, Equal<FSAppointment.sOID>>,
        //            LeftJoin<BAccount,
        //                On<
        //                    BAccount.bAccountID, Equal<EPEmployee.bAccountID>>>>>>,
        //            Where<
        //                FSAppointment.scheduledDateTimeBegin, GreaterEqual<Required<FSAppointment.scheduledDateTimeBegin>>,
        //            And<
        //                FSAppointment.scheduledDateTimeEnd, Less<Required<FSAppointment.scheduledDateTimeEnd>>,
        //            And<
        //                FSServiceOrder.branchID, Equal<Required<FSServiceOrder.branchID>>>>>,
        //            Aggregate<
        //                GroupBy<EPEmployee.bAccountID>>,
        //            OrderBy<
        //                Asc<EPEmployee.acctName>>> EmployeeAppointmentsByDate;

        //        public 
        //            PXSelectJoinGroupBy<EPEmployee,
        //            InnerJoin<FSAppointmentEmployee,
        //                On<
        //                    FSAppointmentEmployee.employeeID, Equal<EPEmployee.bAccountID>>,
        //            InnerJoin<FSAppointment,
        //                On<
        //                    FSAppointment.appointmentID, Equal<FSAppointmentEmployee.appointmentID>>,
        //            InnerJoin<FSServiceOrder,
        //                On<
        //                    FSServiceOrder.sOID, Equal<FSAppointment.sOID>>,
        //            LeftJoin<BAccount,
        //                On<
        //                    BAccount.bAccountID, Equal<EPEmployee.bAccountID>>>>>>,
        //            Where<
        //                FSAppointment.scheduledDateTimeBegin, GreaterEqual<Required<FSAppointment.scheduledDateTimeBegin>>,
        //            And<
        //                FSAppointment.scheduledDateTimeEnd, Less<Required<FSAppointment.scheduledDateTimeEnd>>,
        //            And<
        //                FSServiceOrder.branchID, Equal<Required<FSServiceOrder.branchID>>,
        //            And<
        //                FSServiceOrder.branchLocationID, Equal<Required<FSServiceOrder.branchLocationID>>>>>>,
        //            Aggregate<
        //                GroupBy<EPEmployee.bAccountID>>,
        //            OrderBy<
        //                Asc<EPEmployee.acctName>>> EmployeeAppointmentsByDateAndBranchLocation;

        //        public 
        //            PXSelectJoin<FSRouteDocument, 
        //            InnerJoin<FSRoute,
        //                On<
        //                    FSRoute.routeID, Equal<FSRouteDocument.routeID>>,
        //            InnerJoin<EPEmployee,
        //                On<
        //                    EPEmployee.bAccountID, Equal<FSRouteDocument.driverID>>,
        //            LeftJoin<BAccount,
        //                On<
        //                    BAccount.bAccountID, Equal<EPEmployee.bAccountID>>>>>,
        //            Where<
        //                FSRouteDocument.date, GreaterEqual<Required<FSRouteDocument.date>>,
        //            And<
        //                FSRouteDocument.date, Less<Required<FSRouteDocument.date>>,
        //            And<
        //                FSRouteDocument.branchID, Equal<Required<FSRouteDocument.branchID>>>>>,
        //            OrderBy<
        //                Asc<FSRouteDocument.timeBegin>>> RoutesAndDriversByDate;
                    
        //     #endregion

        //    #region SODetServices
        //        public PXSelect<FSSODet,
        //                Where<
        //                    FSSODet.sOID, Equal<Required<FSSODet.sOID>>,
        //                    And<FSSODet.lineType, Equal<ListField_LineType_Service_ServiceTemplate.Service>>>> SODetServices;
							
        //        public PXSelect<FSSODet,
        //                        Where2<
        //                            Where<
        //                                    FSSODet.sOID, Equal<Required<FSSODet.sOID>>,
        //                                And<
        //                                    FSSODet.lineType, Equal<ListField_LineType_Service_ServiceTemplate.Service>>>,
        //                            And<
        //                                Where<
        //                                        FSSODet.status, Equal<FSSODet.status.Open>,
        //                                    Or<
        //                                        FSSODet.status, Equal<FSSODet.status.InProcess>>>>>> SODetPendingLines;
        //    #endregion
            
        //    #region ServiceOrder
        //        public PXSelect<FSServiceOrder,
        //                Where<
        //                    FSServiceOrder.sOID, Equal<Required<FSServiceOrder.sOID>>>> ServiceOrderRecord;
        //    #endregion
            
        //    #region Appointment
        //        public PXSelect<FSAppointment,
        //                Where<
        //                    FSAppointment.appointmentID, Equal<Required<FSAppointment.appointmentID>>>> AppointmentRecord;

        //        public PXSelect<FSAppointmentEmployee,
        //                Where<
        //                    FSAppointmentEmployee.appointmentID, Equal<Required<FSAppointmentEmployee.appointmentID>>,
        //                And<
        //                    FSAppointmentEmployee.employeeID, Equal<Required<FSAppointmentEmployee.employeeID>>>>> AppointmentEmployee; 

        //        public PXSelectJoin<EPEmployee,
        //                        InnerJoin<FSAppointmentEmployee,
        //                            On<
        //                                FSAppointmentEmployee.employeeID, Equal<EPEmployee.bAccountID>>>,
        //                        Where<
        //                            FSAppointmentEmployee.appointmentID, Equal<Required<FSAppointmentEmployee.appointmentID>>>> AppointmentEPEmployees;

        //        public PXSelectJoin<BAccountStaffMember,
        //                        InnerJoin<FSAppointmentEmployee,
        //                            On<
        //                                FSAppointmentEmployee.employeeID, Equal<BAccountStaffMember.bAccountID>>>,
        //                        Where<
        //                            FSAppointmentEmployee.appointmentID, Equal<Required<FSAppointmentEmployee.appointmentID>>>> AppointmentBAccountStaffMember;

        //        public PXSelectJoin<FSAppointmentDet,
        //                    InnerJoin<FSSODet,
        //                        On<
        //                            FSSODet.sODetID, Equal<FSAppointmentDet.sODetID>>>,
        //                    Where<
        //                        FSSODet.lineType, Equal<ListField_LineType_Service_ServiceTemplate.Service>,
        //                        And<FSAppointmentDet.sODetID, Equal<Required<FSAppointmentDet.sODetID>>>>> AppointmentDets;

        //        public PXSelectJoin<FSAppointmentDet,
        //                        InnerJoin<FSAppointment,
        //                                On<FSAppointment.appointmentID, Equal<FSAppointmentDet.appointmentID>>>,
        //                        Where<
        //                            FSAppointmentDet.sODetID, Equal<Required<FSAppointmentDet.sODetID>>,
        //                        And<
        //                            Where<FSAppointment.status, Equal<FSAppointment.status.ManualScheduled>,
        //                                Or<FSAppointment.status, Equal<FSAppointment.status.AutomaticScheduled>,
        //                                Or<FSAppointment.status, Equal<FSAppointment.status.InProcess>>>>>>> ActiveAppointmentDets;

        //        public PXSelectJoin<InventoryItem,
        //                        InnerJoin<FSSODet,
        //                            On<
        //                                FSSODet.serviceID, Equal<InventoryItem.inventoryID>>,
        //                        InnerJoin<FSAppointmentDet,
        //                            On<
        //                                FSAppointmentDet.sODetID, Equal<FSSODet.sODetID>>,
        //                        LeftJoin<INItemClass, 
        //                            On<
        //                                INItemClass.itemClassID, Equal<InventoryItem.itemClassID>>>>>,
        //                        Where<
        //                            FSSODet.lineType, Equal<ListField_LineType_Service_ServiceTemplate.Service>,
        //                            And<FSAppointmentDet.appointmentID, Equal<Required<FSAppointmentDet.appointmentID>>>>> AppointmentServices;

        //        public PXSelectJoin<EPEquipment,
        //                        InnerJoin<FSAppointmentResource,
        //                            On<
        //                                FSAppointmentResource.resourceID, Equal<EPEquipment.equipmentID>>>,
        //                        Where<
        //                            FSAppointmentResource.appointmentID, Equal<Required<FSAppointmentResource.appointmentID>>>> AppointmentResources;

        //        public PXSelectJoin<EPEmployee,
        //                    InnerJoin<FSAppointmentEmployee,
        //                        On<
        //                            FSAppointmentEmployee.employeeID, Equal<EPEmployee.bAccountID>>,
        //                    InnerJoin<FSAppointment,
        //                        On<
        //                            FSAppointment.appointmentID, Equal<FSAppointmentEmployee.appointmentID>>,
        //                    InnerJoin<FSServiceOrder,
        //                        On<
        //                            FSServiceOrder.sOID, Equal<FSAppointment.sOID>>,
        //                    LeftJoin<Location,
        //                        On<
        //                            Location.locationID, Equal<FSServiceOrder.locationID>>,
        //                    LeftJoin<Address,
        //                        On<
        //                            Address.addressID, Equal<Location.defAddressID>>,
        //                    LeftJoin<Country,
        //                        On
        //                            <Country.countryID, Equal<Address.countryID>>,
        //                    LeftJoin<State,
        //                        On<
        //                            State.countryID, Equal<Address.countryID>,
        //                            And<State.stateID, Equal<Address.state>>>>>>>>>>,
        //                    Where<
        //                        FSServiceOrder.status, NotEqual<FSServiceOrder.status.Canceled>,
        //                    And<
        //                        FSAppointmentEmployee.employeeID, Equal<Required<FSAppointmentEmployee.employeeID>>>>> AndroidAppointments;

        //        public PXSelect<FSAppointmentAttendee,
        //                    Where<
        //                        FSAppointmentAttendee.appointmentID, Equal<Required<FSAppointmentAttendee.appointmentID>>>> AppointmentAttendees;

        //        public PXSelectJoin<EPEmployee,
        //                        InnerJoin<FSAppointmentEmployee,
        //                            On<
        //                                FSAppointmentEmployee.employeeID, Equal<EPEmployee.bAccountID>>,
        //                        InnerJoin<FSAppointment,
        //                            On<
        //                                FSAppointment.appointmentID, Equal<FSAppointmentEmployee.appointmentID>>,
        //                        InnerJoin<FSServiceOrder,
        //                            On<
        //                                FSServiceOrder.sOID, Equal<FSAppointment.sOID>>,
        //                        InnerJoin<FSSrvOrdType,
        //                            On<
        //                                FSSrvOrdType.srvOrdType, Equal<FSServiceOrder.srvOrdType>>,
        //                        InnerJoin<BAccountStaffMember,
        //                            On<
        //                                BAccountStaffMember.bAccountID, Equal<EPEmployee.bAccountID>>,
        //                        LeftJoin<Location,
        //                            On<
        //                                Location.locationID, Equal<FSServiceOrder.locationID>>,
        //                        LeftJoin<Address,
        //                            On<
        //                                Address.addressID, Equal<Location.defAddressID>>,
        //                        LeftJoin<Country,
        //                            On
        //                                <Country.countryID, Equal<Address.countryID>>,
        //                        LeftJoin<State,
        //                            On<
        //                                State.countryID, Equal<Address.countryID>,
        //                                And<State.stateID, Equal<Address.state>>>,
        //                        LeftJoin<FSCustomer,
        //                            On<
        //                                FSCustomer.bAccountID, Equal<FSServiceOrder.customerID>>>>>>>>>>>>,
        //                        Where<
        //                            FSServiceOrder.status, NotEqual<FSServiceOrder.status.Canceled>,
        //                        And<
        //                            FSAppointment.scheduledDateTimeBegin, GreaterEqual<Required<FSAppointment.scheduledDateTimeBegin>>,
        //                        And<
        //                            FSAppointment.scheduledDateTimeEnd, Less<Required<FSAppointment.scheduledDateTimeEnd>>,
        //                        And<
        //                            FSAppointmentEmployee.employeeID, Equal<Required<FSAppointmentEmployee.employeeID>>>>>>,
        //                        OrderBy<
        //                            Asc<FSAppointment.scheduledDateTimeBegin>>> AppointmentsByEmployee;

        //        public PXSelect<FSAppointment,
        //                        Where<
        //                            FSAppointment.status, Equal<FSAppointment.status.Closed>,
        //                        And<
        //                            FSAppointment.sOID, Equal<Required<FSAppointment.sOID>>>>> SDAppointmentsCloseBySO;
        //     #endregion

        //    #region Room
        //        public PXSelect<FSRoom,
        //                Where<
        //                    FSRoom.branchLocationID, Equal<Required<FSRoom.branchLocationID>>>,
        //                OrderBy<
        //                    Asc<FSRoom.descr>>> BranchLocationRooms;

        //        public PXSelect<FSRoom,
        //                Where<
        //                    FSRoom.branchLocationID, Equal<Required<FSRoom.branchLocationID>>,
        //                    And<FSRoom.roomID, Equal<Required<FSRoom.roomID>>>>,
        //                OrderBy<
        //                    Asc<FSRoom.descr>>> BranchLocationRoom;
        //    #endregion

        //    #region ServiceOrderType
        //        public PXSelect<FSSrvOrdType,
        //            Where<
        //                FSSrvOrdType.srvOrdType, Equal<Required<FSSrvOrdType.srvOrdType>>>> SrvOrdTypeRecord;
        //    #endregion
            
        //    #region CalendarPreferences
        //       #region CustomFieldAppointment
        //            public PXSelect<FSCustomFieldAppointment,
        //                    Where<
        //                        FSCustomFieldAppointment.customFieldAppointmentID, Equal<Required<FSCustomFieldAppointment.customFieldAppointmentID>>>> CustomFieldAppointment;

        //            public PXSelectOrderBy<FSCustomFieldAppointment,
        //                    OrderBy<
        //                        Asc<FSCustomFieldAppointment.position>>> CustomFieldAppointments;

        //            public PXSelect<FSCustomFieldAppointment,
        //                    Where<
        //                        FSCustomFieldAppointment.active, Equal<Required<FSCustomFieldAppointment.active>>>,
        //                    OrderBy<
        //                        Asc<FSCustomFieldAppointment.position>>> ActiveCustomFieldAppointments;
        //        #endregion
        //       #region CustomAppointmentStatus
        //            public PXSelect<FSCustomAppointmentStatus> CustomAppointmentStatuses;

        //            public PXSelect<FSCustomAppointmentStatus,
        //                    Where<
        //                        FSCustomAppointmentStatus.customAppointmentStatusID, Equal<Required<FSCustomAppointmentStatus.customAppointmentStatusID>>>> CustomAppointmentStatus;

        //            public PXSelect<FSCustomAppointmentStatus,
        //                    Where<
        //                         FSCustomAppointmentStatus.fieldName, Equal<Required<FSCustomAppointmentStatus.fieldName>>>> CustomAppointmentStatusName;
        //        #endregion
        //    #endregion

        //    #region TimeSlot
        //        public PXSelect<FSTimeSlot,
        //                Where<
        //                    FSTimeSlot.timeSlotID, Equal<Required<FSTimeSlot.timeSlotID>>>> TimeSlotRecord;
        //    #endregion
        //#endregion

        //#region RouteSelect
        //    #region Route
        //        public PXSelectJoin<FSRouteDocument,
        //                    LeftJoin<FSShift,
        //                        On<
        //                            FSShift.shiftID, Equal<FSRouteDocument.shiftID>>,
        //                    LeftJoin<FSRoute,
        //                        On<
        //                            FSRoute.routeID, Equal<FSRouteDocument.routeID>>,
        //                    LeftJoin<EPEquipment,
        //                        On<
        //                            EPEquipment.equipmentID, Equal<FSRouteDocument.vehicleID>>,
        //                    LeftJoin<EPEmployee,
        //                        On<
        //                            EPEmployee.bAccountID, Equal<FSRouteDocument.driverID>>>>>>,
        //                    Where<
        //                            FSRouteDocument.date, Equal<Required<FSRouteDocument.date>>,
        //                        And<
        //                            FSRouteDocument.branchID, Equal<Required<FSRouteDocument.branchID>>>>> RouteRecordsByDate;

        //        public PXSelectJoin<FSRouteDocument,
        //                    LeftJoin<FSShift,
        //                        On<
        //                            FSShift.shiftID, Equal<FSRouteDocument.shiftID>>,
        //                    LeftJoin<FSRoute,
        //                        On<
        //                            FSRoute.routeID, Equal<FSRouteDocument.routeID>>,
        //                    LeftJoin<EPEquipment,
        //                        On<
        //                            EPEquipment.equipmentID, Equal<FSRouteDocument.vehicleID>>,
        //                    LeftJoin<EPEmployee,
        //                        On<
        //                            EPEmployee.bAccountID, Equal<FSRouteDocument.driverID>>>>>>,
        //                    Where<
        //                            FSRouteDocument.date, Equal<Required<FSRouteDocument.date>>,
        //                        And<
        //                            FSRouteDocument.refNbr, Equal<Required<FSRouteDocument.refNbr>>,
        //                        And<
        //                            FSRouteDocument.branchID, Equal<Required<FSRouteDocument.branchID>>>>>> RouteRecordsByDateAndRefNbr;
        //    #endregion

        //    #region AppointmentRoute
        //        public PXSelectJoin<FSAppointment,
        //                        InnerJoin<FSServiceOrder,
        //                            On<
        //                                FSServiceOrder.sOID, Equal<FSAppointment.sOID>>,
        //                        LeftJoin<FSSrvOrdType,
        //                            On<
        //                                FSSrvOrdType.srvOrdType, Equal<FSServiceOrder.srvOrdType>>,
        //                        LeftJoin<Location,
        //                            On<
        //                                Location.locationID, Equal<FSServiceOrder.locationID>>,
        //                        LeftJoin<Address,
        //                            On<
        //                                Address.addressID, Equal<Location.defAddressID>>,
        //                        LeftJoin<Country,
        //                            On
        //                                <Country.countryID, Equal<Address.countryID>>,
        //                        LeftJoin<State,
        //                            On<
        //                                State.countryID, Equal<Address.countryID>,
        //                                And<State.stateID, Equal<Address.state>>>,
        //                        LeftJoin<FSCustomer,
        //                            On<
        //                                FSCustomer.bAccountID, Equal<FSServiceOrder.customerID>>>>>>>>>,
        //                        Where<
        //                            FSServiceOrder.status, NotEqual<FSServiceOrder.status.Canceled>,
        //                        And<
        //                            FSAppointment.routeDocumentID, Equal<Required<FSRouteDocument.routeDocumentID>>>>,
        //                        OrderBy<
        //                            Asc<FSAppointment.scheduledDateTimeBegin>>> AppointmentRecordsByRoute;
        //    #endregion
        //#endregion

        //#region PrivateFunctions
        //    public FSAppointment GetCalendarAppointment(
        //                                                FSAppointment fsAppointmentRow, 
        //                                                FSAppointmentEmployee fsAppointmentEmployeeRow, 
        //                                                FSServiceOrder fsServiceOrderRow, 
        //                                                Customer customerRow, 
        //                                                Location locationRow, 
        //                                                Contact contactRow,
        //                                                FSWFStage wfStageRow) 
        //    {
        //        fsAppointmentRow.SORefNbr = fsServiceOrderRow.RefNbr;
        //        fsAppointmentRow.SrvOrdType = fsServiceOrderRow.SrvOrdType;
        //        fsAppointmentRow.Mem_BranchLocationID = fsServiceOrderRow.BranchLocationID;
        //        fsAppointmentRow.Mem_RoomID = fsServiceOrderRow.RoomID;
        //        fsAppointmentRow.CustomRoomID = fsServiceOrderRow.BranchLocationID.ToString() + "-" + fsServiceOrderRow.RoomID;

        //        PXResultset<InventoryItem> inventoryItemRows = this.AppointmentServices.Select(fsAppointmentRow.AppointmentID);
        //        if (inventoryItemRows != null && inventoryItemRows.Count > 0)
        //        {
        //            InventoryItem inventoryItemRow = inventoryItemRows;
        //            fsAppointmentRow.FirstServiceDesc = inventoryItemRow.Descr;
        //        }

        //        FSRoom fsRoomRow = this.BranchLocationRoom.Select(fsServiceOrderRow.BranchLocationID, fsServiceOrderRow.RoomID);
        //        if (fsRoomRow != null)
        //        {
        //            fsAppointmentRow.RoomDesc = fsRoomRow.Descr;
        //        }

        //        if (fsAppointmentEmployeeRow != null)
        //        {
        //            fsAppointmentRow.CustomID = fsAppointmentRow.AppointmentID.ToString() + fsAppointmentEmployeeRow.EmployeeID.ToString();
        //            fsAppointmentRow.EmployeeID = fsAppointmentEmployeeRow.EmployeeID;
        //            fsAppointmentRow.OldEmployeeID = fsAppointmentEmployeeRow.EmployeeID;
        //        }

        //        var fsAppointmentEmployeeRows = (PXResultset<FSAppointmentEmployee>)this.SDAppointmentEmployees
        //                        .Select(fsAppointmentRow.AppointmentID);

        //        var fsAppointmentAttendeeRows = (PXResultset<FSAppointmentAttendee>)this.AppointmentAttendees
        //                        .Select(fsAppointmentRow.AppointmentID);

        //        fsAppointmentRow.EmployeeCount = fsAppointmentEmployeeRows.Count;
        //        fsAppointmentRow.AttendeeCount = fsAppointmentAttendeeRows.Count;

        //        fsAppointmentRow.ContactName = contactRow.DisplayName;
        //        fsAppointmentRow.ContactEmail = contactRow.EMail;
        //        fsAppointmentRow.ContactPhone = contactRow.Phone1;
        //        fsAppointmentRow.CustomerName = customerRow.AcctName;
        //        fsAppointmentRow.LocationDesc = locationRow.Descr;
        //        fsAppointmentRow.WFStageCD = wfStageRow.WFStageCD;

        //        return fsAppointmentRow;
        //    }

        //    public List<FSAppointment> GetAppointmentRecords(DateTime timeBegin, DateTime timeEnd, int branchID, int? branchLocationID, int[] employeeIDList)
        //    {
        //        List<object> args = new List<object>();
        //        List<FSAppointment> appointmentList = new List<FSAppointment>();

        //        BqlCommand appointmentRecords = new Select2<FSAppointmentEmployee,
        //                                                InnerJoin<FSAppointment,
        //                                                    On<FSAppointment.appointmentID, Equal<FSAppointmentEmployee.appointmentID>>,
        //                                                InnerJoin<FSServiceOrder,
        //                                                    On<FSServiceOrder.sOID, Equal<FSAppointment.sOID>>,
        //                                                LeftJoin<Customer,
        //                                                    On<Customer.bAccountID, Equal<FSServiceOrder.customerID>>,
        //                                                LeftJoin<Location,
        //                                                    On<Location.locationID, Equal<FSServiceOrder.locationID>>,
        //                                                LeftJoin<Contact,
        //                                                    On<Contact.contactID, Equal<FSServiceOrder.contactID>>,
        //                                                LeftJoin<FSWFStage,
        //                                                    On<FSWFStage.wFStageID, Equal<FSAppointment.wFStageID>>>>>>>>,
        //                                                Where<
        //                                                    FSServiceOrder.status, NotEqual<FSServiceOrder.status.Canceled>,
        //                                                    And<FSServiceOrder.branchID, Equal<Required<FSServiceOrder.branchID>>,
        //                                                    And<Where<
        //                                                            Where2<
        //                                                                Where<FSAppointment.scheduledDateTimeBegin, GreaterEqual<Required<FSAppointment.scheduledDateTimeBegin>>,
        //                                                                    And<FSAppointment.scheduledDateTimeBegin, LessEqual<Required<FSAppointment.scheduledDateTimeEnd>>>>,
        //                                                            Or<
        //                                                                Where<FSAppointment.scheduledDateTimeEnd, GreaterEqual<Required<FSAppointment.scheduledDateTimeBegin>>,
        //                                                                    And<FSAppointment.scheduledDateTimeEnd, LessEqual<Required<FSAppointment.scheduledDateTimeEnd>>>>>>>>>>,
        //                                                OrderBy<
        //                                                    Asc<FSAppointment.appointmentID>>>();
        //        args.Add(branchID);
        //        args.Add(timeBegin);
        //        args.Add(timeEnd);
        //        args.Add(timeBegin);
        //        args.Add(timeEnd);
                
        //        if (branchLocationID != null)
        //        {
        //            appointmentRecords = appointmentRecords.WhereAnd(typeof(Where<FSServiceOrder.branchLocationID, Equal<Required<FSServiceOrder.branchLocationID>>>));
        //            args.Add(branchLocationID);
        //        }

        //        if (employeeIDList != null && employeeIDList.Length > 0)
        //        {
        //            appointmentRecords = appointmentRecords.WhereAnd(InHelper<FSAppointmentEmployee.employeeID>.Create(employeeIDList.Length));
        //            foreach (int employeeID in employeeIDList)
        //            {
        //                args.Add(employeeID);
        //            }
        //        }

        //        PXResultset<FSCustomAppointmentStatus> fsCustomAppointmentStatusRows = PXSelect<FSCustomAppointmentStatus,
        //                                                                                    Where<
        //                                                                                        FSCustomAppointmentStatus.hideStatus, Equal<False>>
        //                                                                                    >.Select(this);

        //        List<string> appointmentStatusesList = new List<string>();

        //        foreach (FSCustomAppointmentStatus fsCustomAppointmentStatusRow in fsCustomAppointmentStatusRows)
        //        {
        //            if (fsCustomAppointmentStatusRow.FieldName == TX.Status_Appointment.CANCELED && fsCustomAppointmentStatusRow.HideStatus == false)
        //            {
        //                appointmentStatusesList.Add(ID.Status_Appointment.CANCELED);
        //            }

        //            if (fsCustomAppointmentStatusRow.FieldName == TX.Status_Appointment.COMPLETED && fsCustomAppointmentStatusRow.HideStatus == false)
        //            {
        //                appointmentStatusesList.Add(ID.Status_Appointment.COMPLETED);
        //            }

        //            if (fsCustomAppointmentStatusRow.FieldName == TX.Status_Appointment.IN_PROCESS && fsCustomAppointmentStatusRow.HideStatus == false)
        //            {
        //                appointmentStatusesList.Add(ID.Status_Appointment.IN_PROCESS);
        //            }

        //            if (fsCustomAppointmentStatusRow.FieldName == TX.Status_Appointment.SCHEDULED && fsCustomAppointmentStatusRow.HideStatus == false)
        //            {
        //                appointmentStatusesList.Add(ID.Status_Appointment.MANUAL_SCHEDULED);
        //                appointmentStatusesList.Add(ID.Status_Appointment.AUTOMATIC_SCHEDULED);
        //            }

        //            if (fsCustomAppointmentStatusRow.FieldName == TX.Status_Appointment.CLOSED && fsCustomAppointmentStatusRow.HideStatus == false)
        //            {
        //                appointmentStatusesList.Add(ID.Status_Appointment.CLOSED);
        //            }
        //        }

        //        if (appointmentStatusesList.Count > 0)
        //        {
        //            appointmentRecords = appointmentRecords.WhereAnd(InHelper<FSAppointment.status>.Create(appointmentStatusesList.Count));
        //        }

        //        foreach (string appointmentStatus in appointmentStatusesList)
        //        {
        //            args.Add(appointmentStatus);
        //        }

        //        PXView appointmentRecordsView = new PXView(this, true, appointmentRecords);
        //        var results = appointmentRecordsView.SelectMulti(args.ToArray());

        //        foreach (PXResult<FSAppointmentEmployee, FSAppointment, FSServiceOrder, Customer, Location, Contact, FSWFStage> result in results)
        //        {
        //            FSAppointment fsAppointmentRow = result;
        //            FSAppointmentEmployee fsAppointmentEmployeeRow = result;
        //            FSServiceOrder fsServiceOrderRow = result;
        //            Customer customerRow = result;
        //            Location locationRow = result;
        //            Contact contactRow = result;
        //            FSWFStage wfStageRow = result;

        //            appointmentList.Add(this.GetCalendarAppointment(fsAppointmentRow, fsAppointmentEmployeeRow, fsServiceOrderRow, customerRow, locationRow, contactRow, wfStageRow));
        //        }

        //        return appointmentList;
        //    }
            
        //    /// <summary>
        //    /// Gets the appointment records related for the given dates.
        //    /// </summary>
        //    /// <param name="timeBegin">Schedule Start Date.</param>
        //    /// <param name="timeEnd">Schedule End Date.</param>
        //    /// <param name="branchID">Current Branch ID.</param>
        //    /// <param name="branchLocationID">Branch Location ID.</param>
        //    /// <param name="roomIDList">Room id list.</param>
        //    /// <returns>Appointment list.</returns>
        //    public List<FSAppointment> GetAppointmentRecordsByRooms(DateTime timeBegin, DateTime timeEnd, int branchID, int? branchLocationID, int[] roomIDList)
        //    {
        //        List<object> args = new List<object>();
        //        List<FSAppointment> appointmentList = new List<FSAppointment>();

        //        BqlCommand appointmentRecords = new Select2<FSAppointment,
        //                                            InnerJoin<FSServiceOrder,
        //                                                On<FSServiceOrder.sOID, Equal<FSAppointment.sOID>>,
        //                                            LeftJoin<Customer,
        //                                                On<Customer.bAccountID, Equal<FSServiceOrder.customerID>>,
        //                                            LeftJoin<Location,
        //                                                On<Location.locationID, Equal<FSServiceOrder.locationID>>,
        //                                            LeftJoin<Contact,
        //                                                On<Contact.contactID, Equal<FSServiceOrder.contactID>>,
        //                                            LeftJoin<FSWFStage,
        //                                                On<FSWFStage.wFStageID, Equal<FSAppointment.wFStageID>>>>>>>,
        //                                            Where<
        //                                                FSServiceOrder.status, NotEqual<FSServiceOrder.status.Canceled>,
        //                                                And<FSServiceOrder.branchID, Equal<Required<FSServiceOrder.branchID>>,
        //                                                And<Where<
        //                                                        Where2<
        //                                                            Where<FSAppointment.scheduledDateTimeBegin, GreaterEqual<Required<FSAppointment.scheduledDateTimeBegin>>,
        //                                                                And<FSAppointment.scheduledDateTimeBegin, LessEqual<Required<FSAppointment.scheduledDateTimeEnd>>>>,
        //                                                        Or<
        //                                                            Where<FSAppointment.scheduledDateTimeEnd, GreaterEqual<Required<FSAppointment.scheduledDateTimeBegin>>,
        //                                                                And<FSAppointment.scheduledDateTimeEnd, LessEqual<Required<FSAppointment.scheduledDateTimeEnd>>>>>>>>>>,
        //                                            OrderBy<
        //                                                Asc<FSAppointment.appointmentID>>>();
        //        args.Add(branchID);
        //        args.Add(timeBegin);
        //        args.Add(timeEnd);
        //        args.Add(timeBegin);
        //        args.Add(timeEnd);

        //        if (branchLocationID != null)
        //        {
        //            appointmentRecords = appointmentRecords.WhereAnd(typeof(Where<FSServiceOrder.branchLocationID, Equal<Required<FSServiceOrder.branchLocationID>>>));
        //            args.Add(branchLocationID);
        //        }

        //        if (roomIDList != null && roomIDList.Length > 0)
        //        {
        //            appointmentRecords = appointmentRecords.WhereAnd(InHelper<FSServiceOrder.roomID>.Create(roomIDList.Length));
        //            foreach (int employeeID in roomIDList)
        //            {
        //                args.Add(employeeID);
        //            }
        //        }

        //        PXResultset<FSCustomAppointmentStatus> fsCustomAppointmentStatusRows = PXSelect<FSCustomAppointmentStatus,
        //                                                                               Where<
        //                                                                                   FSCustomAppointmentStatus.hideStatus, Equal<False>>
        //                                                                               >.Select(this);

        //        List<string> appointmentStatusesList = new List<string>();

        //        foreach (FSCustomAppointmentStatus fsCustomAppointmentStatusRow in fsCustomAppointmentStatusRows)
        //        {
        //            if (fsCustomAppointmentStatusRow.FieldName == TX.Status_Appointment.CANCELED && fsCustomAppointmentStatusRow.HideStatus == false)
        //            {
        //                appointmentStatusesList.Add(ID.Status_Appointment.CANCELED);
        //            }

        //            if (fsCustomAppointmentStatusRow.FieldName == TX.Status_Appointment.COMPLETED && fsCustomAppointmentStatusRow.HideStatus == false)
        //            {
        //                appointmentStatusesList.Add(ID.Status_Appointment.COMPLETED);
        //            }

        //            if (fsCustomAppointmentStatusRow.FieldName == TX.Status_Appointment.IN_PROCESS && fsCustomAppointmentStatusRow.HideStatus == false)
        //            {
        //                appointmentStatusesList.Add(ID.Status_Appointment.IN_PROCESS);
        //            }

        //            if (fsCustomAppointmentStatusRow.FieldName == TX.Status_Appointment.SCHEDULED && fsCustomAppointmentStatusRow.HideStatus == false)
        //            {
        //                appointmentStatusesList.Add(ID.Status_Appointment.MANUAL_SCHEDULED);
        //                appointmentStatusesList.Add(ID.Status_Appointment.AUTOMATIC_SCHEDULED);
        //            }

        //            if (fsCustomAppointmentStatusRow.FieldName == TX.Status_Appointment.CLOSED && fsCustomAppointmentStatusRow.HideStatus == false)
        //            {
        //                appointmentStatusesList.Add(ID.Status_Appointment.CLOSED);
        //            }
        //        }

        //        if (appointmentStatusesList.Count > 0)
        //        {
        //            appointmentRecords = appointmentRecords.WhereAnd(InHelper<FSAppointment.status>.Create(appointmentStatusesList.Count));
        //        }

        //        foreach (string appointmentStatus in appointmentStatusesList)
        //        {
        //            args.Add(appointmentStatus);
        //        }

        //        PXView appointmentRecordsView = new PXView(this, true, appointmentRecords);
        //        var results = appointmentRecordsView.SelectMulti(args.ToArray());

        //        foreach (PXResult<FSAppointment, FSServiceOrder, Customer, Location, Contact, FSWFStage> result in results)
        //        {
        //            FSAppointment fsAppointmentRow = result;
        //            FSServiceOrder fsServiceOrderRow = result;
        //            Customer customerRow = result;
        //            Location locationRow = result;
        //            Contact contactRow = result;
        //            FSWFStage wfStageRow = result;

        //            appointmentList.Add(this.GetCalendarAppointment(fsAppointmentRow, null, fsServiceOrderRow, customerRow, locationRow, contactRow, wfStageRow));
        //        }

        //        return appointmentList;
        //    }

        //    public List<FSTimeSlot> GetWorkingScheduleRecords(DateTime timeBegin, DateTime timeEnd, int branchID, int? branchLocationID, bool compressSlot, int[] employeeIDList)
        //    {
        //        List<object> args = new List<object>();
        //        List<FSTimeSlot> workingScheduleList = new List<FSTimeSlot>();

        //        BqlCommand workingScheduleRecords = new Select2<FSTimeSlot,
        //                                                    LeftJoin<FSBranchLocation,
        //                                                        On<FSBranchLocation.branchLocationID, Equal<FSTimeSlot.branchLocationID>>>,
        //                                                    Where<
        //                                                        FSTimeSlot.branchID, Equal<Required<FSTimeSlot.branchID>>,
        //                                                        And<FSTimeSlot.timeStart, GreaterEqual<Required<FSTimeSlot.timeStart>>,
        //                                                        And<FSTimeSlot.timeEnd, Less<Required<FSTimeSlot.timeEnd>>,
        //                                                        And<FSTimeSlot.slotLevel, Equal<Required<FSTimeSlot.slotLevel>>>>>>,
        //                                                    OrderBy<
        //                                                        Asc<FSAppointment.appointmentID>>>();
        //        args.Add(branchID);
        //        args.Add(timeBegin);
        //        args.Add(timeEnd);
        //        args.Add(compressSlot ? 1 : 0);

        //        if (branchLocationID != null)
        //        {
        //            workingScheduleRecords = workingScheduleRecords.WhereAnd(typeof(Where<FSTimeSlot.branchLocationID, Equal<Required<FSTimeSlot.branchLocationID>>,
        //                                                                                Or<FSTimeSlot.branchLocationID, IsNull>>));
        //            args.Add(branchLocationID);
        //        }
        //        else 
        //        {
        //            PXResultset<FSBranchLocation> fsBranchLocationRows = this.BranchLocationRecordsByBranch.Select(branchID);
        //            if (fsBranchLocationRows.Count > 0 && compressSlot)
        //            {
        //                workingScheduleRecords = workingScheduleRecords.WhereAnd(InHelper<FSTimeSlot.branchLocationID>.Create(fsBranchLocationRows.Count));
        //                foreach (FSBranchLocation fsBranchLocationRow in fsBranchLocationRows)
        //                {
        //                    args.Add(fsBranchLocationRow.BranchLocationID);
        //                }
        //            }
        //        }

        //        if (employeeIDList != null && employeeIDList.Length > 0)
        //        {
        //            workingScheduleRecords = workingScheduleRecords.WhereAnd(InHelper<FSTimeSlot.employeeID>.Create(employeeIDList.Length));
        //            foreach (int employeeID in employeeIDList)
        //            {
        //                args.Add(employeeID);
        //            }
        //        }

        //        PXView workingScheduleRecordsView = new PXView(this, true, workingScheduleRecords);
        //        var results = workingScheduleRecordsView.SelectMulti(args.ToArray());
        //        DateTime? minTimeBegin = null;
        //        DateTime? maxTimeEnd = null;
        //        int count = 0;

        //        foreach (PXResult<FSTimeSlot, FSBranchLocation> result in results)
        //        {
        //            FSTimeSlot fsTimeSlotRow = result;
        //            FSBranchLocation fsBranchLocationRow = result;

        //            fsTimeSlotRow.CustomID = fsTimeSlotRow.TimeStart.Value.Month.ToString("00") +
        //                                        fsTimeSlotRow.TimeStart.Value.Day.ToString("00") +
        //                                        fsTimeSlotRow.TimeStart.Value.Year.ToString("0000");

        //            if (fsBranchLocationRow != null)
        //            {
        //                fsTimeSlotRow.BranchLocationDesc = fsBranchLocationRow.Descr;
        //                fsTimeSlotRow.BranchLocationCD = fsBranchLocationRow.BranchLocationCD;
        //            }

        //            fsTimeSlotRow.WrkEmployeeScheduleID = count.ToString();

        //            ++count;

        //            if (minTimeBegin == null || minTimeBegin > fsTimeSlotRow.TimeStart)
        //            {
        //                minTimeBegin = fsTimeSlotRow.TimeStart;
        //            }

        //            if (maxTimeEnd == null || maxTimeEnd < fsTimeSlotRow.TimeEnd)
        //            {
        //                maxTimeEnd = fsTimeSlotRow.TimeEnd;
        //            }

        //            workingScheduleList.Add(fsTimeSlotRow);
        //        }

        //        return workingScheduleList;
        //    }

        //    //@TODO SD-6613
        //    public PXResultset<EPEmployee> FilterEmployeesByBranchLocation(PXResultset<EPEmployee> ePEmployeeRows, PXResultset<FSTimeSlot> fsTimeSlotRows, PXResultset<FSAppointmentEmployee> fsAppointmentEmployeeRows)
        //    {
        //        bool employeeHasRules;
        //        bool employeeHasAppointment;
        //        if (ePEmployeeRows != null)
        //        {
        //            for (int i = 0; i < ePEmployeeRows.Count; i++)
        //            {
        //                employeeHasRules = false;
        //                employeeHasAppointment = false;
        //                EPEmployee ePEmployeeRow = (EPEmployee)ePEmployeeRows[i];

        //                if (fsTimeSlotRows != null)
        //                {
        //                    for (int j = 0; j < fsTimeSlotRows.Count; j++)
        //                    {
        //                        FSTimeSlot fsTimeSlotRow = fsTimeSlotRows[j];
        //                        if (ePEmployeeRow.BAccountID == fsTimeSlotRow.EmployeeID)
        //                        {
        //                            fsTimeSlotRows.RemoveAt(j);
        //                            employeeHasRules = true;
        //                            break;
        //                        }
        //                    }
        //                }

        //                if (fsAppointmentEmployeeRows != null)
        //                {
        //                    for (int j = 0; j < fsAppointmentEmployeeRows.Count; j++)
        //                    {
        //                        FSAppointmentEmployee fsAppointmentEmployeeRow = fsAppointmentEmployeeRows[j];
        //                        if (ePEmployeeRow.BAccountID == fsAppointmentEmployeeRow.EmployeeID)
        //                        {
        //                            fsAppointmentEmployeeRows.RemoveAt(j);
        //                            employeeHasAppointment = true;
        //                            break;
        //                        }
        //                    }
        //                }

        //                if (employeeHasRules == false && employeeHasAppointment == false)
        //                {
        //                    ePEmployeeRows.RemoveAt(i--);
        //                }
        //            }
        //        }

        //        return ePEmployeeRows;
        //    }

        //    public PXResultset<EPEmployee> FilterEmployeesByBranchLocationAndScheduled(PXResultset<EPEmployee> ePEmployeeRows, PXResultset<FSTimeSlot> fsEmployeeScheduleRows, PXResultset<FSAppointmentEmployee> fsAppointmentEmployeeRows)
        //    {
        //        bool employeeHasRules;
        //        bool employeeHasAppointment;

        //        if (ePEmployeeRows != null)
        //        {
        //            for (int i = 0; i < ePEmployeeRows.Count; i++)
        //            {
        //                employeeHasRules = false;
        //                employeeHasAppointment = false;
        //                EPEmployee ePEmployeeRow = (EPEmployee)ePEmployeeRows[i];

        //                if (fsEmployeeScheduleRows != null)
        //                {
        //                    for (int j = 0; j < fsEmployeeScheduleRows.Count; j++)
        //                    {
        //                        FSTimeSlot fsEmployeeScheduleRow = fsEmployeeScheduleRows[j];
        //                        if (ePEmployeeRow.BAccountID == fsEmployeeScheduleRow.EmployeeID)
        //                        {
        //                            fsEmployeeScheduleRows.RemoveAt(j);
        //                            employeeHasRules = true;
        //                            break;
        //                        }
        //                    }
        //                }

        //                if (fsAppointmentEmployeeRows != null)
        //                {
        //                    for (int j = 0; j < fsAppointmentEmployeeRows.Count; j++)
        //                    {
        //                        FSAppointmentEmployee fsAppointmentEmployeeRow = fsAppointmentEmployeeRows[j];
        //                        if (ePEmployeeRow.BAccountID == fsAppointmentEmployeeRow.EmployeeID)
        //                        {
        //                            fsAppointmentEmployeeRows.RemoveAt(j);
        //                            employeeHasRules = true;
        //                            break;
        //                        }
        //                    }
        //                }

        //                if (employeeHasRules == false && employeeHasAppointment == false)
        //                {
        //                    ePEmployeeRows.RemoveAt(i--);
        //                }
        //            }
        //        }

        //        return ePEmployeeRows;
        //    }

        //    public PXResultset<EPEmployee> EmployeeRecords(int branchID, int? branchLocationID, bool? ignoreActiveFlag, bool? ignoreAvailabilityFlag, DateTime? calendarDate, DispatchBoardFilters[] filters)
        //    {
        //        DateTime? startDate = (calendarDate != null) ? calendarDate : Accessinfo.BusinessDate;

        //        DateHandler date = new DateHandler();
        //        date.setDate(startDate.Value);
        //        List<object> args = new List<object>();
        //        List<object> employeeScheduleArgs = new List<object>();
        //        List<object> appointmentArgs = new List<object>();

        //        var graphEmployeeMaint = PXGraph.CreateInstance<EmployeeMaint>();
        //        bool auxByScheduled = false;
        //        PXResultset<FSTimeSlot> fsEmployeeScheduleRows = null;
        //        PXResultset<FSAppointmentEmployee> fsAppointmentEmployeeRows = null;

        //        PXSelectBase<EPEmployee> employeesBase = new PXSelectJoin<EPEmployee,
        //                                                        LeftJoin<Branch,
        //                                                            On<
        //                                                                Branch.bAccountID, Equal<EPEmployee.parentBAccountID>>>,
        //                                                        Where<
        //                                                            EPEmployee.parentBAccountID, IsNotNull,
        //                                                            And<Branch.branchID, Equal<Required<Branch.branchID>>>>,
        //                                                        OrderBy<
        //                                                            Asc<EPEmployee.acctName>>>
        //                                                        (graphEmployeeMaint);
        //        args.Add(branchID);

        //        if (filters != null)
        //        {
        //            for (int i = 0; i < filters.Length; i++)
        //            {
        //                if (filters[i].property == TX.Dispatch_Board.DEFINED_SCHEDULER_FILTER)
        //                {
        //                    auxByScheduled = true;
        //                    break;
        //                }
        //            }
        //        }

        //        if (ignoreAvailabilityFlag != true)
        //        {
        //            if (auxByScheduled)
        //            {
        //                PXSelectBase<FSTimeSlot> fsTimeSlotBase = new PXSelectGroupBy<FSTimeSlot,
        //                                                                                Where<
        //                                                                                        FSTimeSlot.branchID, Equal<Required<FSTimeSlot.branchID>>,
        //                                                                                    And<
        //                                                                                        FSTimeSlot.timeStart, GreaterEqual<Required<FSTimeSlot.timeStart>>,
        //                                                                                    And<
        //                                                                                        FSTimeSlot.timeEnd, LessEqual<Required<FSTimeSlot.timeEnd>>>>>,
        //                                                                                Aggregate<Max<FSTimeSlot.employeeID, GroupBy<FSTimeSlot.employeeID>>>>(this);

        //                employeeScheduleArgs.Add(branchID);
        //                employeeScheduleArgs.Add(date.StartOfDay());
        //                employeeScheduleArgs.Add(date.EndOfDay());

        //                if (branchLocationID != null)
        //                {
        //                    fsTimeSlotBase.WhereAnd<Where<FSTimeSlot.branchLocationID, Equal<Required<FSTimeSlot.branchLocationID>>>>();
        //                    employeeScheduleArgs.Add(branchLocationID);
        //                }

        //                fsEmployeeScheduleRows = fsTimeSlotBase.Select(employeeScheduleArgs.ToArray());
        //            }
        //            else
        //            {
        //                //@TODO SD-6613
        //                PXSelectBase<FSTimeSlot> employeeScheduleBase = new PXSelectGroupBy<FSTimeSlot,
        //                                                                                Where<
        //                                                                                    FSTimeSlot.branchID, Equal<Required<FSTimeSlot.branchID>>,
        //                                                                                And<
        //                                                                                    FSTimeSlot.timeEnd, GreaterEqual<Required<FSTimeSlot.timeEnd>>>>,
        //                                                                            Aggregate<Max<FSTimeSlot.employeeID, GroupBy<FSTimeSlot.employeeID>>>>(this);

        //                employeeScheduleArgs.Add(branchID);
        //                employeeScheduleArgs.Add(startDate);

        //                if (branchLocationID != null)
        //                {
        //                    employeeScheduleBase.WhereAnd<Where<FSTimeSlot.branchLocationID, Equal<Required<FSTimeSlot.branchLocationID>>>>();
        //                    employeeScheduleArgs.Add(branchLocationID);
        //                }

        //                fsEmployeeScheduleRows = employeeScheduleBase.Select(employeeScheduleArgs.ToArray());
        //            }

        //            PXSelectBase<FSAppointmentEmployee> appointmentBase = new PXSelectJoinGroupBy<FSAppointmentEmployee,
        //                                        InnerJoin<FSAppointment,
        //                                            On<
        //                                                FSAppointment.appointmentID, Equal<FSAppointmentEmployee.appointmentID>>,
        //                                        InnerJoin<FSServiceOrder,
        //                                            On<
        //                                                FSServiceOrder.sOID, Equal<FSAppointment.sOID>>>>,
        //                                            Where2<
        //                                                Where<
        //                                                    FSServiceOrder.status, NotEqual<FSServiceOrder.status.Canceled>,
        //                                                    And<
        //                                                        FSServiceOrder.branchID, Equal<Required<FSServiceOrder.branchID>>>>,
        //                                                And<
        //                                                    Where2<
        //                                                        Where<
        //                                                            FSAppointment.scheduledDateTimeBegin, GreaterEqual<Required<FSAppointment.scheduledDateTimeBegin>>,
        //                                                            And<
        //                                                                FSAppointment.scheduledDateTimeBegin, LessEqual<Required<FSAppointment.scheduledDateTimeEnd>>>>,
        //                                                    Or<
        //                                                        Where<
        //                                                            FSAppointment.scheduledDateTimeEnd, GreaterEqual<Required<FSAppointment.scheduledDateTimeBegin>>,
        //                                                        And<
        //                                                            FSAppointment.scheduledDateTimeEnd, LessEqual<Required<FSAppointment.scheduledDateTimeEnd>>>>>>>>,
        //                                            Aggregate<Max<FSAppointmentEmployee.employeeID, GroupBy<FSAppointmentEmployee.employeeID>>>>(this);

        //            appointmentArgs.Add(branchID);
        //            appointmentArgs.Add(date.StartOfDay());
        //            appointmentArgs.Add(date.EndOfDay());
        //            appointmentArgs.Add(date.StartOfDay());
        //            appointmentArgs.Add(date.EndOfDay());

        //            if (branchLocationID != null)
        //            {
        //                appointmentBase.WhereAnd<Where<FSServiceOrder.branchLocationID, Equal<Required<FSServiceOrder.branchLocationID>>>>();
        //                appointmentArgs.Add(branchLocationID);
        //            }

        //            fsAppointmentEmployeeRows = appointmentBase.Select(appointmentArgs.ToArray());
        //        }

        //        if (!ignoreActiveFlag.HasValue || !ignoreActiveFlag.Value)
        //        {
        //            employeesBase.WhereAnd<Where<FSxEPEmployee.sDEnabled, Equal<True>>>();
        //        }

        //        PXResultset<EPEmployee> employeesToFilter;

        //        if (filters != null)
        //        {
        //            bool auxDisplayName = false;
        //            bool auxEmployeeID = false;
        //            bool auxReportsTo = false;

        //            for (int i = 0; i < filters.Length; i++)
        //            {
        //                if (filters[i].property == TX.Dispatch_Board.DISPLAY_NAME_FILTER)
        //                {
        //                    employeesBase.WhereAnd<Where<EPEmployee.acctName, Like<Required<EPEmployee.acctName>>>>();
        //                    args.Add(string.Concat("%", filters[i].value[0], "%"));
        //                    auxDisplayName = true;
        //                    if (auxEmployeeID && auxReportsTo)
        //                    {
        //                        break;
        //                    }
        //                }

        //                if (filters[i].property == TX.Dispatch_Board.EMPLOYEE_ID_FILTER)
        //                {
        //                    employeesBase.WhereAnd<Where<EPEmployee.bAccountID, Equal<Required<EPEmployee.bAccountID>>>>();
        //                    args.Add(filters[i].value[0]);
        //                    auxEmployeeID = true;
        //                    if (auxDisplayName && auxReportsTo)
        //                    {
        //                        break;
        //                    }
        //                }

        //                if (filters[i].property == TX.Dispatch_Board.REPORT_TO_EMPLOYEE_FILTER)
        //                {
        //                    employeesBase.WhereAnd<Where<EPEmployee.supervisorID, Equal<Required<EPEmployee.supervisorID>>>>();
        //                    args.Add(filters[i].value[0]);
        //                    auxReportsTo = true;
        //                    if (auxEmployeeID && auxDisplayName)
        //                    {
        //                        break;
        //                    }
        //                }
        //            }

        //            employeesToFilter = (args.Count > 0) ? employeesBase.Select(args.ToArray()) : employeesBase.Select();

        //            //Remove duplicate employees
        //            int? lastEmployeeID = -1;
        //            for (int i = 0; i < employeesToFilter.Count; i++)
        //            {
        //                EPEmployee employee = (EPEmployee)employeesToFilter[i];
        //                if (employee.BAccountID == lastEmployeeID)
        //                {
        //                    employeesToFilter.RemoveAt(i--);
        //                }

        //                lastEmployeeID = employee.BAccountID;
        //            }

        //            if (ignoreAvailabilityFlag != true)
        //            {
        //                if (auxByScheduled)
        //                {
        //                    employeesToFilter = FilterEmployeesByBranchLocationAndScheduled(employeesToFilter, fsEmployeeScheduleRows, fsAppointmentEmployeeRows);
        //                }
        //                else
        //                {
        //                    employeesToFilter = FilterEmployeesByBranchLocation(employeesToFilter, fsEmployeeScheduleRows, fsAppointmentEmployeeRows);
        //                }
        //            }

        //            List<int?> employeeList = employeesToFilter.Select(y => y.GetItem<EPEmployee>().BAccountID).ToList();

        //            List<SharedClasses.ItemList> employeeSkills = SharedFunctions.GetItemWithList<FSEmployeeSkill,
        //                                                                                          FSEmployeeSkill.employeeID,
        //                                                                                          FSEmployeeSkill.skillID>(graphEmployeeMaint, employeeList);

        //            List<SharedClasses.ItemList> employeeLicenseTypes = SharedFunctions.GetItemWithList<FSLicense,
        //                                                                                                FSLicense.employeeID,
        //                                                                                                FSLicense.licenseTypeID,
        //                                                                                                Where<FSLicense.expirationDate, GreaterEqual<Required<FSLicense.expirationDate>>>>
        //                                                                                                (graphEmployeeMaint, employeeList, startDate);

        //            List<SharedClasses.ItemList> employeeGeoZones = SharedFunctions.GetItemWithList<FSGeoZoneEmp,
        //                                                                                            FSGeoZoneEmp.employeeID,
        //                                                                                            FSGeoZoneEmp.geoZoneID>(graphEmployeeMaint, employeeList);

        //            for (int i = 0; i < employeesToFilter.Count; i++)
        //            {
        //                EPEmployee employee = (EPEmployee)employeesToFilter[i];
        //                List<object> listFilter = new List<object>();

        //                SharedClasses.ItemList employeeSkillList = employeeSkills.FirstOrDefault(y => y.itemID == employee.BAccountID);
        //                SharedClasses.ItemList employeeLicenseTypeList = employeeLicenseTypes.FirstOrDefault(y => y.itemID == employee.BAccountID);
        //                SharedClasses.ItemList employeeGeoZoneList = employeeGeoZones.FirstOrDefault(y => y.itemID == employee.BAccountID);

        //                bool removeEmployee = false;

        //                //Iterate for each filter and try to filter in each List above.
        //                foreach (DispatchBoardFilters filter in filters)
        //                {
        //                    switch (filter.property)
        //                    {
        //                        case TX.Dispatch_Board.SKILL_FILTER:
        //                            listFilter = filter.value.Select(int.Parse).Cast<object>().ToList();
        //                            if (listFilter.Count > 0
        //                                    && (employeeSkillList == null
        //                                    || listFilter.Except(employeeSkillList.list).Any()))
        //                            {
        //                                removeEmployee = true;
        //                            }

        //                            break;
        //                        case TX.Dispatch_Board.LICENSE_TYPE_FILTER:
        //                            listFilter = filter.value.Select(int.Parse).Cast<object>().ToList();
        //                            if (listFilter.Count > 0
        //                                    && (employeeLicenseTypeList == null
        //                                    || listFilter.Except(employeeLicenseTypeList.list).Any()))
        //                            {
        //                                removeEmployee = true;
        //                            }

        //                            break;
        //                        case TX.Dispatch_Board.GEO_ZONE_FILTER:
        //                            listFilter = filter.value.Select(int.Parse).Cast<object>().ToList();
        //                            if (listFilter.Count > 0
        //                                    && (employeeGeoZoneList == null
        //                                    || listFilter.Except(employeeGeoZoneList.list).Any()))
        //                            {
        //                                removeEmployee = true;
        //                            }

        //                            break;
        //                        case TX.Dispatch_Board.SERVICE_FILTER:

        //                            List<int?> serviceIDList = filter.value.Select(int.Parse).Cast<int?>().ToList();
        //                            List<SharedClasses.ItemList> serviceSkills = SharedFunctions.GetItemWithList<FSServiceSkill,
        //                                                                                                         FSServiceSkill.serviceID,
        //                                                                                                         FSServiceSkill.skillID>(graphEmployeeMaint, serviceIDList);

        //                            List<SharedClasses.ItemList> serviceLicenseTypes = SharedFunctions.GetItemWithList<FSServiceLicenseType,
        //                                                                                                               FSServiceLicenseType.serviceID,
        //                                                                                                               FSServiceLicenseType.licenseTypeID>(graphEmployeeMaint, serviceIDList);

        //                            foreach (string value in filter.value)
        //                            {
        //                                int serviceID = int.Parse(value);

        //                                SharedClasses.ItemList serviceSkillList = serviceSkills.FirstOrDefault(y => y.itemID == serviceID);
        //                                SharedClasses.ItemList serviceLicenseTypeList = serviceLicenseTypes.FirstOrDefault(y => y.itemID == serviceID);

        //                                /*Checking if Employee have all skills*/
        //                                if (serviceSkillList != null
        //                                        && (employeeSkillList == null
        //                                            || serviceSkillList.list.Except(employeeSkillList.list).Any()))
        //                                {
        //                                    removeEmployee = true;
        //                                    break;
        //                                }

        //                                /*Checking if Employee have all licenseTypes*/
        //                                if (serviceLicenseTypeList != null
        //                                        && (employeeLicenseTypeList == null
        //                                            || serviceLicenseTypeList.list.Except(employeeLicenseTypeList.list).Any()))
        //                                {
        //                                    removeEmployee = true;
        //                                    break;
        //                                }
        //                            }

        //                            break;
        //                        default:

        //                            break;
        //                    }
        //                }

        //                //If ServiceOrder Does not pass the filters its removed from the List.
        //                if (removeEmployee)
        //                {
        //                    employeesToFilter.RemoveAt(i--);
        //                }
        //            }
        //        }
        //        else
        //        {
        //            employeesToFilter = (args.Count > 0) ? employeesBase.Select(args.ToArray()) : employeesBase.Select();

        //            if (ignoreAvailabilityFlag != true)
        //            {
        //                if (auxByScheduled)
        //                {
        //                    employeesToFilter = FilterEmployeesByBranchLocationAndScheduled(employeesToFilter, fsEmployeeScheduleRows, fsAppointmentEmployeeRows);
        //                }
        //                else
        //                {
        //                    employeesToFilter = FilterEmployeesByBranchLocation(employeesToFilter, fsEmployeeScheduleRows, fsAppointmentEmployeeRows);
        //                }
        //            }
        //        }

        //        return employeesToFilter;
        //    }

        //    public static PXResultset<FSServiceOrder> ServiceOrderRecords(int branchID, int? branchLocationID, DispatchBoardFilters[] filters, DateTime? scheduledDateStart = null, DateTime? scheduledDateEnd = null, bool? isRoomCalendar = false)
        //    {
        //        List<object> args = new List<object>();
        //        var graphExternalControls = PXGraph.CreateInstance<ExternalControls>();
        //        var graphServiceOrderMaint = PXGraph.CreateInstance<ServiceOrderEntry>();
        //        List<object> retList = new List<object>();

        //        //Get all ServiceOrders
        //        PXSelectBase<FSServiceOrder> servicesOrdersBase = new PXSelectJoin<FSServiceOrder,
        //                                                                InnerJoin<FSSrvOrdType,
        //                                                                    On<
        //                                                                        FSSrvOrdType.srvOrdType, Equal<FSServiceOrder.srvOrdType>>,
        //                                                                InnerJoin<BAccount,
        //                                                                    On<
        //                                                                        BAccount.bAccountID, Equal<FSServiceOrder.customerID>>>>,
        //                                                                Where<
        //                                                                    FSServiceOrder.status, NotEqual<FSServiceOrder.status.Quote>,
        //                                                                And<
        //                                                                    FSServiceOrder.status, NotEqual<FSServiceOrder.status.Hold>,
        //                                                                And<
        //                                                                    FSServiceOrder.status, NotEqual<FSServiceOrder.status.Closed>,
        //                                                                And<
        //                                                                    FSServiceOrder.status, NotEqual<FSServiceOrder.status.Canceled>,
        //                                                                And<
        //                                                                    FSServiceOrder.status, NotEqual<FSServiceOrder.status.Completed>,
        //                                                                And<
        //                                                                    BAccount.type, NotEqual<BAccountType.prospectType>,
        //                                                                And<
        //                                                                    FSServiceOrder.branchID, Equal<Required<FSServiceOrder.branchID>>>>>>>>>>
        //                                                                (graphServiceOrderMaint);

        //        args.Add(branchID);

        //        if (scheduledDateStart != null)
        //        {
        //            servicesOrdersBase.WhereAnd<Where2<
        //                                        Where<
        //                                            FSServiceOrder.sLAETA, GreaterEqual<Required<FSServiceOrder.sLAETA>>,
        //                                        Or<
        //                                            FSServiceOrder.promisedDate, GreaterEqual<Required<FSServiceOrder.promisedDate>>>>,
        //                                        Or<
        //                                            Where<
        //                                                FSServiceOrder.sLAETA, IsNull,
        //                                            And<
        //                                                FSServiceOrder.promisedDate, IsNull>>>>>();
        //            args.Add(scheduledDateStart);
        //            args.Add(scheduledDateStart);
        //        }

        //        if (scheduledDateEnd != null)
        //        {
        //            servicesOrdersBase.WhereAnd<Where2<
        //                                        Where<
        //                                            FSServiceOrder.sLAETA, LessEqual<Required<FSServiceOrder.sLAETA>>,
        //                                        Or<
        //                                            FSServiceOrder.promisedDate, LessEqual<Required<FSServiceOrder.promisedDate>>>>,
        //                                        Or<
        //                                            Where<
        //                                                FSServiceOrder.sLAETA, IsNull,
        //                                            And<
        //                                                FSServiceOrder.promisedDate, IsNull>>>>>();
        //            args.Add(scheduledDateEnd);
        //            args.Add(scheduledDateEnd);
        //        }

        //        if (branchLocationID != null)
        //        {
        //            servicesOrdersBase.WhereAnd<Where<
        //                                            FSServiceOrder.branchLocationID, Equal<Required<FSServiceOrder.branchLocationID>>>>();

        //            args.Add(branchLocationID);
        //        }

        //        PXResultset<FSServiceOrder> servicesOrdersToFilter;

        //        if (filters != null)
        //        {
        //            bool auxLike = false;
        //            bool auxAssignedEmployee = false;
        //            for (int i = 0; i < filters.Length; i++)
        //            {
        //                if (filters[i].property == TX.Dispatch_Board.LIKETEXT_FILTER)
        //                {
        //                    servicesOrdersBase.WhereAnd<Where<
        //                                                    FSServiceOrder.refNbr, Like<Required<FSServiceOrder.refNbr>>,
        //                                                    Or<BAccount.acctName, Like<Required<Customer.acctName>>>>>();

        //                    args.Add(string.Concat("%", filters[i].value[0], "%"));
        //                    args.Add(string.Concat("%", filters[i].value[0], "%"));
        //                    auxLike = true;
        //                    if (auxAssignedEmployee)
        //                    {
        //                        break;
        //                    }
        //                }

        //                if (filters[i].property == TX.Dispatch_Board.ASSIGNED_EMPLOYEE_FILTER)
        //                {
        //                    servicesOrdersBase.WhereAnd<Where<
        //                                                    FSServiceOrder.assignedEmpID, Equal<Required<FSServiceOrder.assignedEmpID>>>>();
        //                    args.Add(filters[i].value[0]);
        //                    auxAssignedEmployee = true;
        //                    if (auxLike)
        //                    {
        //                        break;
        //                    }
        //                }
        //            }

        //            servicesOrdersBase.View.Clear();
        //            servicesOrdersToFilter = (args.Count > 0) ? servicesOrdersBase.Select(args.ToArray()) : servicesOrdersBase.Select();

        //            List<int?> serviceOrderIDList = servicesOrdersToFilter.Select(y => y.GetItem<FSServiceOrder>().SOID).ToList();

        //            //SDAppointmentsCloseBySO
        //            List<SharedClasses.ItemList> appointmentsCloseList = SharedFunctions.GetItemWithList<FSAppointment,
        //                                                                                               FSAppointment.sOID,
        //                                                                                               FSAppointment.appointmentID,
        //                                                                                               Where<FSAppointment.status, Equal<FSAppointment.status.Closed>>>
        //                                                                                               (graphServiceOrderMaint, serviceOrderIDList);

        //             for (int i = 0; i < servicesOrdersToFilter.Count; i++)
        //             {
        //                 FSServiceOrder serviceOrder = (FSServiceOrder)servicesOrdersToFilter[i];

        //                 graphExternalControls.SODetPendingLines.View.Clear();
        //                PXResultset<FSSODet> serviceOrderDetails = graphExternalControls.SODetPendingLines.Select(serviceOrder.SOID);

        //                List<int> listFilter = new List<int>();
        //                List<int> listSkills = new List<int>();
        //                List<int> listLicenseTypes = new List<int>();
        //                List<int> listProblem = new List<int>();
        //                List<string> listServiceClasses = new List<string>();

        //                bool removeServiceOrder = false;
        //                bool serviceOrderAppointment = true;

        //                SharedClasses.ItemList appointmentsClose = appointmentsCloseList.FirstOrDefault(y => y.itemID == serviceOrder.SOID);

        //                if (serviceOrderDetails.Count == 0)
        //                {
        //                    servicesOrdersToFilter.RemoveAt(i--);
        //                }
        //                else if (isRoomCalendar == true && appointmentsClose != null && appointmentsClose.list.Count > 0)
        //                {
        //                    servicesOrdersToFilter.RemoveAt(i--);
        //                } 
        //                else
        //                {
        //                    List<int?> serviceIDList = serviceOrderDetails.Select(y => y.GetItem<FSSODet>().ServiceID).ToList();
        //                    List<int?> soDetIDList = serviceOrderDetails.Select(y => y.GetItem<FSSODet>().SODetID).ToList();

        //                    List<SharedClasses.ItemList> serviceSkills = SharedFunctions.GetItemWithList<FSServiceSkill,
        //                                                                                                 FSServiceSkill.serviceID,
        //                                                                                                 FSServiceSkill.skillID>
        //                                                                                                 (graphServiceOrderMaint, serviceIDList);

        //                    List<SharedClasses.ItemList> serviceLicenseTypes = SharedFunctions.GetItemWithList<FSServiceLicenseType,
        //                                                                                                       FSServiceLicenseType.serviceID,
        //                                                                                                       FSServiceLicenseType.licenseTypeID>
        //                                                                                                       (graphServiceOrderMaint, serviceIDList);

        //                    List<SharedClasses.ItemList> serviceItemClass = SharedFunctions.GetItemWithList<InventoryItem,
        //                                                                                                    InventoryItem.inventoryID,
        //                                                                                                    InventoryItem.itemClassID>
        //                                                                                                    (graphServiceOrderMaint, serviceIDList);

        //                    //ActiveAppointmentDets
        //                    List<SharedClasses.ItemList> activeAppointmentDetList =
        //                                SharedFunctions.GetItemWithList<FSAppointmentDet,
        //                                                                InnerJoin<FSAppointment,
        //                                                                        On<FSAppointment.appointmentID, Equal<FSAppointmentDet.appointmentID>>>,
        //                                                                FSAppointmentDet.sODetID,
        //                                                                FSAppointmentDet.appDetID,
        //                                                                Where<FSAppointment.status, Equal<FSAppointment.status.ManualScheduled>,
        //                                                                    Or<FSAppointment.status, Equal<FSAppointment.status.AutomaticScheduled>,
        //                                                                    Or<FSAppointment.status, Equal<FSAppointment.status.InProcess>>>>>
        //                                                                (graphServiceOrderMaint, soDetIDList);

        //                    //Iterate to get Skills, LicenseTypes and ServiceClasses for each SODet
        //                    foreach (FSSODet serviceOrderDetail in serviceOrderDetails)
        //                    {
        //                        SharedClasses.ItemList appointmentDetList = activeAppointmentDetList.FirstOrDefault(y => y.itemID == serviceOrderDetail.SODetID);
        //                        SharedClasses.ItemList serviceSkillList = serviceSkills.FirstOrDefault(y => y.itemID == serviceOrderDetail.ServiceID);
        //                        SharedClasses.ItemList serviceLicenseTypeList = serviceLicenseTypes.FirstOrDefault(y => y.itemID == serviceOrderDetail.ServiceID);
        //                        SharedClasses.ItemList serviceItemClassList = serviceItemClass.FirstOrDefault(y => y.itemID == serviceOrderDetail.ServiceID);

        //                        if (serviceSkillList != null)
        //                        {
        //                            foreach (int serviceSkillID in serviceSkillList.list)
        //                            {
        //                                listSkills.Add(serviceSkillID);
        //                            }
        //                        }

        //                        if (serviceLicenseTypeList != null)
        //                        {
        //                            foreach (int serviceLicenseTypeID in serviceLicenseTypeList.list)
        //                            {
        //                                listLicenseTypes.Add(serviceLicenseTypeID);
        //                            }
        //                        }

        //                        if (serviceItemClassList != null)
        //                        {
        //                            foreach (string serviceItemClassID in serviceItemClassList.list)
        //                            {
        //                                listServiceClasses.Add(serviceItemClassID);
        //                            }
        //                        }

        //                        if (appointmentDetList == null)
        //                        {
        //                            serviceOrderAppointment = false;
        //                        }

        //                        if (serviceOrder.ProblemID != null)
        //                        {
        //                            listProblem.Add((int)serviceOrder.ProblemID);
        //                        }
        //                    }

        //                    if (serviceOrderAppointment)
        //                    {
        //                        servicesOrdersToFilter.RemoveAt(i--);
        //                    }
        //                    else
        //                    {
        //                        //Iterate for each filter and try to filter in each List above.
        //                        foreach (ExternalControls.DispatchBoardFilters filter in filters)
        //                        {
        //                            switch (filter.property)
        //                            {
        //                                case TX.Dispatch_Board.SKILL_FILTER:
        //                                    listFilter = filter.value.Select(int.Parse).ToList();
        //                                    if (listFilter.Except(listSkills).Any())
        //                                    {
        //                                        removeServiceOrder = true;
        //                                        break;
        //                                    }

        //                                    break;
        //                                case TX.Dispatch_Board.LICENSE_TYPE_FILTER:
        //                                    listFilter = filter.value.Select(int.Parse).ToList();
        //                                    if (listFilter.Except(listLicenseTypes).Any())
        //                                    {
        //                                        removeServiceOrder = true;
        //                                        break;
        //                                    }

        //                                    break;
        //                                case TX.Dispatch_Board.PROBLEM_FILTER:
        //                                    listFilter = filter.value.Select(int.Parse).ToList();
        //                                    if (listFilter.Except(listProblem).Any())
        //                                    {
        //                                        removeServiceOrder = true;
        //                                        break;
        //                                    }

        //                                    break;
        //                                case TX.Dispatch_Board.SERVICE_CLASS_FILTER:
        //                                    List<string> listFilterString = filter.value.ToList();
        //                                    if (listFilterString.Except(listServiceClasses).Any())
        //                                    {
        //                                        removeServiceOrder = true;
        //                                        break;
        //                                    }

        //                                    break;
        //                                default:

        //                                    break;
        //                            }
        //                        }

        //                        //If ServiceOrder does not pass the filters its removed from the List.
        //                        if (removeServiceOrder)
        //                        {
        //                            servicesOrdersToFilter.RemoveAt(i--);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        else
        //        {
        //            servicesOrdersToFilter = (args.Count > 0) ? servicesOrdersBase.Select(args.ToArray()) : servicesOrdersBase.Select();

        //            List<int?> serviceOrderIDList = servicesOrdersToFilter.Select(y => y.GetItem<FSServiceOrder>().SOID).ToList();

        //            //SODetPendingLines
        //            List<SharedClasses.ItemList> serviceOrderDetailList = SharedFunctions.GetItemWithList<FSSODet,
        //                                                                                               FSSODet.sOID,
        //                                                                                               FSSODet.sODetID,
        //                                                                                               Where<FSSODet.lineType, Equal<ListField_LineType_Service_ServiceTemplate.Service>,
        //                                                                                                    And<Where<FSSODet.status, Equal<FSSODet.status.Open>,
        //                                                                                                            Or<FSSODet.status, Equal<FSSODet.status.InProcess>>>>>>
        //                                                                                               (graphServiceOrderMaint, serviceOrderIDList);

        //            //SDAppointmentsCloseBySO
        //            List<SharedClasses.ItemList> appointmentsCloseList = SharedFunctions.GetItemWithList<FSAppointment,
        //                                                                                               FSAppointment.sOID,
        //                                                                                               FSAppointment.appointmentID,
        //                                                                                               Where<FSAppointment.status, Equal<FSAppointment.status.Closed>>>
        //                                                                                               (graphServiceOrderMaint, serviceOrderIDList);

        //            for (int i = 0; i < servicesOrdersToFilter.Count; i++)
        //            {
        //                bool serviceOrderAppointment = true;

        //                FSServiceOrder serviceOrder = (FSServiceOrder)servicesOrdersToFilter[i];

        //                SharedClasses.ItemList serviceOrderDetails = serviceOrderDetailList.FirstOrDefault(y => y.itemID == serviceOrder.SOID);
        //                SharedClasses.ItemList appointmentsClose = appointmentsCloseList.FirstOrDefault(y => y.itemID == serviceOrder.SOID);

        //                if (serviceOrderDetails == null)
        //                {
        //                    servicesOrdersToFilter.RemoveAt(i--);
        //                }
        //                else if (isRoomCalendar == true && appointmentsClose != null && appointmentsClose.list.Count > 0)
        //                {
        //                    servicesOrdersToFilter.RemoveAt(i--);
        //                }
        //                else
        //                {
        //                    List<int?> serviceOrderDetailIDList = serviceOrderDetails.list.Cast<int?>().ToList();

        //                    //ActiveAppointmentDets
        //                    List<SharedClasses.ItemList> activeAppointmentDetList =
        //                                SharedFunctions.GetItemWithList<FSAppointmentDet,
        //                                                                InnerJoin<FSAppointment,
        //                                                                        On<FSAppointment.appointmentID, Equal<FSAppointmentDet.appointmentID>>>,
        //                                                                FSAppointmentDet.sODetID,
        //                                                                FSAppointmentDet.appDetID,
        //                                                                Where<FSAppointment.status, Equal<FSAppointment.status.ManualScheduled>,
        //                                                                    Or<FSAppointment.status, Equal<FSAppointment.status.AutomaticScheduled>,
        //                                                                    Or<FSAppointment.status, Equal<FSAppointment.status.InProcess>>>>>
        //                                (graphServiceOrderMaint, serviceOrderDetailIDList);

        //                    foreach (var sODetID in serviceOrderDetails.list)
        //                    {
        //                        SharedClasses.ItemList activeAppointmentDet = activeAppointmentDetList.FirstOrDefault(y => y.itemID == (int?)sODetID);

        //                        if (activeAppointmentDet == null)
        //                        {
        //                            serviceOrderAppointment = false;
        //                        }
        //                    }

        //                    if (serviceOrderAppointment)
        //                    {
        //                        servicesOrdersToFilter.RemoveAt(i--);
        //                    }
        //                }
        //            }
        //        }

        //        return servicesOrdersToFilter;
        //    }

        //    public FSxService ServiceExtension(InventoryItem inventoryItemRow)
        //    {
        //        return (inventoryItemRow != null) ? PXCache<InventoryItem>.GetExtension<FSxService>(inventoryItemRow) : null;
        //    }

        //    public DispatchBoardAppointmentMessages DBCreateAppointments(FSAppointment appointment, List<int> sODetIDList, List<int> employeeIDList, List<int> resourceIDList)
        //    {
        //        DispatchBoardAppointmentMessages messages;
        //        messages = new DispatchBoardAppointmentMessages();
        //        try
        //        {
        //            var graphAppointmentMaint = PXGraph.CreateInstance<AppointmentEntry>();
        //            graphAppointmentMaint.AppointmentRecords.Insert(appointment);
        //            if (employeeIDList != null)
        //            {
        //                foreach (int sODetID in sODetIDList)
        //                {
        //                    FSAppointmentDetService fsAppointmentDetServiceRow = new FSAppointmentDetService();
        //                    fsAppointmentDetServiceRow.SODetID = sODetID;
        //                    fsAppointmentDetServiceRow.AppointmentID = appointment.AppointmentID;
        //                    graphAppointmentMaint.AppointmentDetServices.Insert(fsAppointmentDetServiceRow);
        //                }
        //            }

        //            if (employeeIDList != null)
        //            {
        //                foreach (int employeeID in employeeIDList)
        //                {
        //                    FSAppointmentEmployee temp = new FSAppointmentEmployee();
        //                    temp.EmployeeID = employeeID;
        //                    temp.AppointmentID = appointment.AppointmentID;
        //                    graphAppointmentMaint.AppointmentEmployees.Insert(temp);
        //                }
        //            }

        //            if (resourceIDList != null)
        //            {
        //                foreach (int resourceID in resourceIDList)
        //                {
        //                    FSAppointmentResource temp = new FSAppointmentResource();
        //                    temp.ResourceID = resourceID;
        //                    temp.AppointmentID = appointment.AppointmentID;
        //                    graphAppointmentMaint.AppointmentResources.Insert(temp);
        //                }
        //            }

        //            graphAppointmentMaint.Save.PressButton();
        //            return graphAppointmentMaint.messages;
        //        }
        //        catch (Exception e)
        //        {
        //            messages.ErrorMessages.Add(e.Message);
        //            return messages;
        //        }
        //    }

        //    public DispatchBoardAppointmentMessages DBPutAppointments(FSAppointment appointment)
        //    {
        //        DispatchBoardAppointmentMessages messages;
        //        messages = new DispatchBoardAppointmentMessages();

        //        try
        //        {
        //            var graphAppointmentMaint = PXGraph.CreateInstance<AppointmentEntry>();

        //            if (appointment != null)
        //            {
        //                graphAppointmentMaint.AppointmentRecords.Current = graphAppointmentMaint.AppointmentRecords.Search<FSAppointment.refNbr>(appointment.RefNbr, appointment.SrvOrdType);
        //                FSAppointment auxAppointment = graphAppointmentMaint.AppointmentRecords.Current;

        //                if (auxAppointment != null)
        //                {
        //                    auxAppointment.ScheduledDateTimeBegin = appointment.ScheduledDateTimeBegin;
        //                    auxAppointment.ScheduledDateTimeEnd = appointment.ScheduledDateTimeEnd;
        //                    auxAppointment.Confirmed = appointment.Confirmed;
        //                    auxAppointment.LongDescr = appointment.LongDescr;
        //                    auxAppointment.ValidatedByDispatcher = appointment.ValidatedByDispatcher;
        //                    auxAppointment.Confirmed = appointment.Confirmed;

        //                    graphAppointmentMaint.AppointmentRecords.Update(auxAppointment);

        //                    FSServiceOrder fsServiceOrderRow = graphAppointmentMaint.ServiceOrderSelected.Current;
        //                    if (string.IsNullOrEmpty(appointment.Mem_RoomID) == false
        //                            && string.IsNullOrWhiteSpace(appointment.Mem_RoomID) == false)
        //                    {
        //                        fsServiceOrderRow.RoomID = appointment.Mem_RoomID;
        //                    }

        //                    graphAppointmentMaint.ServiceOrderSelected.Update(fsServiceOrderRow);

        //                    if (appointment.OldEmployeeID != appointment.EmployeeID)
        //                    {
        //                        FSAppointmentEmployee employee = (FSAppointmentEmployee)this.AppointmentEmployee
        //                                                            .Select(appointment.AppointmentID, appointment.EmployeeID);

        //                        if (employee != null)
        //                        {
        //                            messages.ErrorMessages.Add(GetErrorMessage(ErrorCode.APPOINTMENT_SHARED));
        //                            return messages;
        //                        }
        //                        else
        //                        {
        //                            FSAppointmentEmployee oldEmployee = (FSAppointmentEmployee)this.AppointmentEmployee
        //                                                                   .Select(appointment.AppointmentID, appointment.OldEmployeeID);

        //                            if (oldEmployee != null)
        //                            {
        //                                graphAppointmentMaint.AppointmentEmployees.Delete(oldEmployee);
        //                            }

        //                            FSAppointmentEmployee newEmployee = new FSAppointmentEmployee();
        //                            newEmployee.AppointmentID = appointment.AppointmentID;
        //                            newEmployee.EmployeeID = appointment.EmployeeID;

        //                            graphAppointmentMaint.AppointmentEmployees.Insert(newEmployee);
        //                        }
        //                    }

        //                    graphAppointmentMaint.Save.PressButton();
        //                    return graphAppointmentMaint.messages;
        //                }
        //                else
        //                {
        //                    messages.ErrorMessages.Add(GetErrorMessage(ErrorCode.APPOINTMENT_NOT_FOUND));
        //                    return messages;
        //                }
        //            }

        //            return null;
        //        }
        //        catch (Exception e)
        //        {
        //            string errorMessage = e.Message;
        //            int first = errorMessage.IndexOf(ID.AcumaticaErrorNumber.SAVE_DISABLED_BUTTON);
        //            if (first > 0)
        //            {
        //                messages.ErrorMessages.Add(TX.Error.APPOINTMENT_NOT_EDITABLE);
        //            }
        //            else
        //            {
        //                messages.ErrorMessages.Add(errorMessage);
        //            }

        //            return messages;
        //        }
        //    }

        //    public DispatchBoardAppointmentMessages DBUpdateAppointments(
        //                                                                FSAppointment appointment,
        //                                                                List<int> appointmentServicesList,
        //                                                                List<int> employeeIDList,
        //                                                                List<int> resourceIDList,
        //                                                                List<int> oldAppointmentServicesList,
        //                                                                List<int> oldEmployeeIDList,
        //                                                                List<int> oldResourceIDList)
        //    {
        //        DispatchBoardAppointmentMessages messages;
        //        messages = new DispatchBoardAppointmentMessages();
        //        try
        //        {
        //            var graphAppointmentMaint = PXGraph.CreateInstance<AppointmentEntry>();

        //            if (appointment != null)
        //            {
        //                FSAppointment auxAppointment = (FSAppointment)this.AppointmentRecord
        //                                                    .Select(appointment.AppointmentID);
        //                if (auxAppointment != null)
        //                {
        //                    auxAppointment.Confirmed = appointment.Confirmed;
        //                    auxAppointment.ScheduledDateTimeBegin = appointment.ScheduledDateTimeBegin;
        //                    auxAppointment.ScheduledDateTimeEnd = appointment.ScheduledDateTimeEnd;
        //                    auxAppointment.LongDescr = appointment.LongDescr;
        //                    auxAppointment.ValidatedByDispatcher = appointment.ValidatedByDispatcher;
        //                    auxAppointment.Confirmed = appointment.Confirmed;

        //                    FSServiceOrder fsServiceOrderRow = ServiceOrderRecord.SelectSingle(appointment.SOID);

        //                    if (!string.IsNullOrEmpty(fsServiceOrderRow.RoomID))
        //                    {
        //                        graphAppointmentMaint.ServiceOrderSelected.Current.RoomID = fsServiceOrderRow.RoomID;
        //                    }

        //                    graphAppointmentMaint.AppointmentRecords.Update(auxAppointment);

        //                    #region HandleServices
        //                    // AppointmentDet para agregar
        //                    IEnumerable<int> newServices = appointmentServicesList.Except(oldAppointmentServicesList);
        //                    if (newServices.Count() > 0)
        //                    {
        //                        foreach (var soDetID in newServices)
        //                        {
        //                            //Creacion de appointmentDet
        //                            FSAppointmentDetService newAppointmentDetService = new FSAppointmentDetService();
        //                            newAppointmentDetService.AppointmentID = auxAppointment.AppointmentID;
        //                            newAppointmentDetService.SODetID = soDetID;
        //                            graphAppointmentMaint.AppointmentDetServices.Insert(newAppointmentDetService);
        //                        }
        //                    }

        //                    // AppointmentDet para remover
        //                    IEnumerable<int> removedServices = oldAppointmentServicesList.Except(appointmentServicesList);
        //                    if (removedServices.Count() > 0)
        //                    {
        //                        foreach (var soDetID in removedServices)
        //                        {
        //                            //Eliminar de appointmentDet
        //                            FSAppointmentDetService newAppointmentDetService = new FSAppointmentDetService();
        //                            newAppointmentDetService.AppointmentID = auxAppointment.AppointmentID;
        //                            newAppointmentDetService.SODetID = soDetID;
        //                            graphAppointmentMaint.AppointmentDetServices.Delete(newAppointmentDetService);
        //                        }
        //                    }
        //                    #endregion
        //                    #region HandleEmployees
        //                    // AppointmentEmployee para agregar
        //                    IEnumerable<int> newEmployees = employeeIDList.Except(oldEmployeeIDList);
        //                    if (newEmployees.Count() > 0)
        //                    {
        //                        foreach (var employeeID in newEmployees)
        //                        {
        //                            //Creacion de AppointmentEmployee
        //                            FSAppointmentEmployee newAppointmentEmployee = new FSAppointmentEmployee();
        //                            newAppointmentEmployee.AppointmentID = auxAppointment.AppointmentID;
        //                            newAppointmentEmployee.EmployeeID = employeeID;
        //                            graphAppointmentMaint.AppointmentEmployees.Insert(newAppointmentEmployee);
        //                        }
        //                    }

        //                    // AppointmentEmployee para remover
        //                    IEnumerable<int> removedEmployees = oldEmployeeIDList.Except(employeeIDList);
        //                    if (removedEmployees.Count() > 0)
        //                    {
        //                        foreach (var employeeID in removedEmployees)
        //                        {
        //                            //Eliminar de AppointmentEmployee
        //                            FSAppointmentEmployee newAppointmentEmployee = new FSAppointmentEmployee();
        //                            newAppointmentEmployee.AppointmentID = auxAppointment.AppointmentID;
        //                            newAppointmentEmployee.EmployeeID = employeeID;
        //                            graphAppointmentMaint.AppointmentEmployees.Delete(newAppointmentEmployee);
        //                        }
        //                    }
        //                    #endregion
        //                    #region HandleResources
        //                    // AppointmentResource para agregar
        //                    IEnumerable<int> newResources = resourceIDList.Except(oldResourceIDList);
        //                    if (newResources.Count() > 0)
        //                    {
        //                        foreach (var resourceID in newResources)
        //                        {
        //                            //Creacion de AppointmentResource
        //                            FSAppointmentResource newAppointmentResource = new FSAppointmentResource();
        //                            newAppointmentResource.AppointmentID = auxAppointment.AppointmentID;
        //                            newAppointmentResource.ResourceID = resourceID;
        //                            graphAppointmentMaint.AppointmentResources.Insert(newAppointmentResource);
        //                        }
        //                    }

        //                    // AppointmentResource para remover
        //                    IEnumerable<int> removedResources = oldResourceIDList.Except(resourceIDList);
        //                    if (removedResources.Count() > 0)
        //                    {
        //                        foreach (var resourceID in removedResources)
        //                        {
        //                            //Eliminar de AppointmentResource
        //                            FSAppointmentResource newAppointmentResource = new FSAppointmentResource();
        //                            newAppointmentResource.AppointmentID = auxAppointment.AppointmentID;
        //                            newAppointmentResource.ResourceID = resourceID;
        //                            graphAppointmentMaint.AppointmentResources.Delete(newAppointmentResource);
        //                        }
        //                    }
        //                    #endregion
        //                    graphAppointmentMaint.Save.PressButton();
        //                    return graphAppointmentMaint.messages;
        //                }
        //                else
        //                {
        //                    messages = new DispatchBoardAppointmentMessages();
        //                    messages.ErrorMessages.Add(GetErrorMessage(ErrorCode.APPOINTMENT_NOT_FOUND));
        //                    return messages;
        //                }
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            messages.ErrorMessages.Add(e.Message);
        //            return messages;
        //        }

        //        return messages;
        //    }

        //    public DispatchBoardAppointmentMessages DBShareAppointments(FSAppointment appointment, List<int> employeeIDList, List<int> oldEmployeeIDList)
        //    {
        //        DispatchBoardAppointmentMessages messages;
        //        messages = new DispatchBoardAppointmentMessages();
        //        try
        //        {
        //            var graphAppointmentMaint = PXGraph.CreateInstance<AppointmentEntry>();

        //            if (appointment != null)
        //            {
        //                FSAppointment auxAppointment = (FSAppointment)this.AppointmentRecord
        //                                                    .Select(appointment.AppointmentID);
        //                if (auxAppointment != null)
        //                {                            
        //                    #region HandleEmployees
        //                    // AppointmentEmployee para agregar
        //                    IEnumerable<int> newEmployees = employeeIDList.Except(oldEmployeeIDList);
        //                    graphAppointmentMaint.AppointmentRecords.Update(auxAppointment);

        //                    if (newEmployees.Count() > 0)
        //                    {
        //                        foreach (var employeeID in newEmployees)
        //                        {
        //                            //Creacion de AppointmentEmployee
        //                            FSAppointmentEmployee newAppointmentEmployee = new FSAppointmentEmployee();
        //                            newAppointmentEmployee.AppointmentID = auxAppointment.AppointmentID;
        //                            newAppointmentEmployee.EmployeeID = employeeID;
        //                            graphAppointmentMaint.AppointmentEmployees.Insert(newAppointmentEmployee);
        //                        }
        //                    }

        //                    // AppointmentEmployee para remover
        //                    IEnumerable<int> removedEmployees = oldEmployeeIDList.Except(employeeIDList);
        //                    if (removedEmployees.Count() > 0)
        //                    {
        //                        foreach (var employeeID in removedEmployees)
        //                        {
        //                            //Eliminar de AppointmentEmployee
        //                            FSAppointmentEmployee newAppointmentEmployee = new FSAppointmentEmployee();
        //                            newAppointmentEmployee.AppointmentID = auxAppointment.AppointmentID;
        //                            newAppointmentEmployee.EmployeeID = employeeID;
        //                            graphAppointmentMaint.AppointmentEmployees.Delete(newAppointmentEmployee);
        //                        }
        //                    }
        //                    #endregion

        //                    graphAppointmentMaint.Save.PressButton();
        //                    return graphAppointmentMaint.messages;
        //                }
        //                else
        //                {
        //                    messages = new DispatchBoardAppointmentMessages();
        //                    messages.ErrorMessages.Add(GetErrorMessage(ErrorCode.APPOINTMENT_NOT_FOUND));
        //                    return messages;
        //                }
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            messages.ErrorMessages.Add(e.Message);
        //            return messages;
        //        }

        //        return messages;
        //    }

        //    public DispatchBoardAppointmentMessages DBDeleteAppointments(FSAppointment appointment) 
        //    {
        //        DispatchBoardAppointmentMessages messages;
        //        messages = new DispatchBoardAppointmentMessages();
        //        try
        //        {
        //            var graphAppointmentMaint = PXGraph.CreateInstance<AppointmentEntry>();

        //            graphAppointmentMaint.AppointmentRecords.Current = graphAppointmentMaint.AppointmentRecords.Search<FSAppointment.refNbr>(appointment.RefNbr, appointment.SrvOrdType);
        //            appointment = graphAppointmentMaint.AppointmentRecords.Current;
        //            graphAppointmentMaint.AppointmentRecords.Delete(appointment);
        //            graphAppointmentMaint.Save.PressButton();
        //            return graphAppointmentMaint.messages;
        //        }
        //        catch (Exception e)
        //        {
        //            messages.ErrorMessages.Add(e.Message);
        //            return messages;
        //        }
        //    }

        //    public DispatchBoardAppointmentMessages AndroidPutAppointments(int appointmentID, string status) 
        //    {
        //        DispatchBoardAppointmentMessages messages;
        //        messages = new DispatchBoardAppointmentMessages();
                
        //        try
        //        {
        //            var graphAppointmentMaint = PXGraph.CreateInstance<AppointmentEntry>();
        //            FSAppointment auxAppointment = this.AppointmentRecord
        //                                            .Select(appointmentID);
        //            if (auxAppointment != null)
        //            {
        //                auxAppointment.Status = status;
        //                graphAppointmentMaint.AppointmentRecords.Update(auxAppointment);
        //                graphAppointmentMaint.Save.PressButton();

        //                return messages;
        //            }
        //            else
        //            {
        //                messages.ErrorMessages.Add(GetErrorMessage(ErrorCode.APPOINTMENT_NOT_FOUND));
        //                return messages;
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            messages.ErrorMessages.Add(e.Message);
        //            return messages;
        //        }
        //    }

        //    public int? DBCreateAppointmentBridge(FSAppointment appointment, List<int> sODetIDList, List<int> employeeIDList)
        //    {
        //        FSWrkProcess fsWrkProcessRow = new FSWrkProcess();

        //        fsWrkProcessRow.RoomID                  = appointment.Mem_RoomID;
        //        fsWrkProcessRow.SOID                    = appointment.SOID;
        //        fsWrkProcessRow.SrvOrdType              = appointment.SrvOrdType;
        //        fsWrkProcessRow.BranchID                = appointment.Mem_BranchID;
        //        fsWrkProcessRow.BranchLocationID        = appointment.Mem_BranchLocationID;
        //        fsWrkProcessRow.CustomerID              = appointment.Mem_CustomerID;
        //        fsWrkProcessRow.ScheduledDateTimeBegin  = appointment.ScheduledDateTimeBegin;
        //        fsWrkProcessRow.ScheduledDateTimeEnd    = appointment.ScheduledDateTimeEnd;
        //        fsWrkProcessRow.TargetScreenID          = ID.ScreenID.APPOINTMENT;
        //        fsWrkProcessRow.EmployeeIDList          = string.Join(",", employeeIDList.ToArray());
        //        fsWrkProcessRow.LineRefList             = string.Join(",", sODetIDList.ToArray());
        //        fsWrkProcessRow.EquipmentIDList         = string.Empty;

        //        return WrkProcess.SaveWrkProcessParameters(fsWrkProcessRow);
        //    }

        //    public int? DBCreateWrkSchedulerBridge(FSWrkEmployeeSchedule wrkEmployeeSchedule)
        //    {
        //        FSWrkProcess fsWrkProcessRow = new FSWrkProcess();

        //        fsWrkProcessRow.BranchID = wrkEmployeeSchedule.BranchID;
        //        fsWrkProcessRow.BranchLocationID = wrkEmployeeSchedule.BranchLocationID;
        //        fsWrkProcessRow.EmployeeIDList = wrkEmployeeSchedule.EmployeeID.ToString();
        //        fsWrkProcessRow.ScheduledDateTimeBegin = wrkEmployeeSchedule.TimeStart;
        //        fsWrkProcessRow.ScheduledDateTimeEnd = wrkEmployeeSchedule.TimeEnd;
        //        fsWrkProcessRow.TargetScreenID = ID.ScreenID.EMPLOYEE_SCHEDULE;

        //        return WrkProcess.SaveWrkProcessParameters(fsWrkProcessRow);
        //    }        

        //    public DispatchBoardAppointmentMessages DBPutAvailability(FSTimeSlot fsTimeSlotRow)
        //    {
        //        DispatchBoardAppointmentMessages messages;
        //        messages = new DispatchBoardAppointmentMessages();
        //        try
        //        {
        //            TimeSlotMaint timeSlotMaintGraph = PXGraph.CreateInstance<TimeSlotMaint>();
        //            FSTimeSlot auxTimeSlot = this.TimeSlotRecord.Select(fsTimeSlotRow.TimeSlotID);
        //            if (auxTimeSlot != null)
        //            {
        //                auxTimeSlot.TimeStart = fsTimeSlotRow.TimeStart;
        //                auxTimeSlot.TimeEnd = fsTimeSlotRow.TimeEnd;
        //                TimeSpan duration = (DateTime)auxTimeSlot.TimeEnd - (DateTime)auxTimeSlot.TimeStart;
        //                auxTimeSlot.TimeDiff = (decimal?)duration.TotalMinutes;
        //                timeSlotMaintGraph.TimeSlotRecords.Update(auxTimeSlot);
        //                timeSlotMaintGraph.Actions.PressSave();
        //            }

        //            return messages;
        //        }
        //        catch (Exception e)
        //        {
        //            messages.ErrorMessages.Add(e.Message);
        //            return messages;
        //        }
        //    }

        //    public DispatchBoardAppointmentMessages CreateCustomFieldAppointments(FSCustomFieldAppointment[] customFieldAppointments)
        //    {
        //        DispatchBoardAppointmentMessages messages;
        //        messages = new DispatchBoardAppointmentMessages();
        //        try
        //        {
        //            var graphCustomFieldAppointmentsMaint = PXGraph.CreateInstance<CustomFieldAppointmentsMaint>();
        //            if (customFieldAppointments != null)
        //            {
        //                foreach (FSCustomFieldAppointment customFieldAppointment in customFieldAppointments)
        //                {
        //                    FSCustomFieldAppointment fsCustomFieldAppointmentRow = this.CustomFieldAppointment.Select(customFieldAppointment.CustomFieldAppointmentID);
        //                    if (fsCustomFieldAppointmentRow != null)
        //                    {
        //                        fsCustomFieldAppointmentRow.Active = customFieldAppointment.Active;
        //                        fsCustomFieldAppointmentRow.Position = customFieldAppointment.Position;
        //                        fsCustomFieldAppointmentRow.FieldDescr = customFieldAppointment.FieldDescr;
        //                        graphCustomFieldAppointmentsMaint.CustomFieldAppointmentRecords.Update(fsCustomFieldAppointmentRow);
        //                        graphCustomFieldAppointmentsMaint.Save.PressButton();
        //                    }
        //                    else
        //                    {
        //                        fsCustomFieldAppointmentRow = new FSCustomFieldAppointment();

        //                        fsCustomFieldAppointmentRow.Active = customFieldAppointment.Active;
        //                        fsCustomFieldAppointmentRow.Position = customFieldAppointment.Position;
        //                        fsCustomFieldAppointmentRow.FieldDescr = customFieldAppointment.FieldDescr;
        //                        fsCustomFieldAppointmentRow.CustomFieldAppointmentID = customFieldAppointment.CustomFieldAppointmentID;

        //                        graphCustomFieldAppointmentsMaint.CustomFieldAppointmentRecords.Insert(fsCustomFieldAppointmentRow);
        //                        graphCustomFieldAppointmentsMaint.Save.PressButton();
        //                    }
        //                }

        //                return messages;
        //            }

        //            return null;
        //        }
        //        catch
        //        {
        //            messages.ErrorMessages.Add(TX.Error.CUSTOM_APPOINTMENT_FIELD_NOT_FOUND);
        //            return messages;
        //        }
        //    }

        //    public DispatchBoardAppointmentMessages PutCustomFieldAppointments(FSCustomFieldAppointment[] customFieldAppointments)
        //    {
        //        DispatchBoardAppointmentMessages messages;
        //        messages = new DispatchBoardAppointmentMessages();

        //        try
        //        {
        //            var graphCustomFieldAppointmentsMaint = PXGraph.CreateInstance<CustomFieldAppointmentsMaint>();

        //            if (customFieldAppointments != null)
        //            {
        //                foreach (FSCustomFieldAppointment customFieldAppointment in customFieldAppointments)
        //                {
        //                    FSCustomFieldAppointment fsCustomFieldAppointmentRow = this.CustomFieldAppointment.Select(customFieldAppointment.CustomFieldAppointmentID);

        //                    if (fsCustomFieldAppointmentRow != null)
        //                    {
        //                        fsCustomFieldAppointmentRow.CustomFieldAppointmentID = customFieldAppointment.CustomFieldAppointmentID;
        //                        fsCustomFieldAppointmentRow.Active = customFieldAppointment.Active;
        //                        fsCustomFieldAppointmentRow.Position = customFieldAppointment.Position;
        //                        fsCustomFieldAppointmentRow.FieldDescr = customFieldAppointment.FieldDescr;
        //                        fsCustomFieldAppointmentRow.FieldImg = customFieldAppointment.FieldImg;

        //                        graphCustomFieldAppointmentsMaint.CustomFieldAppointmentRecords.Update(fsCustomFieldAppointmentRow);
        //                        graphCustomFieldAppointmentsMaint.Save.PressButton();
        //                    }
        //                    else
        //                    {
        //                        messages.ErrorMessages.Add(TX.Error.CUSTOM_APPOINTMENT_FIELD_NOT_FOUND);
        //                        return messages;
        //                    }
        //                }
        //            }

        //            return null;
        //        }
        //        catch
        //        {
        //            messages.ErrorMessages.Add(TX.Error.CUSTOM_APPOINTMENT_FIELD_NOT_FOUND);
        //            return messages;
        //        }
        //    }

        //    public DispatchBoardAppointmentMessages DeleteCustomFieldAppointments(FSCustomFieldAppointment[] customFieldAppointments)
        //    {
        //        DispatchBoardAppointmentMessages messages;
        //        messages = new DispatchBoardAppointmentMessages();

        //        try
        //        {
        //            var graphCustomFieldAppointmentsMaint = PXGraph.CreateInstance<CustomFieldAppointmentsMaint>();

        //            if (customFieldAppointments != null)
        //            {
        //                foreach (FSCustomFieldAppointment customFieldAppointment in customFieldAppointments)
        //                {
        //                    FSCustomFieldAppointment fsCustomFieldAppointmentRow = this.CustomFieldAppointment.Select(customFieldAppointment.CustomFieldAppointmentID);

        //                    if (fsCustomFieldAppointmentRow != null)
        //                    {
        //                        graphCustomFieldAppointmentsMaint.CustomFieldAppointmentRecords.Delete(fsCustomFieldAppointmentRow);
        //                    }
        //                    else
        //                    {
        //                        messages.ErrorMessages.Add(TX.Error.CUSTOM_APPOINTMENT_FIELD_NOT_FOUND);
        //                        return messages;
        //                    }
        //                }

        //                graphCustomFieldAppointmentsMaint.Save.PressButton();
        //                return messages;
        //            }

        //            return null;
        //        }
        //        catch
        //        {
        //            messages.ErrorMessages.Add(TX.Error.CUSTOM_APPOINTMENT_FIELD_NOT_FOUND);
        //            return messages;
        //        }
        //    }

        //    public DispatchBoardAppointmentMessages CreateCustomAppointmentStatuses(FSCustomAppointmentStatus[] customAppointmentStatuses)
        //    {
        //        DispatchBoardAppointmentMessages messages;
        //        messages = new DispatchBoardAppointmentMessages();
        //        try
        //        {
        //            var graphCustomAppointmentStatusMaint = PXGraph.CreateInstance<CustomAppointmentStatusMaint>();

        //            if (customAppointmentStatuses != null)
        //            {
        //                foreach (FSCustomAppointmentStatus customAppointmentStatus in customAppointmentStatuses)
        //                {
        //                    FSCustomAppointmentStatus fsCustomAppointmentStatusRow = this.CustomAppointmentStatusName.Select(customAppointmentStatus.FieldName);

        //                    if (fsCustomAppointmentStatusRow != null)
        //                    {
        //                        graphCustomAppointmentStatusMaint.Clear();
        //                        fsCustomAppointmentStatusRow.BackgroundColor = customAppointmentStatus.BackgroundColor;
        //                        fsCustomAppointmentStatusRow.TextColor = customAppointmentStatus.TextColor;
        //                        fsCustomAppointmentStatusRow.FieldName = customAppointmentStatus.FieldName;
        //                        fsCustomAppointmentStatusRow.FieldDescr = customAppointmentStatus.FieldDescr;
        //                        fsCustomAppointmentStatusRow.HideStatus = customAppointmentStatus.HideStatus;

        //                        graphCustomAppointmentStatusMaint.CustomAppointmentStatusRecords.Update(fsCustomAppointmentStatusRow);
        //                        graphCustomAppointmentStatusMaint.Save.PressButton();
        //                    }
        //                    else
        //                    {
        //                        fsCustomAppointmentStatusRow = new FSCustomAppointmentStatus();

        //                        graphCustomAppointmentStatusMaint.Clear();
        //                        fsCustomAppointmentStatusRow.BackgroundColor = customAppointmentStatus.BackgroundColor;
        //                        fsCustomAppointmentStatusRow.TextColor = customAppointmentStatus.TextColor;
        //                        fsCustomAppointmentStatusRow.FieldName = customAppointmentStatus.FieldName;
        //                        fsCustomAppointmentStatusRow.FieldDescr = customAppointmentStatus.FieldDescr;
        //                        fsCustomAppointmentStatusRow.HideStatus = customAppointmentStatus.HideStatus;

        //                        graphCustomAppointmentStatusMaint.CustomAppointmentStatusRecords.Insert(fsCustomAppointmentStatusRow);
        //                        graphCustomAppointmentStatusMaint.Save.PressButton();
        //                    }
        //                }

        //                return messages;
        //            }

        //            return null;
        //        }
        //        catch
        //        {
        //            messages.ErrorMessages.Add(TX.Error.CUSTOM_APPOINTMENT_STATUS_NOT_FOUND);
        //            return messages;
        //        }
        //    }

        //    public DispatchBoardAppointmentMessages PutCustomAppointmentStatuses(FSCustomAppointmentStatus[] customAppointmentStatuses)
        //    {
        //        DispatchBoardAppointmentMessages messages;
        //        messages = new DispatchBoardAppointmentMessages();

        //        try
        //        {
        //            var graphCustomAppointmentStatusMaint = PXGraph.CreateInstance<CustomAppointmentStatusMaint>();
                    
        //            if (customAppointmentStatuses != null)
        //            {
        //                foreach (FSCustomAppointmentStatus customAppointmentStatus in customAppointmentStatuses)
        //                {
        //                    FSCustomAppointmentStatus fsCustomAppointmentStatusRow = this.CustomAppointmentStatusName.Select(customAppointmentStatus.FieldName);

        //                    if (fsCustomAppointmentStatusRow != null)
        //                    {
        //                        fsCustomAppointmentStatusRow.BackgroundColor = customAppointmentStatus.BackgroundColor;
        //                        fsCustomAppointmentStatusRow.TextColor = customAppointmentStatus.TextColor;
        //                        fsCustomAppointmentStatusRow.FieldName = customAppointmentStatus.FieldName;
        //                        fsCustomAppointmentStatusRow.FieldDescr = customAppointmentStatus.FieldDescr;
        //                        fsCustomAppointmentStatusRow.HideStatus = customAppointmentStatus.HideStatus;

        //                        graphCustomAppointmentStatusMaint.CustomAppointmentStatusRecords.Update(fsCustomAppointmentStatusRow);
        //                        graphCustomAppointmentStatusMaint.Save.PressButton();
        //                    }
        //                    else
        //                    {
        //                        messages.ErrorMessages.Add(TX.Error.CUSTOM_APPOINTMENT_STATUS_NOT_FOUND);
        //                        return messages;
        //                    }
        //                }
        //            }

        //            return null;
        //        }
        //        catch
        //        {
        //            messages.ErrorMessages.Add(TX.Error.CUSTOM_APPOINTMENT_STATUS_NOT_FOUND);
        //            return messages;
        //        }
        //    }

        //    public DispatchBoardAppointmentMessages DeleteCustomAppointmentStatuses(FSCustomAppointmentStatus[] customAppointmentStatuses)
        //    {
        //        DispatchBoardAppointmentMessages messages;
        //        messages = new DispatchBoardAppointmentMessages();

        //        try
        //        {
        //            var graphCustomAppointmentStatusMaint = PXGraph.CreateInstance<CustomAppointmentStatusMaint>();

        //            if (customAppointmentStatuses != null)
        //            {
        //                foreach (FSCustomAppointmentStatus customAppointmentStatus in customAppointmentStatuses)
        //                {
        //                    FSCustomAppointmentStatus fsCustomAppointmentStatusesRow = this.CustomAppointmentStatus.Select(customAppointmentStatus.CustomAppointmentStatusID);

        //                    if (fsCustomAppointmentStatusesRow != null)
        //                    {
        //                        graphCustomAppointmentStatusMaint.CustomAppointmentStatusRecords.Delete(fsCustomAppointmentStatusesRow);
        //                    }
        //                    else
        //                    {
        //                        messages.ErrorMessages.Add(TX.Error.CUSTOM_APPOINTMENT_STATUS_NOT_FOUND);
        //                        return messages;
        //                    }
        //                }

        //                graphCustomAppointmentStatusMaint.Save.PressButton();
        //                return messages;
        //            }

        //            return null;
        //        }
        //        catch
        //        {
        //            messages.ErrorMessages.Add(TX.Error.CUSTOM_APPOINTMENT_STATUS_NOT_FOUND);
        //            return messages;
        //        }
        //    }

        //    public List<object> GetTreeAppointmentNodesByRoute(int routeDocumentID, FSRoute fsRouteRow)
        //    {
        //        var results = this.AppointmentRecordsByRoute.Select(routeDocumentID); 
        //        var graphExternalControls = PXGraph.CreateInstance<ExternalControls>();
        //        List<object> tmpLeaf = new List<object>();

        //        if (fsRouteRow == null)
        //        {
        //            fsRouteRow = PXSelectJoin<FSRoute,
        //                                        InnerJoin<FSRouteDocument,
        //                                            On<FSRouteDocument.routeDocumentID, Equal<Required<FSRouteDocument.routeDocumentID>>>>,
        //                                        Where<
        //                                            FSRoute.routeID, Equal<FSRouteDocument.routeID>>>
        //                                        .Select(this, routeDocumentID);                    
        //        }               

        //        FSBranchLocation fsBeginBranchLocationRow = PXSelect<FSBranchLocation,
        //                                        Where<
        //                                            FSBranchLocation.branchLocationID, Equal<Required<FSBranchLocation.branchLocationID>>>>
        //                                        .Select(this, fsRouteRow.BeginBranchLocationID);

        //        //Start Location Tree Node 
        //        tmpLeaf.Add(new
        //        {
        //            NodeID = "START LOCATION (" + fsRouteRow.RouteCD.Trim() + ")",
        //            Text = "START LOCATION (" + fsRouteRow.RouteCD.Trim() + ")",
        //            Mem_Duration = 0,
        //            Address = fsBeginBranchLocationRow.AddressLine1 + " " + fsBeginBranchLocationRow.AddressLine2 
        //                        + ", " + fsBeginBranchLocationRow.State + ", " + fsBeginBranchLocationRow.City + " " 
        //                        + fsBeginBranchLocationRow.PostalCode + ", " + fsBeginBranchLocationRow.CountryID,
        //            CustomerLocation = fsBeginBranchLocationRow.AddressLine1 + " " + fsBeginBranchLocationRow.AddressLine2,
        //            PostalCode = fsBeginBranchLocationRow.PostalCode,
        //            Leaf = true,
        //            Checked = true
        //        });

        //        foreach (PXResult<FSAppointment, FSServiceOrder, FSSrvOrdType, Location, Address, Country, State, FSCustomer> result in results)
        //        {
        //            FSAppointment fsAppointmentRow = result;
        //            FSServiceOrder fsServiceOrderRow = result;
        //            FSSrvOrdType fsSrvOrdTypeRow = result;
        //            Country countryRow = result;
        //            State stateRow = result;
        //            Address addressRow = result;
        //            FSCustomer fsCustomerRow = result;

        //            //Appointment Tree Node 
        //            tmpLeaf.Add(new
        //            {
        //                RefNbr = fsAppointmentRow.RefNbr,
        //                NodeID = fsAppointmentRow.RefNbr,
        //                Text = fsAppointmentRow.RefNbr,
        //                CustomerName = fsCustomerRow.AcctName,
        //                ScheduledDateTimeBegin = fsAppointmentRow.ScheduledDateTimeBegin,
        //                ScheduledDateTimeEnd = fsAppointmentRow.ScheduledDateTimeEnd,
        //                Mem_ServicesDuration = fsAppointmentRow.EstimatedDurationTotal,
        //                AutoDocDesc = fsAppointmentRow.AutoDocDesc,
        //                Address = fsServiceOrderRow.AddressLine1 + " " + fsServiceOrderRow.AddressLine2 + ", " + fsServiceOrderRow.State 
        //                            + ", " + fsServiceOrderRow.City + " " + fsServiceOrderRow.PostalCode + ", " + fsServiceOrderRow.CountryID,
        //                CustomerLocation = fsServiceOrderRow.AddressLine1 + " " + fsServiceOrderRow.AddressLine2,
        //                PostalCode = fsServiceOrderRow.PostalCode,
        //                SrvOrdType = fsSrvOrdTypeRow.SrvOrdType,
        //                SrvOrdTypeDescr = fsSrvOrdTypeRow.Descr,
        //                Leaf = true,
        //                Checked = true,
        //                Latitude = fsAppointmentRow.MapLatitude,
        //                Longitude = fsAppointmentRow.MapLongitude
        //            });
        //        }

        //        FSBranchLocation fsEndBranchLocationRow = PXSelect<FSBranchLocation,
        //                                        Where<
        //                                            FSBranchLocation.branchLocationID, Equal<Required<FSBranchLocation.branchLocationID>>>>
        //                                        .Select(this, fsRouteRow.EndBranchLocationID);

        //        //End Location Tree Node 
        //        tmpLeaf.Add(new
        //        {
        //            NodeID = "END LOCATION (" + fsRouteRow.RouteCD.Trim() + ")",
        //            Text = "END LOCATION ( " + fsRouteRow.RouteCD.Trim() + ")",
        //            Mem_Duration = 0,
        //            Address = fsEndBranchLocationRow.AddressLine1 + " " + fsEndBranchLocationRow.AddressLine2 + ", " 
        //                        + fsEndBranchLocationRow.State + ", " + fsEndBranchLocationRow.City + " " 
        //                        + fsEndBranchLocationRow.PostalCode + ", " + fsEndBranchLocationRow.CountryID,
        //            CustomerLocation = fsEndBranchLocationRow.AddressLine1 + " " + fsEndBranchLocationRow.AddressLine2,
        //            PostalCode = fsEndBranchLocationRow.PostalCode,
        //            Leaf = true,
        //            Checked = true
        //        });

        //        return tmpLeaf;
        //    }

        //    public List<object> GetTreeAppointmentNodesByEmployee(int employeeID, DateTime calendarDate)
        //    {
        //        DateHandler requestDate = new DateHandler(calendarDate);
        //        DateTime timeBegin = requestDate.StartOfDay();
        //        DateTime timeEnd = requestDate.EndOfDay();

        //        var results = this.AppointmentsByEmployee.Select(timeBegin, timeEnd, employeeID);
        //        var graphExternalControls = PXGraph.CreateInstance<ExternalControls>();
        //        List<object> tmpLeaf = new List<object>();

        //        foreach (PXResult<EPEmployee, 
        //                          FSAppointmentEmployee, 
        //                          FSAppointment, 
        //                          FSServiceOrder, 
        //                          FSSrvOrdType, 
        //                          BAccountStaffMember,
        //                          Location, 
        //                          Address, 
        //                          Country, 
        //                          State, 
        //                          FSCustomer> result in results)
        //        {
        //            FSAppointment fsAppointmentRow = result;
        //            FSServiceOrder fsServiceOrderRow = result;
        //            FSSrvOrdType fsSrvOrdTypeRow = result;
        //            Country countryRow = result;
        //            State stateRow = result;
        //            Address addressRow = result;
        //            FSCustomer fsCustomerRow = result;
        //            BAccountStaffMember bAccountStaffMemberRow = result;

        //            //Appointment Tree Node 
        //            tmpLeaf.Add(new
        //            {
        //                RefNbr = fsAppointmentRow.RefNbr,
        //                NodeID = employeeID + "-" + fsAppointmentRow.AppointmentID,
        //                Text = bAccountStaffMemberRow.AcctCD.Trim() + "-" + fsAppointmentRow.RefNbr,
        //                CustomerName = fsCustomerRow.AcctName,
        //                Mem_ServicesDuration = fsAppointmentRow.EstimatedDurationTotal,
        //                ScheduledDateTimeBegin = fsAppointmentRow.ScheduledDateTimeBegin,
        //                ScheduledDateTimeEnd = fsAppointmentRow.ScheduledDateTimeEnd,
        //                AutoDocDesc = fsAppointmentRow.AutoDocDesc,
        //                Address = fsServiceOrderRow.AddressLine1 + " " + fsServiceOrderRow.AddressLine2 + ", " + fsServiceOrderRow.State + ", " + fsServiceOrderRow.City + " " + fsServiceOrderRow.PostalCode + ", " + fsServiceOrderRow.CountryID,
        //                CustomerLocation = fsServiceOrderRow.AddressLine1 + " " + fsServiceOrderRow.AddressLine2,
        //                PostalCode = fsServiceOrderRow.PostalCode,
        //                SrvOrdType = fsSrvOrdTypeRow.SrvOrdType,
        //                SrvOrdTypeDescr = fsSrvOrdTypeRow.Descr,
        //                Leaf = true,
        //                Checked = true,
        //                Latitude = fsAppointmentRow.MapLatitude,
        //                Longitude = fsAppointmentRow.MapLongitude
        //            });
        //        }

        //        return tmpLeaf;
        //    }

        //    public List<FSAppointment> UnassignedAppointmentRecords(DateTime timeBegin, DateTime timeEnd, int branchID, int? branchLocationID, bool? unassignedAppointmentByRoom, DispatchBoardFilters[] filters)
        //    {
        //        List<object> args = new List<object>();
        //        List<FSAppointment> appointmentList = new List<FSAppointment>();

        //        PXSelectBase<FSAppointment> appointmentRecords = new PXSelectJoin<FSAppointment,
        //                                InnerJoin<FSServiceOrder,
        //                                        On<FSServiceOrder.sOID, Equal<FSAppointment.sOID>>,
        //                                InnerJoin<FSSrvOrdType,
        //                                        On<FSSrvOrdType.srvOrdType, Equal<FSServiceOrder.srvOrdType>>,
        //                                LeftJoin<Customer,
        //                                        On<Customer.bAccountID, Equal<FSServiceOrder.customerID>>,
        //                                LeftJoin<Location,
        //                                        On<Location.locationID, Equal<FSServiceOrder.locationID>>,
        //                                LeftJoin<Address,
        //                                        On<Address.addressID, Equal<Location.defAddressID>>,
        //                                LeftJoin<Country,
        //                                        On<Country.countryID, Equal<Address.countryID>>,
        //                                LeftJoin<State,
        //                                        On<State.countryID, Equal<Address.countryID>,
        //                                            And<State.stateID, Equal<Address.state>>>>>>>>>>,
        //                                Where2<
        //                                    Where<
        //                                        FSAppointment.status, NotEqual<FSAppointment.status.Completed>,
        //                                        And<FSAppointment.status, NotEqual<FSAppointment.status.Canceled>,
        //                                        And<FSAppointment.status, NotEqual<FSAppointment.status.Closed>,
        //                                        And<FSServiceOrder.branchID, Equal<Required<FSServiceOrder.branchID>>>>>>,
        //                                    And<
        //                                        Where<
        //                                            Where2<
        //                                                Where<FSAppointment.scheduledDateTimeBegin, GreaterEqual<Required<FSAppointment.scheduledDateTimeBegin>>,
        //                                                And<FSAppointment.scheduledDateTimeBegin, Less<Required<FSAppointment.scheduledDateTimeEnd>>>>,
        //                                            Or<
        //                                                Where<FSAppointment.scheduledDateTimeEnd, GreaterEqual<Required<FSAppointment.scheduledDateTimeBegin>>,
        //                                                And<FSAppointment.scheduledDateTimeEnd, Less<Required<FSAppointment.scheduledDateTimeEnd>>>>>>>>>
        //                                >(this);

        //        if (unassignedAppointmentByRoom == false)
        //        {
        //            appointmentRecords.Join<LeftJoin<FSAppointmentEmployee,
        //                                                On<FSAppointmentEmployee.appointmentID, Equal<FSAppointment.appointmentID>>>>();

        //            appointmentRecords.WhereAnd<Where<FSAppointmentEmployee.employeeID, IsNull>>();
        //        }
        //        else
        //        {
        //            appointmentRecords.WhereAnd<Where<FSServiceOrder.roomID, IsNull>>();
        //        }

        //        args.Add(branchID);
        //        args.Add(timeBegin);
        //        args.Add(timeEnd);
        //        args.Add(timeBegin);
        //        args.Add(timeEnd);

        //        if (branchLocationID != null)
        //        {
        //            appointmentRecords.WhereAnd<Where<FSServiceOrder.branchLocationID, Equal<Required<FSServiceOrder.branchLocationID>>>>();
        //            args.Add(branchLocationID);
        //        }

        //        if (filters != null)
        //        {
        //            for (int i = 0; i < filters.Length; i++)
        //            {
        //                if (filters[i].property == TX.Dispatch_Board.LIKETEXT_FILTER)
        //                {
        //                    appointmentRecords.WhereAnd<Where<
        //                                                    FSAppointment.refNbr, Like<Required<FSAppointment.refNbr>>,
        //                                                    Or<Customer.acctName, Like<Required<Customer.acctName>>>>>();

        //                    args.Add(string.Concat("%", filters[i].value[0], "%"));
        //                    args.Add(string.Concat("%", filters[i].value[0], "%"));
        //                }
        //            }
        //        }

        //        var results = appointmentRecords.View.SelectMulti(args.ToArray());
                
        //        foreach (PXResult<FSAppointment, FSServiceOrder, FSSrvOrdType, Customer> result in results)
        //        {
        //            FSAppointment fsAppointmentRow = result;
        //            FSServiceOrder fsServiceOrderRow = result;
        //            FSSrvOrdType fsSrvOrdTypeRow = result;
        //            Customer customer = result;
        //            PXResultset<InventoryItem> inventoryItemRows = this.AppointmentServices.Select(fsAppointmentRow.AppointmentID);

        //            if (customer != null)
        //            {
        //                fsAppointmentRow.CustomerName = customer.AcctName;
        //            }

        //            fsAppointmentRow.SORefNbr = fsServiceOrderRow.RefNbr;
        //            fsAppointmentRow.SrvOrdType = fsServiceOrderRow.SrvOrdType + " - " + fsSrvOrdTypeRow.Descr;
        //            fsAppointmentRow.ServiceCount = inventoryItemRows.Count;

        //            appointmentList.Add(fsAppointmentRow);
        //        }
                
        //        return appointmentList;
        //    }

        //    public List<object> GetUnassignedAppointmentNode(DateTime calendarDate, int branchID, int? branchLocationID)
        //    {
        //        DateHandler requestDate = new DateHandler(calendarDate);
        //        DateTime timeBegin = requestDate.StartOfDay();
        //        DateTime timeEnd = requestDate.EndOfDay();
        //        var graphExternalControls = PXGraph.CreateInstance<ExternalControls>();
        //        List<object> args = new List<object>();
                
        //        BqlCommand appointmentRecords = new Select2<FSAppointment,
        //                                                InnerJoin<FSServiceOrder,
        //                                                    On<FSServiceOrder.sOID, Equal<FSAppointment.sOID>>,
        //                                                InnerJoin<FSSrvOrdType,
        //                                                        On<FSSrvOrdType.srvOrdType, Equal<FSServiceOrder.srvOrdType>>,
        //                                                LeftJoin<Customer,
        //                                                        On<Customer.bAccountID, Equal<FSServiceOrder.customerID>>,
        //                                                LeftJoin<FSAppointmentEmployee,
        //                                                        On<FSAppointmentEmployee.appointmentID, Equal<FSAppointment.appointmentID>>,
        //                                                LeftJoin<Location,
        //                                                        On<Location.locationID, Equal<FSServiceOrder.locationID>>,
        //                                                LeftJoin<Address,
        //                                                        On<Address.addressID, Equal<Location.defAddressID>>,
        //                                                LeftJoin<Country,
        //                                                        On<Country.countryID, Equal<Address.countryID>>,
        //                                                LeftJoin<State,
        //                                                        On<State.countryID, Equal<Address.countryID>,
        //                                                        And<State.stateID, Equal<Address.state>>>>>>>>>>>,
        //                                                Where2<
        //                                                    Where<FSServiceOrder.branchID, Equal<Required<FSServiceOrder.branchID>>>,
        //                                                    And2<
        //                                                        Where<
        //                                                            Where2<
        //                                                                Where<FSAppointment.scheduledDateTimeBegin, GreaterEqual<Required<FSAppointment.scheduledDateTimeBegin>>,
        //                                                                And<FSAppointment.scheduledDateTimeBegin, LessEqual<Required<FSAppointment.scheduledDateTimeEnd>>>>,
        //                                                            Or< 
        //                                                                Where<FSAppointment.scheduledDateTimeEnd, GreaterEqual<Required<FSAppointment.scheduledDateTimeBegin>>,
        //                                                                And<FSAppointment.scheduledDateTimeEnd, LessEqual<Required<FSAppointment.scheduledDateTimeEnd>>>>>>>,
        //                                                    And<
        //                                                        FSAppointmentEmployee.employeeID, IsNull>>>>();

        //        args.Add(branchID);
        //        args.Add(timeBegin);
        //        args.Add(timeEnd);
        //        args.Add(timeBegin);
        //        args.Add(timeEnd);

        //        if (branchLocationID != null)
        //        {
        //            appointmentRecords = appointmentRecords.WhereAnd(typeof(Where<FSServiceOrder.branchLocationID, Equal<Required<FSServiceOrder.branchLocationID>>>));
        //            args.Add(branchLocationID);
        //        }

        //        PXView appointmentRecordsView = new PXView(this, true, appointmentRecords);
        //        var results = appointmentRecordsView.SelectMulti(args.ToArray());
                
        //        List<object> tmpLeaf = new List<object>();

        //        foreach (PXResult<FSAppointment, FSServiceOrder, FSSrvOrdType, Customer, FSAppointmentEmployee, Location, Address, Country, State> result in results)
        //        {
        //            FSAppointment fsAppointmentRow = result;
        //            FSServiceOrder fsServiceOrderRow = result;
        //            FSSrvOrdType fsSrvOrdTypeRow = result;
        //            Country countryRow = result;
        //            State stateRow = result;
        //            Address addressRow = result;
        //            Customer customer = result;

        //            //Appointment Tree Node 
        //            tmpLeaf.Add(new
        //            {
        //                NodeID = "Unassigned-" + fsAppointmentRow.RefNbr,
        //                Text = fsAppointmentRow.RefNbr,
        //                CustomerName = customer.AcctName,
        //                Duration = fsAppointmentRow.EstimatedDurationTotal,
        //                ScheduledDateTimeBegin = fsAppointmentRow.ScheduledDateTimeBegin,
        //                ScheduledDateTimeEnd = fsAppointmentRow.ScheduledDateTimeEnd,
        //                AutoDocDesc = fsAppointmentRow.AutoDocDesc,
        //                Address = fsServiceOrderRow.AddressLine1 + " " + fsServiceOrderRow.AddressLine2 + ", " + fsServiceOrderRow.State + ", " + fsServiceOrderRow.City + " " + fsServiceOrderRow.PostalCode + ", " + fsServiceOrderRow.CountryID,
        //                CustomerLocation = fsServiceOrderRow.AddressLine1 + " " + fsServiceOrderRow.AddressLine2,
        //                PostalCode = fsServiceOrderRow.PostalCode,
        //                SrvOrdType = fsSrvOrdTypeRow.SrvOrdType,
        //                SrvOrdTypeDescr = fsSrvOrdTypeRow.Descr,
        //                Leaf = true,
        //                Checked = true,
        //                Latitude = fsAppointmentRow.MapLatitude,
        //                Longitude = fsAppointmentRow.MapLongitude
        //            });
        //        }

        //        return tmpLeaf;
        //    }
        //#endregion

        #region Privateclass
        public class DispatchBoardFilters
        {
            public string property { get; set; }

            public string[] value { get; set; }
        }

        public class DispatchBoardAppointmentMessages
        {
            public List<string> ErrorMessages;
            public List<string> WarningMessages;

            public DispatchBoardAppointmentMessages()
            {
                this.ErrorMessages = new List<string>();
                this.WarningMessages = new List<string>();
            }
        }
        #endregion

        #region ErrorMessages
            /// <summary> 
            /// Gets the error message for a given error code.
            /// </summary>
            /// <returns>String with the error message and the error code.</returns>
            public static string GetErrorMessage(ErrorCode code)
            {
                string message = (int)code + ":";
                switch (code)
                {
                    case ErrorCode.APPOINTMENT_SHARED:
                        message = message + TX.Error.APPOINTMENT_SHARED;
                        break;
                    case ErrorCode.APPOINTMENT_NOT_FOUND:
                        message = message + TX.Error.APPOINTMENT_NOT_FOUND;
                        break;
                    default:
                        message = TX.Error.TECHNICAL_ERROR;
                        break;
                }

                return message;
            }

            public enum ErrorCode
            {
                APPOINTMENT_SHARED = 0,
                APPOINTMENT_NOT_FOUND = 1
            }
        #endregion
    }

    public class DateHandler
    {
        private DateTime date;

        public DateHandler()
        {
            this.date = DateTime.Now;
        }

        public DateHandler(double date)
        {
            this.date = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Local).AddMilliseconds(date);
        }

        public DateHandler(DateTime date)
        {
            this.date = date;
        }

        public void setDate(DateTime date)
        {
            this.date = date;
        }

        public DateTime StartOfDay()
        {
            return new DateTime(this.date.Year, this.date.Month, this.date.Day, 0, 0, 0, 0);
        }

        public DateTime EndOfDay()
        {
            return (new DateTime(this.date.Year, this.date.Month, this.date.Day, 0, 0, 0, 0)).AddDays(1);
        }

        public DateTime setHours(DateTime? date)
        {
            return new DateTime(this.date.Year, this.date.Month, this.date.Day, date.Value.Hour, date.Value.Minute, date.Value.Second, date.Value.Millisecond);
        }

        public string getDay()
        {
            return this.date.ToString("ddd");
        }
    }
}
