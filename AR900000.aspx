﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AR900000.aspx.cs" Inherits="Page_AR900000" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" ng-app="DB" >
    <head id="Head1" runat="server">
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">

        <title>Routes on Map</title>

    </head>
    <body>   
		<!-- Main Container -->
		<div id="main-container">
			<!-- Title -->
			<form id="form1" runat="server">
				<px_pt:PageTitle ID="pageTitle" runat="server" CustomizationAvailable="false" HelpAvailable="false"/>
			</form>
			<!-- Routes -->
			<div class="container">
				<div id="routes-container">
				</div>
			</div>
			<!-- End Routes -->
		</div>
	   <!-- End Main Container -->

    <!-- Google Maps api-->
    <script src="http://maps.googleapis.com/maps/api/js?sensor=false&language=en" type="text/javascript"></script>

    <!-- Global Variables -->
    <script type="text/javascript">
        var pageUrl= "<%= pageUrl %>";
        var baseUrl= "<%= baseUrl %>";
    </script>
    <script type="text/javascript">
        var Ext = Ext || {}; // Ext namespace won't be defined yet...

        // This function is called by the Microloader after it has performed basic
        // device detection. The results are provided in the "tags" object. You can
        // use these tags here or even add custom tags. These can be used by platform
        // filters in your manifest or by platformConfig expressions in your app.
        //
        Ext.beforeLoad = function (tags) {
            var s = location.search,  // the query string (ex "?foo=1&bar")
                profile;

            // For testing look for "?classic" or "?modern" in the URL to override
            // device detection default.
            //
            if (s.match(/\bclassic\b/)) {
                profile = 'classic';
            }
            else if (s.match(/\bmodern\b/)) {
                profile = 'modern';
            }
            else {
                profile = tags.desktop ? 'classic' : 'modern';
                //profile = tags.phone ? 'modern' : 'classic';
            }

            Ext.manifest = profile; // this name must match a build profile name

            // This function is called once the manifest is available but before
            // any data is pulled from it.
            //
            //return function (manifest) {
                // peek at / modify the manifest object
            //};
        };
    </script>

    <script id="microloader" data-app="042e83aa-e07f-4cdf-9ba7-57c891254e8c" type="text/javascript" src="bootstrap.js"></script>

    </body>
</html>

