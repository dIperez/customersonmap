/***
 * Consumed by both SchedulerPanel instances, adds a simple method for determining if a resource is available.
 */
Ext.define('CustomersOnMap.store.CustomerStore',
    {
        extend: 'Ext.data.Store',
        model: 'CustomersOnMap.model.Customer',

        proxy: 
        {
            url: pageUrl + '/GetCustomers',
            type: 'ajax',
            headers: { 'Content-type': 'application/json; charset=utf-8' },

            reader:{
                type: 'json',
                rootProperty: 'd.Result.Rows',
                metaProperty : 'd.MetaData',
                totalProperty : 'd.Result.TotalRows',
                successProperty: 'd.Success'
                //,messageProperty: 'd.Message'
            },
            
            //writer:{ 
            //    type: 'json',
            //    root: 'customer',
           //     encode: false
            //},
            /*extractResponseData: function (response) {
                console.log(response);
                return Ext.decode(response.responseText).d.Result;
            },*/
        },

        listeners:
        {
            load: function (store, records, success, eOpts)
            {

            }
        }
    }
);