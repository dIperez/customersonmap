Ext.define('CustomersOnMap.view.MainContainer', {

    extend: 'Ext.Panel',

    requires: [
            'CustomersOnMap.store.CustomerStore',
            'CustomersOnMap.model.Customer'
        ],

    layout: {
        type: 'border'
    },

    width: 750,
    height: 550,

    initComponent: function () {
        //console.log("View");
        console.log(pageUrl);
        var me = this;
        //var customerStore = Ext.create('CustomersOnMap.store.CustomerStore');

        var customerStore = new CustomersOnMap.store.CustomerStore();
        console.log(customerStore);
        customerStore.load();
        /*var customer1 = new CustomersOnMap.model.Customer();
        customer1.set('CustomerID',1);
        customer1.set('CustomerCD','Wendys');

        var customer2 = new CustomersOnMap.model.Customer();
        customer2.set('CustomerID',2);
        customer2.set('CustomerCD','Arturos');

        customerStore.add(customer1);
        customerStore.add(customer2);*/

        console.log(customerStore);

        var config = {
            items: [
            {
                region: 'west',
                xtype: 'grid',
                title: 'Customers',
                width: 250,
                store: customerStore,
                columns: 
                [
                    {
                        text: 'CustomerID',
                        dataIndex: 'CustomerID',
                        flex: 1
                    },
                    {
                        text: 'Customer CD',
                        dataIndex: 'CustomerCD',
                        flex: 2
                    }
                ]
            },{
                region: 'center',
                xtype: 'gmappanel',
                zoomLevel: 3,
                 gmapType: 'map',
                center: {
                        geoCodeAddr: '4 Yawkey Way, Boston, MA, 02215-3409, USA',
                        marker: {title: 'Fenway Park'}
                    },
                    markers: [{
                        lat: 42.339641,
                        lng: -71.094224,
                        title: 'Boston Museum of Fine Arts',
                        listeners: {
                            click: function(e){
                                Ext.Msg.alert('It\'s fine', 'and it\'s art.');
                            }
                        }
                    },{
                        lat: 42.339419,
                        lng: -71.09077,
                        title: 'Northeastern University'
                    }]
            }]
        };

        Ext.apply(me,config);

        me.callParent(arguments);
    }
});