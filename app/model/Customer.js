/***
 * A simple subclass of the Sch.model.Event class, which only adds a 'Duration' field. This field is only used when
 * the model is in the unplanned task grid.
 */
Ext.define('CustomersOnMap.model.Customer',
    {
        extend: 'Ext.data.Model',

        fields: 
        [
            { 
                name: 'CustomerID',
                type: 'int',
                useNull: true
            },
            {
                name: 'CustomerCD',
                type: 'string'
            }
        ]
    }
)
