using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using FieldService.ServiceDispatch;
using PX.Common;
using PX.Data;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.EP;
using PX.Objects.IN;
using PX.Objects.AR;


public partial class Page_AR990000 : PX.Web.UI.PXPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static Response GetCustomers()
    {
        //var graphARInvoiceEntry = PXGraph.CreateInstance<ARInvoiceEntry>();
        RequestParams request = new RequestParams(HttpContext.Current.Request);
        List<object> retList = new List<object>();
        List<object> employeeList = new List<object>();

        PXResultset<Customer> customersRows = PXSelect<Customer, Where<Customer.status, Equal<Customer.status.active>>>.Select(new PXGraph());

        int totalRows = customersRows.Count;
        int total = (request.Limit != 0 && request.Limit < totalRows) ? request.Limit : totalRows;
        int start = (request.Start != 0) ? request.Start : 0;

        //Paging Service Order
        for (int i = 0; i < totalRows; i++)
        {
            Customer customerRow = customersRows[i];

            var auxObject = new { CustomerID = customerRow.BAccountID, CustomerCD = customerRow.AcctCD };

            retList.Add(auxObject);
        }

        MetaData metaData = new MetaData(totalRows);
        Response response = new Response(metaData, retList, true, null);

        return response;
    }
    /*
    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static Response GetEmployees(int branchID, int? branchLocationID, bool? ignoreActiveFlag, bool? ignoreAvailabilityFlag, DateTime? scheduledDate)
    {
        var graphExternalControls = PXGraph.CreateInstance<ExternalControls>();
        RequestParams request = new RequestParams(HttpContext.Current.Request);
        List<object> retList = new List<object>();
        List<object> employeeList = new List<object>();
        DateTime? date = null;

        if (scheduledDate != null)
        {
            date = new DateHandler((DateTime)scheduledDate).StartOfDay();
        }

        PXResultset<EPEmployee> epEmployeeRows = graphExternalControls.EmployeeRecords(branchID, branchLocationID, ignoreActiveFlag, ignoreAvailabilityFlag, date, request.Filters);

        int totalRows = epEmployeeRows.Count;
        int total = (request.Limit != 0 && request.Limit < totalRows) ? request.Limit : totalRows;
        int start = (request.Start != 0) ? request.Start : 0;

        //Paging Service Order
        for (int i = 0; i < total && (start + i < totalRows); i++)
        {
            EPEmployee epEmployeeRow = epEmployeeRows[start + i];

            Contact contactRow = (Contact)graphExternalControls.EmployeeContact.Select(epEmployeeRow.ParentBAccountID, epEmployeeRow.DefContactID);

            var auxObject = new { EmployeeID = epEmployeeRow.BAccountID, EmployeeCD = epEmployeeRow.AcctCD, Contact = contactRow };

            retList.Add(auxObject);
        }

        MetaData metaData = new MetaData(totalRows);
        Response response = new Response(metaData, retList, true, null);

        return response;
    }*/

    #region PrivateClasses&Methods
    /*private static string durationFormat(double duration = 0, int format = (int)SharedFunctions.TimeFormat.Hours)
    {

        string value = "";

        int minutes = 0;
        int hours = 0;
        int days = 0;

        if (format == (int)SharedFunctions.TimeFormat.Hours) 
        {
            duration = duration * 60;
        }

        if (format == (int)SharedFunctions.TimeFormat.Seconds)
        {
            duration = duration / 60;
        }

        hours = (int)(duration / 60);
        minutes = (int)(duration % 60);

        days = (int)duration / (60 * 24);
        hours = hours - days * 24;

        if (days > 0) 
        {
            value = days + "d ";
        }

        value = value + hours + "h " + minutes + "m";

        return value;
    }*/

    public new class Response
    {
        public Result Result;
        public MetaData MetaData;
        public bool Success;
        public String Error;
        public ExternalControls.DispatchBoardAppointmentMessages Messages;

        public Response(MetaData metaData, List<object> rows, bool success , ExternalControls.DispatchBoardAppointmentMessages messages)
        //public Response(MetaData metaData, List<object> rows, bool success)
        {
            this.MetaData = metaData;
            this.Result = new Result();
            this.Result.Rows = (rows != null) ? rows : new List<object>();
            this.Result.TotalRows = (rows != null) ? rows.Count : 0;

            if (this.MetaData.TotalRows != null && this.MetaData.TotalRows > 0)
            {
                this.Result.TotalRows = this.MetaData.TotalRows;
            }

            this.Success = success;

            this.Messages = (messages != null) ? messages : null;

            if (!success)
            {
                HttpContext.Current.Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                HttpContext.Current.Response.StatusDescription = (messages.ErrorMessages.Count > 0) ? messages.ErrorMessages[0] : "";
            }

        }
    }

    public class MetaData
    {
        public int? TotalRows;
        public DateTime? minTimeBegin;
        public DateTime? maxTimeEnd;

        public MetaData(int? totalRows)
        {
            this.TotalRows = totalRows;
        }

        public MetaData(DateTime? minTimeBegin, DateTime? maxTimeEnd)
        {
            this.minTimeBegin = minTimeBegin;
            this.maxTimeEnd = maxTimeEnd;
        }

        public MetaData()
        {
            this.TotalRows = null;
        }
    }

    public class Result
    {
        public List<object> Rows;
        public int? TotalRows;
    }

    public class RequestParams
    {
        public ExternalControls.DispatchBoardFilters[] Filters;
        public int Page;
        public int Start;
        public int Limit;
        public bool PagingFlag = false;

        public RequestParams(HttpRequest request)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonFilters = HttpContext.Current.Request.QueryString["filter"];
            string jsonPage = HttpContext.Current.Request.QueryString["page"];
            string jsonStart = HttpContext.Current.Request.QueryString["start"];
            string jsonLimit = HttpContext.Current.Request.QueryString["limit"];

            Filters = (jsonFilters != null) ?
                        serializer.Deserialize<ExternalControls.DispatchBoardFilters[]>(jsonFilters) :
                        null;

            Limit = (jsonLimit != null && jsonLimit != "") ? Convert.ToInt32(jsonLimit) : 0;
            Start = (jsonStart != null && jsonStart != "") ? Convert.ToInt32(jsonStart) : 0;
            Page = (jsonPage != null && jsonPage != "") ? Convert.ToInt32(jsonPage) : 0;
        }

        public RequestParams()
        {
            Page = 0;
            Start = 0;
            Limit = 0;
            Filters = null;
        }

        public void EnablePaging()
        {
            PagingFlag = true;
        }

        public void DisablePaging()
        {
            PagingFlag = false;
        }

        public bool IsPaging() 
        {
            return PagingFlag;
        }
    }
    #endregion
}